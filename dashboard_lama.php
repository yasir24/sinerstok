<!DOCTYPE html>
<html lang="en">
<?php 
    date_default_timezone_set('Asia/Jakarta');
    include 'inc/head.php';
    if(empty($_SESSION['ak_id'] )){
        header('location:'.$url);
    }
    
?>
<body>

<!--Preloader Starts Here-->
<div id="ip-container" class="ip-container">
    <header class="ip-header">
        <h1 class="ip-logo text-center"><img class="img-fluid" src="assets/images/logo-c.png" alt="" class="ip-logo text-center"/></h1>
        <div class="ip-loader">
            <svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
                <path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                <path id="ip-loader-circle" class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
            </svg>
        </div>
    </header>
</div>
<!--Preloader Ends Here-->

<?php include 'inc/menu.php';?>

<!--Page Container-->
<section class="page-container">
    <div class="page-content-wrapper">
        <!--Main Content-->
        <?php
            $klien   = mysqli_query($koneksi, "SELECT * from klien order by nama_klien asc");
            $mesin      = mysqli_query($koneksi, "SELECT * from mesin order by nama_mesin asc");
            $produk     = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from produk p 
                                                  left join kategori k on p.id_kategori=k.id_kategori 
                                                  left join type t on p.id_type=t.id_type 
                                                  left join unit u on p.id_unit=u.id_unit 
                                                  left join ukur r on p.id_ukur=r.id_ukur 
                                                  LEFT JOIN stok s on p.produkId=id_produk
                                                  GROUP by produkId
                                                  order by produkId desc");
            $kategori   = mysqli_query($koneksi, "SELECT * from kategori order by nama_kategori asc");
            $type       = mysqli_query($koneksi, "SELECT * from type order by nama_type asc");
            $unit       = mysqli_query($koneksi, "SELECT * from unit order by nama_unit asc");
            $ukur       = mysqli_query($koneksi, "SELECT * from ukur order by nama_ukur asc");
            $feed       = mysqli_query($koneksi, "SELECT * FROM `feed` f LEFT JOIN status_feed s on f.status=s.id_status  ORDER BY `id_feed` DESC limit 5");
            if (isset($_GET['mod'])) {
                $mod = $_GET['mod'];
            }
            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            }
            if (isset($_GET['folder'])) {
                $folder = $_GET['folder'];
            }    
                
            
            if ($_GET['mod'] == 'add') {
                include "mod/$_GET[folder]/$_GET[page].php";
            }
            if ($_GET['mod'] == 'addgudang') {
                include "mod/gudang/add.php";
            }

            if ($_GET['mod'] == 'update') {
                include "mod/$_GET[folder]/update$_GET[page].php";
            }

            if ($_GET['mod'] == 'edit') {
                include "mod/$_GET[folder]/$_GET[folder].php";
            }
            if ($_GET['mod'] == 'editgudang') {
                include "mod/gudang/add.php";
            }

            if ($_GET['mod'] == 'out') {
                include "mod/$_GET[folder]/stok.php";
            }

            if ($_GET['mod'] == 'mod') {
                include "mod/$_GET[folder]/konten.php";
            }
            if ($_GET['mod'] == 'modgudang') {
                include "mod/gudang/kontenku.php";
            }

            if ($_GET['mod'] == 'detail') {
                include "mod/$_GET[folder]/view.php";
            }
            if ($_GET['mod'] == 'posisi') {
                include "mod/$_GET[folder]/posisi.php";
            }
            if ($_GET['mod'] == 'updateset') {
                include "mod/produk/updateset.php";
            }
            
            if ($_GET['mod'] == 'updateklien') {
                include "mod/produk/updateklien.php";
            }
            if ($_GET['mod'] == 'updatemesin') {
                include "mod/produk/updatemesin.php";
            }
            if ($_GET['mod'] == 'opname') {
                include "mod/produk/opname.php";
            }

            if ($_GET['mod'] == 'setpaket') {
                include "mod/$_GET[folder]/set.php";
            }

            // if($_GET['ys']==1){
            //     include "mod/$mod/konten.php";
            // }
            // elseif($_GET['ys']==3){
            //     include "mod/$mod/form-batchnonpack.php";
            // }
            // elseif($_GET['ys']==4){
            //     include "mod/$mod/form-batchpack.php";
            // }
            // elseif($_GET['ys']==5){
            //     include "mod/$mod/form-single.php";
            // }
            // else{
            //     include "mod/$mod/view.php";
            // }
        ?>
    </div>
</section>


<!--Jquery-->
<script type="text/javascript" src="assets/js/jquery-1.10.2.min.js"></script>
<!--Bootstrap Js-->
<script type="text/javascript" src="assets/js/popper.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!--Modernizr Js-->
<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>

<!--Morphin Search JS-->
<script type="text/javascript" src="assets/plugins/morphin-search/classie.js"></script>
<!-- <script type="text/javascript" src="assets/plugins/morphin-search/morphin-search.js"></script> -->
<!--Morphin Search JS-->
<script type="text/javascript" src="assets/plugins/preloader/pathLoader.js"></script>
<script type="text/javascript" src="assets/plugins/preloader/preloader-main.js"></script>

<!--Chart js-->
<script type="text/javascript" src="assets/plugins/charts/Chart.min.js"></script>

<!--Sparkline Chart Js-->
<script type="text/javascript" src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="assets/plugins/sparkline/jquery.charts-sparkline.js"></script>

<!--Custom Scroll-->
<script type="text/javascript" src="assets/plugins/customScroll/jquery.mCustomScrollbar.min.js"></script>
<!--Sortable Js-->
<script type="text/javascript" src="assets/plugins/sortable2/sortable.min.js"></script>
<!--DropZone Js-->
<script type="text/javascript" src="assets/plugins/dropzone/dropzone.js"></script>
<!--Date Range JS-->
<script type="text/javascript" src="assets/plugins/date-range/moment.min.js"></script>
<script type="text/javascript" src="assets/plugins/date-range/daterangepicker.js"></script>
<!--CK Editor JS-->
<script type="text/javascript" src="assets/plugins/ckEditor/ckeditor.js"></script>
<!--Data-Table JS-->
<script type="text/javascript" src="assets/plugins/data-tables/datatables.min.js"></script>
<!--Editable JS-->
<script type="text/javascript" src="assets/plugins/editable/editable.js"></script>
<!--Full Calendar JS-->
<script type="text/javascript" src="assets/plugins/full-calendar/fullcalendar.min.js"></script>

<!-- Main JS -->
<script src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/cloud-zoom.1.0.2.min.js"></script>

<script type="text/javascript">
     $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $(document).on("keyup", ".opname", function() {
        var id      =  $(this).data("id");
        var hasil   =  $(this).val();
        $("#opnamesave" + id + " i").removeClass( "simpan" );
        $("#opnamesave" + id + " i").addClass( "simpanaktif" );
        $("#opnamesave" + id + " i").attr("data-hasil", hasil);
    });

    $(document).on("click", ".opnamedelete", function() {
        var id      =  $(this).data("id");
        var produk  = $(this).data("produk");
        if (confirm("Apakah anda yakin?")) {
            $.ajax({
                url: "ajax/opname.php",
                type: 'get',
                data: "id=" + id + "&produk=" + produk + "&kat=hapus", 
                success: function(response){
                    $("#hapusku" + id).remove();
                    $(".opnameganti tr").remove();
                    $(".opnameganti").append(response);
                    alert("Data stok sudah terhapus");
                }
            });
        }
    });

    $(document).on("click", ".simpanaktif", function() {
        var id      = $(this).data("id");
        var stok  = $(this).data("hasil");
        var produk  = $(this).data("produk");
        $.ajax({
            url: "ajax/opname.php",
            type: 'get',
            data: "id=" + id + "&stok=" + stok + "&produk=" + produk + "&kat=update", 
            success: function(response){
                $(".opnameganti tr").remove();
                $(".opnameganti").append(response);
                $("#opnamesave" + id + " i").removeClass("simpanaktif");
                $("#opnamesave" + id + " i").addClass( "simpan" );

                var sum = 0;
                $(".totalku").each(function(){            
                    sum += +$(this).val();
                });
                $("#totalakhir").val(sum);
            }
        });

        
    });

    $(document).on("keyup", ".totalku", function() {
        var id      = $(this).data("id");
        var produk  = $(this).data("produk");
        var ukuran  = $(this).data("ukuran");
        var kurang  = $(this).val();
        $.ajax({
            url: "ajax/stok.php",
            type: 'get',
            data: "id=" + id + "&produk=" + produk +"&ukuran=" + ukuran +"&kurang=" + kurang, 
            success: function(response){
                $(".stok" + ukuran).remove();
                $("#datastok").append(response);
            }
        });

        var sum = 0;
        $(".totalku").each(function(){            
            sum += +$(this).val();
        });
        $("#totalakhir").val(sum);
    });

    $(document).on("keyup", ".totalpaket", function() {
        var produk  = $(this).data("produk");
        var kurang  = $(this).val();
        var maxkurang = $(this).data("maxkurang");
        $.ajax({
            url: "ajax/stokpaket.php",
            type: 'get',
            data: "produk=" + produk + "&kurang=" + kurang + "&maxkurang=" + maxkurang, 
            success: function(response){
                $(".stok").remove();
                $("#datastokpaket").append(response);
            }
        });

        var sum = 0;
        $(".totalku").each(function(){            
            sum += +$(this).val();
        });
        $("#totalakhir").val(sum);
    });

    $(document).on("keyup", ".totalkubatch", function() {
        var id      = $(this).data("id");
        var produk  = $(this).data("produk");
        var ukuran  = $(this).data("ukuran");
        var type  = $(this).data("type");
        var kurang  = $(this).val();
        // alert(id + " - " + produk +" - "+  ukuran);
        
        // alert(kurang);
        $.ajax({
            url: "ajax/stok.php",
            type: 'get',
            data: "id=" + id + "&produk=" + produk +"&ukuran=" + ukuran +"&kurang=" + kurang +"&type=" + type, 
            success: function(response){
                $(".stok" + ukuran).remove();
                $("#datastok").append(response);
            }
        });

        var sum = 0;
        $(".totalkubatch").each(function(){            
            sum += +$(this).val();
        });
        $("#totalakhir").val(sum);
    });

    $(document).ready(function() {
        $(".clickable").click(function() {
            window.location = $(this).data("href");
        });
        $(".duplikat").click(function(){                 
            var asli = $('#asliku').clone(true, true).appendTo("#tempat");
            asli.find(".single-date-picker").each(function() {
                $(this).attr("id", "").removeData('daterangepicker').unbind();
                $(this).val('');
                $(this).daterangepicker({"singleDatePicker": true});
           });            
        });
        $(".delduplikat").click(function(){
            $(this).closest("#asliku").remove();
        });

        $('.onoffswitch-checkbox').click(function() {
          var id = $(this).val();

          var myonoffswitch = $(this).val();

          var cek = $(this).prop('checked');

          if (cek == true) {
            $("#kaleng").html('Plastik');
          } else {
            $("#kaleng").html('Kaleng');
          }
          // alert(cek);
        });
    });

    $(document).ready(function(){
        $("#sat_ukur").change(function(){
            var id = $(this).val();
            $.ajax({
                type:"POST",
                url: "ajax/satuan.php",
                data: "id=" + id,
                success: function(data){
                    $(".satuan").text(data);
                }
            });
            
        });
        $("#propinsi").change(function(){
            var id = $("#propinsi").val();
            $.ajax({
                type:"POST",
                url: "ajax/propinsi.php",
                data: "id=" + id,
                success: function(data){
                    $("#kabupaten").html(data);
                    $("#kabupaten").fadeIn(2000);
                }
            });
        });
        $("#kabupaten").change(function(){
            var id = $("#kabupaten").val();
            $.ajax({
                type:"POST",
                url: "ajax/kota.php",
                data: "id=" + id,
                success: function(data){
                    $("#kecamatan").html(data);
                    $("#kecamatan").fadeIn(2000);
                }
            });
        });

        $("#klien").change(function(){
            var id = $(this).val();
            // alert(id);
            if (id == 'baru') {
                // alert("baru");
                $("#alamat").prop('disabled', false).val('');
                $("#tlp").prop('disabled', false).val('');
                $("#propinsi").prop('disabled', false);
                $("#kabupaten").prop('disabled', false);
                $("#kecamatan").prop('disabled', false);
                $("#kelurahan").prop('disabled', false).val('');
                $("#kode_pos").prop('disabled', false).val('');
                $("#klien_baru").prop('disabled', true);
                $('#propinsi option[value="baru"]').remove();
            }
            else{
                $("#alamat").prop('disabled', true);
                $("#tlp").prop('disabled', true);
                $("#propinsi").prop('disabled', true);
                $("#kabupaten").prop('disabled', true);
                $("#kecamatan").prop('disabled', true);
                $("#kelurahan").prop('disabled', true);
                $("#kode_pos").prop('disabled', true);
                $("#klien_baru").prop('disabled', true);
                $.ajax({
                    type:"POST",
                    url: "ajax/klien.php",
                    data: "id=" + id,
                    dataType: 'json',
                    success: function(data){
                        $("#alamat").val(data.alamat);
                        $("#tlp").val(data.tlp);
                        $("#propinsi").append('<option selected value="baru">' + data.propinsi +'</option>');
                        $("#kabupaten").append('<option selected value="' + data.kabupaten + '">' + data.kabupaten +'</option>');
                        $("#kecamatan").append('<option selected value="' + data.kecamatan + '">' + data.kecamatan +'</option>');
                        $("#kelurahan").val(data.kelurahan);
                        $("#kode_pos").val(data.kode_pos);
                    }
                });
            }
            
        });


        $("#addsup").click(function () {
            $("#showsup p").show();
            $("#showsup").append(
                '<div class="inp-text abu"><input placeholder="..." name="klienbaru[]" class="form-control mb10" style="width: 200px"></div>'
            );
        });
        $("#addmesin").click(function () {
            $("#showmesin p").show();
            $("#showmesin").append(
                '<div class="inp-text abu"><input placeholder="..." name="mesinbaru[]" class="form-control mb10" style="width: 200px"></div>'
            );
        });
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#imgInp").change(function() {
      readURL(this);
    });
    var x = 1;
        $(".tambah_menu").click(function(e){ //on add input button click
            e.preventDefault();
            if(x < 30){ //max input box allowed
                x++; //text box increment
                $(".menu_wrap").append('<div class="hapuss row mb20"><div class="col-md-3 mt15 col-sm-3 mb15"></div><div class="col-md-6 col-sm-6"><div class="inp-text abu"><input type="text" class="form-control" width="80%" placeholder="..." name="klien[]"></div></div><div class="hapus_menu close btn btn-default"></div></div></div>'); //add input box
            }
            if(x > 29){
                $(".tambah_menu").hide();
            }
        });

        $(".menu_wrap").on("click",".hapus_menu", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('.row').remove(); x--;
            if($(".tambah_menu").is(":hidden")){
                $(".tambah_menu").show();
            }
        });
</script>

</body>

<!-- Mirrored from wow.designgurus.in/sideNavigationLayout/blue/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Feb 2018 12:55:55 GMT -->
</html>
