<?php
     include "../inc/db.php";
     $now = time(); 
     $qwyku = "SELECT *, s.tgl as tglstok FROM `stok` s 
               LEFT JOIN produk p on s.id_produk=p.produkId 
               left join unit u   on p.id_unit=u.id_unit 
               LEFT join ukur uk  on p.id_ukur=uk.id_ukur
               LEFT JOIN gudang g on s.gudang=g.id_gudang
               LEFT JOIN rak r on s.rak=r.id_rak
               LEFT JOIN box b on s.box=b.id_box
               LEFT JOIN kalengplastik kp on s.kalengplastik=kp.id_kalengplastik
               WHERE `id_produk` = $_GET[produk] and ukuran = $_GET[ukuran] and exp >= NOW() - INTERVAL 1 DAY and stok_masuk <> 0 ORDER BY `s`.`exp`  ASC";
     if (isset($_GET['type']) && $_GET['type'] == 'single') {
          $qwyku = "SELECT *, s.tgl as tglstok FROM `stok` s 
               LEFT JOIN produk p on s.id_produk=p.produkId 
               left join unit u   on p.id_unit=u.id_unit 
               LEFT join ukur uk  on p.id_ukur=uk.id_ukur
               LEFT JOIN gudang g on s.gudang=g.id_gudang
               LEFT JOIN rak r on s.rak=r.id_rak
               LEFT JOIN box b on s.box=b.id_box
               LEFT JOIN kalengplastik kp on s.kalengplastik=kp.id_kalengplastik
               WHERE `id_produk` = $_GET[produk] and ukuran = $_GET[ukuran] and stok_masuk <> 0 ORDER BY `s`.`exp`  ASC";
     }
     $stok = mysqli_query($koneksi, $qwyku);
     // echo $qwyku;
     // echo "<br><br>";
     $total = "";
     $totalku = "";
     $kurang = $_GET['kurang'];

     while ($s = mysqli_fetch_array($stok)) {

          
          $total += $s['stok_masuk'];
          $totalku += $total - $kurang;

          // echo "total : ".$total;
          // echo "<br>";
          // echo "totalku :".$totalku;
          // echo "<br>";

          
          if ($totalku < 0) {
               $lamanya = strtotime($s['tglstok']);
               $lamadatediff =  ($now - $lamanya);
               $lama = round($lamadatediff / (60 * 60 * 24)) + 1;

               $expnya = strtotime($s['exp']);
               $expdatediff =  ($expnya - $now);
               $exp = round($expdatediff / (60 * 60 * 24));

               if ($exp < 1 ) {
                    $warning = "f-merah";
               }
               else {
                    $warning = "f-hijau";
               }
               $penguranganku = $total - $kurang;
               $penguranganasliku = $s['stok_masuk'] - $penguranganku;
               if ($penguranganasliku >= $s['stok_masuk'] ) {
                   $penguranganasliku = $s['stok_masuk'];
               }
               if ($penguranganasliku > 0) {
                    echo "
                         <tr class='stok$s[ukuran]'>
                              <td>".date("Ymd", strtotime($s['tglstok']))."-$s[id_stok]
                                   <input type='hidden' name='idstok[]' value='$s[id_stok]'>
                                   <input type='hidden' name='id_produk[]' value='$s[id_produk]'>
                                   <input type='hidden' name='id_klien[]' value='$s[id_klien]'>
                                   <input type='hidden' name='stok_out[]' value='$penguranganasliku'>
                                   <input type='hidden' name='batch[]' value='$s[id_klien]'>
                                   <input type='hidden' name='exp[]' value='$s[exp]'>
                                   <input type='hidden' name='ukuran[]' value='$s[ukuran]'>
                                   <input type='hidden' name='gudang[]' value='$s[gudang]'>
                                   <input type='hidden' name='rak[]' value='$s[rak]'>
                                   <input type='hidden' name='box[]' value='$s[box]'>
                                   <input type='hidden' name='kalengplastik[]' value='$s[kalengplastik]'>  
                                   <input type='hidden' name='stok_masuk[]' value='$s[stok_masuk]'>                                       
                              </td>
                              <td class='f-merah'>$penguranganasliku</td>
                              <td>$s[nama_gudang]</td>
                              <td>$s[nama_rak]</td>
                              <td>$s[nama_box]</td>
                              <td>$s[nama_kalengplastik]</td>
                              <td>-</td>
                              <td>$s[nama_unit]</td>
                              <td>@$s[ukuran]</td>
                              <td>$s[stok_masuk]</td>
                              <td>".$lama."</td>";
                              if ($_GET['type'] !== 'single') {
                                   echo "<td class='$warning'>".date("d.m.Y", strtotime($s['exp']))."</td>";
                              }
                              echo "
                         <tr>
                    ";
               }
               

               // echo "id - ".$s['id_stok']." - <br><br>";
               
                
          }
          else {
               $lamanya = strtotime($s['tglstok']);
               $lamadatediff =  ($now - $lamanya);
               $lama = round($lamadatediff / (60 * 60 * 24)) + 1;

               $expnya = strtotime($s['exp']);
               $expdatediff =  ($expnya - $now);
               $exp = round($expdatediff / (60 * 60 * 24));

               if ($exp < 1 ) {
                    $warning = "f-merah";
               }
               else {
                    $warning = "f-hijau";
               }
               $pengurangan = $total - $kurang;
               $penguranganasli = $s['stok_masuk'] - $pengurangan;
               if ($penguranganasli >= $s['stok_masuk'] ) {
                   $penguranganasli = $s['stok_masuk'];
               }
               if ($penguranganasli > 0) {
                    echo "
                         <tr class='stok$s[ukuran]'>
                              <td>
                                   ".date("Ymd", strtotime($s['tglstok']))."-$s[id_stok]
                                   <input type='hidden' name='idstok[]' value='$s[id_stok]'>
                                   <input type='hidden' name='id_produk[]' value='$s[id_produk]'>
                                   <input type='hidden' name='id_klien[]' value='$s[id_klien]'>
                                   <input type='hidden' name='stok_out[]' value='$penguranganasli'>
                                   <input type='hidden' name='batch[]' value='$s[id_klien]'>
                                   <input type='hidden' name='exp[]' value='$s[exp]'>
                                   <input type='hidden' name='ukuran[]' value='$s[ukuran]'>
                                   <input type='hidden' name='gudang[]' value='$s[gudang]'>
                                   <input type='hidden' name='rak[]' value='$s[rak]'>
                                   <input type='hidden' name='box[]' value='$s[box]'>
                                   <input type='hidden' name='kalengplastik[]' value='$s[kalengplastik]'>
                                   <input type='hidden' name='stok_masuk[]' value='$s[stok_masuk]'>      
                              </td>
                              <td class='f-merah'>$penguranganasli</td>
                              <td>$s[nama_gudang]</td>
                              <td>$s[nama_rak]</td>
                              <td>$s[nama_box]</td>
                              <td>$s[nama_kalengplastik]</td>
                              <td>-</td>
                              <td>$s[nama_unit]</td>
                              <td>@$s[ukuran]</td>
                              <td>$s[stok_masuk]</td>
                              <td>".$lama."</td>";
                              if ($_GET['type'] !== 'single') {
                                   echo "<td class='$warning'>".date("d.m.Y", strtotime($s['exp']))."</td>";
                              }
                              echo "
                         <tr>
                    ";
               }
               
               // echo "id - ".$s['id_stok']." - <br><br>";
               break; 
          }
     }
?>