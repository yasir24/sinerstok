<!DOCTYPE html>
<html lang="en">
<?php
    date_default_timezone_set('Asia/Jakarta');
    include 'inc/head.php';
    if(empty($_SESSION['ak_id'] )){
        header('location:'.$url);
    }

?>
<body>

<!--Preloader Starts Here-->
<!-- <div id="ip-container" class="ip-container">
    <header class="ip-header">
        <h1 class="ip-logo text-center"><img class="img-fluid" src="assets/images/logo-c.png" alt="" class="ip-logo text-center"/></h1>
        <div class="ip-loader">
            <svg class="ip-inner" width="60px" height="60px" viewBox="0 0 80 80">
                <path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
                <path id="ip-loader-circle" class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
            </svg>
        </div>
    </header>
</div> -->
<!--Preloader Ends Here-->

<div class="mb50"></div>

<!--Page Container-->
<section class="page-container">
    <div class="page-content-wrapper">
        <!--Main Content-->
        <?php
            $klien   = mysqli_query($koneksi, "SELECT * from klien order by nama_klien asc");
            $mesin      = mysqli_query($koneksi, "SELECT * from mesin order by nama_mesin asc");
            $kategori   = mysqli_query($koneksi, "SELECT * from kategori order by nama_kategori asc");
            $type       = mysqli_query($koneksi, "SELECT * from type order by nama_type asc");
            $unit       = mysqli_query($koneksi, "SELECT * from unit order by nama_unit asc");
            $ukur       = mysqli_query($koneksi, "SELECT * from ukur order by nama_ukur asc");
            $feed       = mysqli_query($koneksi, "SELECT * FROM `feed` f LEFT JOIN status_feed s on f.status=s.id_status  ORDER BY `id_feed` DESC limit 5");
            $produk     = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from produk p 
                                                  left join kategori k on p.id_kategori=k.id_kategori 
                                                  left join type t on p.id_type=t.id_type 
                                                  left join unit u on p.id_unit=u.id_unit 
                                                  left join ukur r on p.id_ukur=r.id_ukur 
                                                  LEFT JOIN stok s on p.produkId=id_produk
                                                  GROUP by produkId
                                                  order by produkId desc");
            $femas     = mysqli_query($koneksi, "SELECT * FROM femas");

            if (isset($_GET['mod'])) {
                $mod = $_GET['mod'];
            }
            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            }
            if (isset($_GET['folder'])) {
                $folder = $_GET['folder'];
            }


            if ($_GET['mod'] == 'add') {
                include "mod/$_GET[folder]/$_GET[page].php";
            }
            if ($_GET['mod'] == 'addgudang') {
                include "mod/gudang/add.php";
            }

            if ($_GET['mod'] == 'update') {
                include "mod/$_GET[folder]/update$_GET[page].php";
            }

            if ($_GET['mod'] == 'edit') {
                include "mod/$_GET[folder]/$_GET[folder].php";
            }
            if ($_GET['mod'] == 'editgudang') {
                include "mod/gudang/add.php";
            }

            if ($_GET['mod'] == 'out') {
                
                include "mod/stok/stok.php";
            }

            if ($_GET['mod'] == 'mod') {
                include "mod/$_GET[folder]/konten.php";
            }
            if ($_GET['mod'] == 'modgudang') {
                include "mod/gudang/kontenku.php";
            }

            if ($_GET['mod'] == 'sup') {
                include "mod/produk/klien.php";
            }
            if ($_GET['mod'] == 'mes') {
                include "mod/produk/mesin.php";
            }
            if ($_GET['mod'] == 'setpaketku') {
                include "mod/stok/setpaket.php";
            }

            if ($_GET['mod'] == 'detail') {
                include "mod/$_GET[folder]/view.php";
            }
            if ($_GET['mod'] == 'posisi') {
                include "mod/$_GET[folder]/posisi.php";
            }
            if ($_GET['mod'] == 'updateset') {
                include "mod/produk/updateset.php";
            }

            if ($_GET['mod'] == 'updateklien') {
                include "mod/klien/klien.php";
            }
            if ($_GET['mod'] == 'updatemesin') {
                include "mod/produk/updatemesin.php";
            }
            if ($_GET['mod'] == 'opname') {
                include "mod/produk/opname.php";
            }

            if ($_GET['mod'] == 'setpaket') {
                include "mod/$_GET[folder]/set.php";
            }

            // if($_GET['ys']==1){
            //     include "mod/$mod/konten.php";
            // }
            // elseif($_GET['ys']==3){
            //     include "mod/$mod/form-batchnonpack.php";
            // }
            // elseif($_GET['ys']==4){
            //     include "mod/$mod/form-batchpack.php";
            // }
            // elseif($_GET['ys']==5){
            //     include "mod/$mod/form-single.php";
            // }
            // else{
            //     include "mod/$mod/view.php";
            // }
        ?>
    </div>
</section>


<!--Jquery-->
<script type="text/javascript" src="assets/js/jquery-1.10.2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
<script type="text/javascript">
    var doc = new jsPDF();
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };
    $('.pdf').click(function () {   
        doc.fromHTML($('.content.sm-gutter').html(), 15, 15, {
            'width': 1200,
                'elementHandlers': specialElementHandlers
        });
        doc.save('sample-file.pdf');
    });
</script>
<!--Bootstrap Js-->
<script type="text/javascript" src="assets/js/popper.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!--Modernizr Js-->
<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>

<!--Morphin Search JS-->
<script type="text/javascript" src="assets/plugins/morphin-search/classie.js"></script>
<!-- <script type="text/javascript" src="assets/plugins/morphin-search/morphin-search.js"></script> -->
<!--Morphin Search JS-->
<script type="text/javascript" src="assets/plugins/preloader/pathLoader.js"></script>
<script type="text/javascript" src="assets/plugins/preloader/preloader-main.js"></script>

<!--Chart js-->
<script type="text/javascript" src="assets/plugins/charts/Chart.min.js"></script>

<!--Sparkline Chart Js-->
<script type="text/javascript" src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="assets/plugins/sparkline/jquery.charts-sparkline.js"></script>

<!--Custom Scroll-->
<script type="text/javascript" src="assets/plugins/customScroll/jquery.mCustomScrollbar.min.js"></script>
<!--Sortable Js-->
<script type="text/javascript" src="assets/plugins/sortable2/sortable.min.js"></script>
<!--DropZone Js-->
<script type="text/javascript" src="assets/plugins/dropzone/dropzone.js"></script>
<!--Date Range JS-->
<script type="text/javascript" src="assets/plugins/date-range/moment.min.js"></script>

<script src="assets/js/jquery-ui.js"></script>
<!--CK Editor JS-->
<script type="text/javascript" src="assets/plugins/ckEditor/ckeditor.js"></script>
<!--Data-Table JS-->
<script type="text/javascript" src="assets/plugins/data-tables/datatables.min.js"></script>
<!--Editable JS-->
<script type="text/javascript" src="assets/plugins/editable/editable.js"></script>
<!--Full Calendar JS-->
<script type="text/javascript" src="assets/plugins/full-calendar/fullcalendar.min.js"></script>

<!-- Main JS -->
<script src="assets/js/main.js"></script>
<script src="assets/js/jasny-bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/cloud-zoom.1.0.2.min.js"></script>

<script type="text/javascript">
     $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $(document).on("keyup", ".opname", function() {
        var id      =  $(this).data("id");
        var hasil   =  $(this).val();
        $("#opnamesave" + id + " i").removeClass( "simpan" );
        $("#opnamesave" + id + " i").addClass( "simpanaktif" );
        $("#opnamesave" + id + " i").attr("data-hasil", hasil);
    });

    $(document).on("click", ".opnamedelete", function() {
        var id      =  $(this).data("id");
        var produk  = $(this).data("produk");
        if (confirm("Apakah anda yakin?")) {
            $.ajax({
                url: "ajax/opname.php",
                type: 'get',
                data: "id=" + id + "&produk=" + produk + "&kat=hapus",
                success: function(response){
                    $("#hapusku" + id).remove();
                    $(".opnameganti tr").remove();
                    $(".opnameganti").append(response);
                    alert("Data stok sudah terhapus");
                }
            });
        }
    });

    $(document).on("click", ".simpanaktif", function() {
        var id      = $(this).data("id");
        var stok  = $(this).data("hasil");
        var produk  = $(this).data("produk");
        $.ajax({
            url: "ajax/opname.php",
            type: 'get',
            data: "id=" + id + "&stok=" + stok + "&produk=" + produk + "&kat=update",
            success: function(response){
                $(".opnameganti tr").remove();
                $(".opnameganti").append(response);
                $("#opnamesave" + id + " i").removeClass("simpanaktif");
                $("#opnamesave" + id + " i").addClass( "simpan" );

                var sum = 0;
                $(".totalku").each(function(){
                    sum += +$(this).val();
                });
                $("#totalakhir").val(sum);
            }
        });


    });

    $(document).on("keyup", ".totalku", function() {
        var id      = $(this).data("id");
        var produk  = $(this).data("produk");
        var ukuran  = $(this).data("ukuran");
        var kurang  = $(this).val();
        $.ajax({
            url: "ajax/stok.php",
            type: 'get',
            data: "id=" + id + "&produk=" + produk +"&ukuran=" + ukuran +"&kurang=" + kurang,
            success: function(response){
                $(".stok" + ukuran).remove();
                $("#datastok").append(response);
            }
        });

        var sum = 0;
        $(".totalku").each(function(){
            sum += +$(this).val();
        });
        $("#totalakhir").val(sum);
    });

    $(document).on("keyup", ".totalpaket", function() {
        <?php 
            $totalpk = "0";
            if (isset($_GET['folderku']) && $_GET['folderku'] == 'paket') {
                
                $pak = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from paket p 
                left join produk pr on p.produk_id=pr.produkId 

                left join unit u on pr.id_unit=u.id_unit 
                left join type t on pr.id_type=t.id_type 
                left join kategori k on pr.id_kategori=k.id_kategori 
                LEFT JOIN stok s on pr.produkId=s.id_produk 
                where p.id_produk = $_GET[id] GROUP by produkId
                ORDER BY `total` ASC limit 1");


                $pk = mysqli_fetch_array($pak);

                $totalpk = $pk['total'];
            }
            else{
                $totalpk = "0";
            }
        ?>
        
        var maxpaket= <?php echo $totalpk;?>;
        var produk  = $(this).data("produk");
        var kurang  = $(this).val();
        var maxkurang = $(this).data("maxkurang");
        
        
        $.ajax({
            url: "ajax/stokpaket.php",
            type: 'get',
            data: "produk=" + produk + "&kurang=" + kurang + "&maxkurang=" + maxkurang,
            success: function(response){
                $(".stok").remove();
                $("#datastokpaket").append(response);
            }
        });

        
        if ($(this).val() > maxpaket){
            $(this).val(maxpaket);
            $(".ukurku").text(maxpaket);
        }               
        else{
            $(".ukurku").text(kurang);
        }
       


        // var sum = 0;
        // $("input[class *= 'masukku']").each(function(){
        //     sum += +$(this).val();
        // });
        
        // if(maxku > 0){
        //     if ($(this).val() > maxku){
        //         $(this).val(maxku);
        //         var sum = 0;
        //         $("input[class *= 'masukku']").each(function(){
        //             sum += +$(this).val();
        //         });
        //     }
        //     $(".unitku").text(sum);
        //     $(".ukurku").text(sum*varian); 
        // }
        // else{
        //     $(".unitku").text(sum);
        //     $(".ukurku").text(sum*varian); 
        // }


        
    });

    $(document).on("keyup", ".totalkubatch", function() {
        var id      = $(this).data("id");
        var produk  = $(this).data("produk");
        var ukuran  = $(this).data("ukuran");
        var type  = $(this).data("type");
        var kurang  = $(this).val();
        // alert(id + " - " + produk +" - "+  ukuran);

        // alert(kurang);
        $.ajax({
            url: "ajax/stok.php",
            type: 'get',
            data: "id=" + id + "&produk=" + produk +"&ukuran=" + ukuran +"&kurang=" + kurang +"&type=" + type,
            success: function(response){
                $(".stok" + ukuran).remove();
                $("#datastok").append(response);
            }
        });

        var sum = 0;
        $(".totalkubatch").each(function(){
            sum += +$(this).val();
        });
        $("#totalakhir").val(sum);
    });

    $(document).ready(function() {
        $(".clickable").click(function() {
            window.location = $(this).data("href");
        });

        $(".duplikat").click(function(){
            var asli = $('#asliku').clone(true, true).appendTo("#tempat");
            asli.find('.hapus_varian').show();
            asli.find('input.single-date-picker')
            .attr("id", "")
            .removeClass('hasDatepicker')
            .removeData('datepicker')
            .unbind()
            .datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "yy-mm-dd"
            });
        });
        $(".delduplikat").click(function(){
            $(this).closest("#asliku").remove();
        });

        $('.onoffswitch-checkbox').click(function() {
          var id = $(this).val();

          var myonoffswitch = $(this).val();

          var cek = $(this).prop('checked');

          if (cek == true) {
            $("#kaleng").html('Plastik');
          } else {
            $("#kaleng").html('Kaleng');
          }
          // alert(cek);
        });
    });

    $(document).ready(function(){
        // $(".inp-text input").attr("placeholder", "—");
        $("#id_typemu").change(function(){
            var iku = $(this).val();
            // alert(iku);
            if(iku == '4'){
                $(".tam_typemu").hide();
                $(".tam_typemu").find('input').prop('required', false);
                $(".tam_typemu").find('select').prop('required', false);
            }
            else{
                $(".tam_typemu").show();

                $(".tam_typemu").find('input').prop('required', true);
                $(".tam_typemu").find('select').prop('required', true);
            }
        });

        $("#sat_keluar").change(function(){
            var unit = $("#sat_unit").val();
            var ukur = $("#sat_ukur").val();
            var keluar = $("#sat_keluar").val();
            $.ajax({
                type:"GET",
                url: "ajax/satuan.php",
                data: "unit=" + unit + "&ukur=" + ukur + "&keluar=" + keluar,
                dataType: 'json',
                success: function(data){
                    $(".satuan").text(data.satuan);
                    $(".satuanukurku").text(data.ukur);
                }
            });

        });
        $("#propinsi").change(function(){
            var id = $("#propinsi").val();
            $.ajax({
                type:"POST",
                url: "ajax/propinsi.php",
                data: "id=" + id,
                success: function(data){
                    $("#kabupaten").html(data);
                    $("#kabupaten").fadeIn(2000);
                }
            });
        });
        $("#kabupaten").change(function(){
            var id = $("#kabupaten").val();
            $.ajax({
                type:"POST",
                url: "ajax/kota.php",
                data: "id=" + id,
                success: function(data){
                    $("#kecamatan").html(data);
                    $("#kecamatan").fadeIn(2000);
                }
            });
        });

        $("#klien").change(function(){
            var id = $(this).val();
            // alert(id);
            if (id == 'baru') {
                // alert("baru");
                $("#alamat").prop('disabled', false).val('');
                $("#tlp").prop('disabled', false).val('');
                $("#propinsi").prop('disabled', false);
                $("#kabupaten").prop('disabled', false);
                $("#kecamatan").prop('disabled', false);
                $("#kelurahan").prop('disabled', false).val('');
                $("#kode_pos").prop('disabled', false).val('');
                $("#klien_baru").prop('disabled', true);
                $('#propinsi option[value="baru"]').remove();
            }
            else{
                $("#alamat").prop('disabled', true);
                $("#tlp").prop('disabled', true);
                $("#propinsi").prop('disabled', true);
                $("#kabupaten").prop('disabled', true);
                $("#kecamatan").prop('disabled', true);
                $("#kelurahan").prop('disabled', true);
                $("#kode_pos").prop('disabled', true);
                $("#klien_baru").prop('disabled', true);
                $.ajax({
                    type:"POST",
                    url: "ajax/klien.php",
                    data: "id=" + id,
                    dataType: 'json',
                    success: function(data){
                        $("#alamat").text(data.alamat);
                        $("#tlp").text(data.tlp);
                        $("#propinsi").text(data.propinsi);
                        $("#kabupaten").text(data.kabupaten);
                        $("#kecamatan").text(data.kecamatan);
                        $("#kelurahan").text(data.kelurahan);
                        $("#kode_pos").text(data.kode_pos);
                    }
                });
            }

        });


        $("#addsup").click(function () {
            $("#showsup p").show();
            $("#showsup").append(
                '<div class="inp-text abu"><input placeholder="..." name="klienbaru[]" class="form-control mb10" style="width: 200px"></div>'
            );
        });
        $("#addmesin").click(function () {
            $("#showmesin p").show();
            $("#showmesin").append(
                '<div class="inp-text abu"><input placeholder="..." name="mesinbaru[]" class="form-control mb10" style="width: 200px"></div>'
            );
        });
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $("#imgInp").change(function() {
      readURL(this);
    });

    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('.fileinput-preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $(".imgInp").change(function() {
      readURL(this);
    });

    var x = 1;
    $(".tambah_menu").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < 30){ //max input box allowed
            x++; //text box increment
            $(".menu_wrap").append('<div class="hapuss row mb20"><div class="col-md-3 mt15 col-sm-3 mb15"></div><div class="col-md-6 col-sm-6"><div class="inp-text abu"><input type="text" class="form-control" width="80%" placeholder="..." name="klien[]"></div></div><div class="hapus_menu close btn btn-default"></div></div></div>'); //add input box
        }
        if(x > 29){
            $(".tambah_menu").hide();
        }
    });

    $(".menu_wrap").on("click",".hapus_menu", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('.row').remove(); x--;
        if($(".tambah_menu").is(":hidden")){
            $(".tambah_menu").show();
        }
    });

    var x = 1;
    $(".tambah_varian").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < 10){ //max input box allowed
            x++; //text box increment
            $(".menu_varian").append('<tr class="ro"><td class="pl0" style="background: none;padding: 0 !important;"><div class="inp-text abu pull-left col-sm-12 pl0"><input type="text" class="form-control" width="90px" value="" name="varian[]" style="width: 90px"><span class="judul satuanukurku f-hitam"></span><div class="hapus_varian btn btn-dark w100">Delete</div></div></td></tr>'); //add input box
            var unit = $("#sat_unit").val();
            var ukur = $("#sat_ukur").val();
            var keluar = $("#sat_keluar").val();
            $.ajax({
                type:"GET",
                url: "ajax/satuan.php",
                data: "unit=" + unit + "&ukur=" + ukur + "&keluar=" + keluar,
                dataType: 'json',
                success: function(data){
                    $(".satuan").text(data.satuan);
                    $(".satuanukurku").text(data.ukur);
                }
            });
        }
        if(x > 9){
            $(".tambah_varian").hide();
        }
    });

    $(".menu_varian").on("click",".hapus_varian", function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parents('tr').remove(); 
        x--;
        if($(".tambah_varian").is(":hidden")){
            $(".tambah_varian").show();
        }
    });
    $(".hapus_varian").click(function(e){ //user click on remove text
        e.preventDefault(); 
        $(this).parents('tr').remove(); 
        x--;
        if($(".tambah_varian").is(":hidden")){
            $(".tambah_varian").show();
        }
    });

    $(".iconx").click(function(){
        $(this).parent('.iconxku').remove();
    });

    <?php
        $tam = 0;
        $id = "";
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $gambar = mysqli_query($koneksi, "SELECT * FROM gambar_produk where id_produk=$id");
            $total = mysqli_num_rows($gambar);
            if (!empty($total)) {
                $tam = $total;
            }
        }
        
    ?>
    var xku = <?php echo "$tam"; ?>;
    $(".tambah_gambar").click(function(e){ //on add input button click
        e.preventDefault();
        if(xku < 5){ //max input box allowed
            xku++; //text box increment
            $(".menu_gambar").append('<div class="hapusku "><div class="fileinput fileinput-new" data-provides="fileinput"><label><div class="fileinput fileinput-new" data-provides="fileinput"><div class="fileinput-preview upload" data-trigger="fileinput"></div><input type="file" name="nama_gambar[]" class="imgInp" class="hidden" style="display: none"></div></label></div><div class="hapus_gambar btn btn-default close mt15"></div></div>'); //add input box
        }
        if(xku > 4){
            $(".tambah_gambar").hide();
        }
    });

    $(".menu_gambar").on("click",".hapus_gambar", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('.hapusku').remove(); xku--;
        if($(".tambah_gambar").is(":hidden")){
            $(".tambah_gambar").show();
        }
    });


    $(".masukku").keyup(function(){
        var unit = $(this).val();
        var varian = $(this).data("varian");
        var keluar = $(this).data("keluar");
        var maxku   =$(this).data('maxku');
            var sum = 0;
            $("input[class *= 'masukku']").each(function(){
                sum += +$(this).val();
            });
            
            if(maxku > 0){
                if ($(this).val() > maxku){
                    $(this).val(maxku);
                    var sum = 0;
                    $("input[class *= 'masukku']").each(function(){
                        sum += +$(this).val();
                    });
                }
                $(".unitku").text(sum);
                $(".ukurku").text(sum*varian); 
            }
            else{
                $(".unitku").text(sum);
                $(".ukurku").text(sum*varian); 
            }
    });

    $(".keluarku").keyup(function(){
        var unit = $(this).val();
        var varian  = $(this).data("varian");
        var keluar  = $(this).data("keluar");
        var maxku   = $(this).data('maxku');
        var type    = $(this).data("type");
            var sum = 0;
            $("input[class *= 'keluarku']").each(function(){
                sum += +$(this).val();
            });

            if(type ==  'unit'){
                if(maxku > 0){
                    if ($(this).val() > maxku){
                        $(this).val(maxku);
                        var sum = 0;
                        $("input[class *= 'keluarku']").each(function(){
                            sum += +$(this).val();
                        });
                    }
                    $(".unitku").text(sum);
                    $(".ukurku").text(sum*varian); 
                }
                else{
                    $(".unitku").text(sum);
                    $(".ukurku").text(sum*varian); 
                }
            }
            else{
                if(maxku > 0){
                    if ($(this).val() > maxku){
                        $(this).val(maxku);
                        var sum = 0;
                        $("input[class *= 'keluarku']").each(function(){
                            sum += +$(this).val();
                        });
                    }
                    $(".unitku").text("0");
                    $(".ukurku").text(sum); 
                }
                
            }
            


    });

    $('#nsj').keyup(function(){
    var ses=$(this).val();
        sessionStorage.setItem('nsj',ses);
    });
    $('#nsj').val(sessionStorage.getItem('nsj'));

    $('#noref').keyup(function(){
    var ses=$(this).val();
        sessionStorage.setItem('noref',ses);
    });
    $('#noref').val(sessionStorage.getItem('noref'));

    $('.totalpaket').keyup(function(){
    var ses=$(this).val();
        sessionStorage.setItem('totalpaket',ses);
    });
    $('.totalpaket').val(sessionStorage.getItem('totalpaket'));




    $('#nama_produk').keyup(function(){
        var ses=$(this).val();
        sessionStorage.setItem('nama_produk',ses);
    });
    $('#nama_produk').val(sessionStorage.getItem('nama_produk'));

    $('#kode_produk').keyup(function(){
        var ses=$(this).val();
        sessionStorage.setItem('kode_produk',ses);
    });
    /* sessionStorage.clear(); */
    $('#kode_produk').val(sessionStorage.getItem('kode_produk'));

    $('#peringatan_produk').keyup(function(){
        var ses=$(this).val();
        sessionStorage.setItem('peringatan_produk',ses);
    });
    /* sessionStorage.clear(); */
    $('#peringatan_produk').val(sessionStorage.getItem('peringatan_produk'));

    $('#id_kategori').change(function(){
        var ses=$(this).val();
        sessionStorage.setItem('id_kategori',ses);
    });
    if(sessionStorage.getItem('id_kategori') == null){}
    else{
        $('#id_kategori').val(sessionStorage.getItem('id_kategori'));
    }

    $('#id_typemu').change(function(){
        var ses=$(this).val();
        sessionStorage.setItem('id_typemu',ses);
    });
    if(sessionStorage.getItem('id_typemu') == null){}
    else{
        $('#id_typemu').val(sessionStorage.getItem('id_typemu'));
    }

    $('#id_type').change(function(){
        var ses=$(this).val();
        sessionStorage.setItem('id_type',ses);
    });
    if(sessionStorage.getItem('id_type') == null){}
    else{
        $('#id_type').val(sessionStorage.getItem('id_type'));
    }

    $('#sat_unit').change(function(){
        var ses=$(this).val();
        sessionStorage.setItem('sat_unit',ses);
    });
    if(sessionStorage.getItem('sat_unit') == null){}
    else{
        $('#sat_unit').val(sessionStorage.getItem('sat_unit'));
    }
    

    $('#sat_ukur').change(function(){
        var ses=$(this).val();
        sessionStorage.setItem('sat_ukur',ses);
    });
    if(sessionStorage.getItem('sat_ukur') == null){}
    else{
        $('#sat_ukur').val(sessionStorage.getItem('sat_ukur'));
    }
    

    $('#sat_keluar').change(function(){
        var ses=$(this).val();
        sessionStorage.setItem('sat_keluar',ses);
    });
    if(sessionStorage.getItem('sat_ukur') == null){}
    else{
        $('#sat_keluar').val(sessionStorage.getItem('sat_keluar'));
    }
    

    
</script>

</body>

<!-- Mirrored from wow.designgurus.in/sideNavigationLayout/blue/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Feb 2018 12:55:55 GMT -->
</html>
