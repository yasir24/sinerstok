
<div class="row mb40">
    
    <div class="col-md-12 col-sm-12">
        <div class="cari pull-right ml10" style='margin-top:-3px'>
            <i class="iconcari"></i>
            <input type="text" id="input-filter" name="cari" placeholder="Search ...">
        </div>
        <span class="select-yasir pull-right mb20">
            <?php
                $ukur = "";
                $qwy = "SELECT *,SUM(stok_masuk) as jumlah, (SUM(stok_masuk)/ukuran) as total, s.tgl as tglstok  FROM `stok` s LEFT JOIN produk p on s.id_produk=p.produkId left join unit u on p.id_unit=u.id_unit LEFT join ukur uk on p.id_ukur=uk.id_ukur WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 GROUP by ukuran";
                $stok = mysqli_query($koneksi, $qwy);
                while ($s = mysqli_fetch_array($stok)) {
                    $ukur .= "<option value='@$s[ukuran]'>@$s[ukuran]</option>";
                }
            ?>
        </span>
        <div class="table-responsive text-no-wrap mt10">
            <table class="table" id="dataTable1" data-table="data-table-polos-disfirst">
                <thead class="text-middle">
                    <tr>
                        <th width="13%">Transaksi</th>
                        <th width="15%">No. Surat Jalan</th>
                        <th width="22%">Kepada / Dari</th>
                        <th width="5%" class="no-sort">Last</th>
                        <th width="5%" class="no-sort">Msk.</th>
                        <th width="5%" class="no-sort">Klr.</th>
                        <th width="5%">Qty</th>
                        <th width="5%">Stn</th>
                        <th width="10%">Date & Time <?=$p['nama_type'];?></th>
                    </tr>
                </thead>
                <tbody class="text-middle">
                    <?php 
                        if ($p['nama_type'] == 'Paket') {

                            $feeda       = mysqli_query($koneksi, "SELECT * FROM `feed` f LEFT JOIN status_feed s on f.status=s.id_status where id_paket = $_GET[id] ORDER BY `id_feed` DESC");
                            // echo "SELECT * FROM `feed` f LEFT JOIN status_feed s on f.status=s.id_status where id_paket = $_GET[id] ORDER BY `id_feed` DESC";
                            
                            while($fee = mysqli_fetch_array($feeda)){
                                $feedaku       = mysqli_query($koneksi, "SELECT * FROM `feed` f LEFT JOIN status_feed s on f.status=s.id_status where id = $fee[id] ORDER BY `id_feed` DESC");
                                while($f=mysqli_fetch_array($feedaku)){
                                    if ($f['page'] == 'stok' && $f['status'] == 1) {
                                        $fku = mysqli_query($koneksi, "SELECT * from stok s 
                                                                        left join produk p on s.id_produk=p.produkId  
                                                                        left join ukur u on p.id_ukur=u.id_ukur
                                                                        left join klien sup on s.id_klien=sup.id_klien 
                                                                        where id_stok    = $f[id]");
                                        while ($fk = mysqli_fetch_array($fku)) {
                                            echo "
                                                <tr>
                                                    <td>$f[id_feed] $f[nama_st] $f[page]</td>
                                                    <td>$fk[surat_jalan]</td>
                                                    <td>$fk[nama_klien]</td>
                                                    <td>-</td>
                                                    <td class='f-hijau'>0</td>
                                                    <td class='f-merah'>7</td>
                                                    <td class='f-hijau'>0</td>
                                                    <td>$fk[nama_ukur]</td>
                                                    <td>".date("d.m.Y", strtotime($f['tgl']))."</td>
                                                </tr>
                                            ";
                                        }    
                                    }
                                    if ($f['page'] == 'stok' && $f['status'] == 3) {
                                        $fout = mysqli_query($koneksi, "SELECT * from stokout s 
                                                                        left join produk p on s.id_produk=p.produkId  
                                                                        left join ukur u on p.id_ukur=u.id_ukur
                                                                        left join klien sup on s.id_klienout=sup.id_klien 
                                                                        where id_stokout = $f[id]");
                                        while ($fo = mysqli_fetch_array($fout)) {
                                            echo "
                                                <tr>
                                                    <td>$f[nama_st] $f[page]</td>
                                                    <td>$fo[surat_jalan]</td>
                                                    <td>$fo[nama_klien]</td>
                                                    <td>$f[sebelum]</td>
                                                    <td class='f-hijau'>-</td>
                                                    <td class='f-merah'>$f[sesudah]</td>
                                                    <td class='f-hijau'>0</td>
                                                    <td>$fo[nama_ukur]</td>
                                                    <td>".date("d.m.Y", strtotime($f['tgl']))."</td>
                                                </tr>
                                            ";
                                        }    
                                    }
                                                                        
                                }
                            }
                        }
                        else{
                            $feeda       = mysqli_query($koneksi, "SELECT * FROM `feed` f LEFT JOIN status_feed s on f.status=s.id_status where id_produk = $_GET[id] ORDER BY `id_feed` DESC");
                            while($f=mysqli_fetch_array($feeda)){
                                if ($f['page'] == 'stok' && $f['status'] == 1) {
                                    $fku = mysqli_query($koneksi, "SELECT * from stok s 
                                                                    left join produk p on s.id_produk=p.produkId  
                                                                    left join ukur u on p.id_ukur=u.id_ukur
                                                                    left join klien sup on s.id_klien=sup.id_klien 
                                                                    where id_stok = $f[id]");
                                    while ($fk = mysqli_fetch_array($fku)) {
                                        echo "
                                            <tr>
                                                <td>$f[id_feed] $f[nama_st] $f[page]</td>
                                                <td>$fk[surat_jalan]</td>
                                                <td>$fk[nama_klien]</td>
                                                <td>-</td>
                                                <td class='f-hijau'>0</td>
                                                <td class='f-merah'>7</td>
                                                <td class='f-hijau'>0</td>
                                                <td>$fk[nama_ukur]</td>
                                                <td>".date("d.m.Y", strtotime($f['tgl']))."</td>
                                            </tr>
                                        ";
                                    }    
                                }
                                if ($f['page'] == 'stok' && $f['status'] == 3) {
                                    $fout = mysqli_query($koneksi, "SELECT * from stokout s 
                                                                    left join produk p on s.id_produk=p.produkId  
                                                                    left join ukur u on p.id_ukur=u.id_ukur
                                                                    left join klien sup on s.id_klienout=sup.id_klien 
                                                                    where id_stokout = $f[id]");
                                    while ($fo = mysqli_fetch_array($fout)) {
                                        echo "
                                            <tr>
                                                <td>$f[nama_st] $f[page]</td>
                                                <td>$fo[surat_jalan]</td>
                                                <td>$fo[nama_klien]</td>
                                                <td>$f[sebelum]</td>
                                                <td class='f-hijau'>-</td>
                                                <td class='f-merah'>$f[sesudah]</td>
                                                <td class='f-hijau'>0</td>
                                                <td>$fo[nama_ukur]</td>
                                                <td>".date("d.m.Y", strtotime($f['tgl']))."</td>
                                            </tr>
                                        ";
                                    }    
                                }
                                                                    
                            }
                        }
                        
                        ?>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>