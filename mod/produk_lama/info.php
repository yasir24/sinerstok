<div class="mb-12 col-sm-12 mb-10">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 judulku mb20">
                    <div class="row">
                        klien
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <?php
                            $result     = explode("+", $p['klien']);                                   
                            foreach($result as $newvalue){
                                $query  = "SELECT * from klien where id_klien='$newvalue' order by nama_klien asc"; 
                                $result = mysqli_query($koneksi, $query);
                                $row    = mysqli_fetch_array($result);
                                echo"
                                    <div class='col-sm-4 mb10'>
                                        <span class='checkedku'></span>
                                        <span class='custom-control-description'>$row[nama_klien]</span>
                                    </div>
                                ";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb30 col-sm-12"></div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 judulku mb20">
                    <div class="row">
                        Mesin
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <?php
                            $result     = explode("+", $p['mesin']);                                   
                            foreach($result as $newvalue){
                                $query  = "SELECT * from mesin where id_mesin='$newvalue' order by nama_mesin asc"; 
                                $result = mysqli_query($koneksi, $query);
                                $row    = mysqli_fetch_array($result);
                                echo"
                                    <div class='col-sm-4 mb10'>
                                        <span class='checkedku'></span>
                                        <span class='custom-control-description'>$row[nama_mesin]</span>
                                    </div>
                                ";
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb30 col-sm-12"></div>
    </div>
</div>

<div class="row m-10">
    <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10">
        <div class="block counter-block mb20 putih">
            <div class="value mb15 f-merah"><?=$p['total']; ?></div>
            <div class="f13"><?=$p['nama_ukur']; ?></div>
            <div class="label m0 f-dark">Total stok unit</div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10">
        <div class="block counter-block mb20 putih">
            <div class="value mb15 f-dark"><?=$ukuranku; ?></div>
            <div class="f13">.</div>
            <div class="label m0 f-dark">Total stok ukuran</div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10">
        <div class="block counter-block mb20 putih">
            <div class="value mb15 f-dark"><?=$lamaku;?></div>
            <div class="f13">Hari</div>
            <div class="label m0 f-dark">Total waktu gudang</div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10">
        <div class="block counter-block mb20 putih">
            <div class="value mb15 f-dark"><?=$lamaku_ot;?></div>
            <div class="f13">hari yang lalu</div>
            <div class="label m0 f-dark">Order terakhir</div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10">
        <div class="block counter-block hitam">
            <div class="value mb15 f-putih"><?=$ohi;?></div>
            <div class="f-putih f13 f-putih" style="color: #fff">(per. <?php echo date("d.m.Y"); ?>)</div>
            <div class="label m0 f-putih">Order hari ini</div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10">
        <div class="block counter-block hitam">
            <div class="value mb15 f-putih"><?=$obi;?></div>
            <div class="f-putih f13 f-putih" style="color: #fff">(per. <?php echo date("m.Y"); ?>)</div>
            <div class="label m0 f-putih">Order bulan ini</div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10">
        <div class="block counter-block hitam">
            <div class="value mb15 f-putih"><?=$oyi;?></div>
            <div class="f-putih f13 f-putih" style="color: #fff">(per. <?php echo date("Y"); ?>)</div>
            <div class="label m0 f-putih">Order tahun ini</div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10">
        <div class="block counter-block hitam">
            <div class="value mb15 f-putih"><?=$rata;?></div>
            <div class="f-putih f13 f-putih" style="color: #fff">/ bulan (per. <?php echo date("Y"); ?>)</div>
            <div class="label m0 f-putih">Rata - rata order</div>
        </div>
    </div>
</div>