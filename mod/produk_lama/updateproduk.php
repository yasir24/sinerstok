<?php 
    $produk     = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from produk p 
    left join kategori k on p.id_kategori=k.id_kategori 
    left join type t on p.id_type=t.id_type 
    left join unit u on p.id_unit=u.id_unit 
    left join ukur r on p.id_ukur=r.id_ukur 
    LEFT JOIN stok s on p.produkId=id_produk
    where p.produkId = $_GET[id]
    GROUP by produkId
    order by produkId desc");
    $p = mysqli_fetch_array($produk);
 ?>
<form method="post" enctype="multipart/form-data" action="mod/<?php echo"$folder/aksi.php?mod=$mod&url=$_GET[url]&folder=$_GET[folder]"; ?>">
<input type="hidden" name="status" value="update">
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 0px ">
            <div class="row">
                <div class="col-lg-12 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <div class="row">
                            <div class="bold col-4">
                                <div class="pull-left mr20">
                                    <a href="#" class="menukekiri pull-left"></a>
                                    <a href="#" class="menukekanan pull-left"></a>
                                </div>
                                <h4 class="arialbold mt5">Update Produk</h4>
                            </div>
                            <div class="col-8 text-right">
                                <input type="submit" name="save" value="" class="btn btn-default simpan mr10">
                                <a href="#" class="btn btn-default close"></a>
                            </div>
                        </div>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="">
                <div class="block counter-block mb-12 col-sm-12 mb20">
                    <div class="bold title mb-20">
                        <h4 class="text-capitalize">Informasi Produk</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Gambar Produk <br>
                                
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-image abu">
                                <label for="imgInp">
                                    <img id="blah" alt="your image" src="assets\images\produk\<?=$p['gambar_produk'];?>" width="100px" />  
                                </label>
                                <input type="file" name="fupload" id="imgInp" style="display: none" />
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Nama Produk
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" class="form-control" value="<?=$p['nama_produk'];?>" width="80%" placeholder="..." name="nama_produk">
                                <!-- <span class="judul f-merah">
                                    Nama barang sudah digunakan
                                </span> -->
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Kode Produk
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" class="form-control" width="80%" value="<?=$p['kode_produk'];?>" placeholder="..." name="kode_produk">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Kategori
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" name='id_kategori'>
                                        <option>- Pilih Kategori -</option>
                                        <?php
                                            $sel = "";
                                            while ($k = mysqli_fetch_array($kategori)) {
                                                if ($k['id_kategori'] == $p['id_kategori']) {
                                                    echo "<option value='$k[id_kategori]' selected>$k[nama_kategori]</option>";
                                                }
                                                else{
                                                    echo "<option value='$k[id_kategori]'>$k[nama_kategori]</option>";
                                                }
                                                
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Type Produk
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" name="id_type">
                                        <option>- Pilih Type Produk -</option>
                                        <?php
                                            $sel = "";
                                            while ($k = mysqli_fetch_array($type)) {
                                                if ($k['id_type'] == $p['id_type']) {
                                                    echo "<option value='$k[id_type]' selected>$k[nama_type]</option>";
                                                }
                                                else{
                                                    echo "<option value='$k[id_type]'>$k[nama_type]</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                                <div class="judul abu">
                                    Deskripsi
                                </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <textarea name="deskripsi" placeholder="Tulis deskripsi untuk keterangan produk ..."><?=$p['deskripsi'];?></textarea><br>
                                <p class="f11 f-abu">Max. 320 karakter</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block counter-block mb-12 col-sm-12 mb20">
                    <div class="bold title mb-20">
                        <h4>klien
                            <a href="updateklien-produk-<?=$_GET['id'];?>-3.htm" class="btn btn-default pull-right update"></a>
                        </h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <?php                                    
                                    $ss = explode('+', $p['klien']);
                                    foreach ($ss as $key) {
                                        $klien   = mysqli_query($koneksi, "SELECT * from klien where id_klien = $key");
                                        $s = mysqli_fetch_array($klien);
                                        echo "
                                            <div class='col-sm-4'>
                                                <label class='custom-control custom-radio'>
                                                    <span class='checkedku'></span>
                                                    <span class='custom-control-description'>$s[nama_klien]</span>
                                                </label>
                                            </div>
                                        ";                                    
                                    }                                    
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block counter-block mb-12 col-sm-12 mb20">
                    <div class="bold title mb-20">
                        <h4>Mesin <a href="updatemesin-produk-<?=$_GET['id'];?>-3.htm" class="btn btn-default pull-right update"></a></h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <?php
                                    $mesin = explode('+', $p['mesin']);
                                    foreach ($mesin as $key) {
                                        $mesin   = mysqli_query($koneksi, "SELECT * from mesin where id_mesin = $key");
                                        $s = mysqli_fetch_array($mesin);
                                        echo "
                                            <div class='col-sm-4'>
                                                <label class='custom-control custom-radio'>
                                                    <span class='checkedku'></span>
                                                    <span class='custom-control-description'>$s[nama_mesin]</span>
                                                </label>
                                            </div>
                                        ";                                    
                                    }  
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block counter-block mb-12 col-sm-12 mb20">
                    <div class="bold title mb-20">
                        <h4>Keterangan stok</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Satuan Unit
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" name="id_unit">
                                        <option>- Pilih Satuan Unit -</option>
                                        <?php
                                            while ($k = mysqli_fetch_array($unit)) {
                                                if ($k['id_unit'] == $p['id_unit']) {
                                                    echo "<option value='$k[id_unit]' selected>$k[nama_unit]</option>";
                                                }
                                                else{
                                                    echo "<option value='$k[id_unit]'>$k[nama_unit]</option>";
                                                }
                                                
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Satuan Ukur
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" name="id_ukur" id="sat_ukur">
                                        <option>- Pilih Ukur Produk -</option>
                                        <?php
                                            while ($k = mysqli_fetch_array($ukur)) {
                                                if ($k['id_ukur'] == $p['id_ukur']) {
                                                    echo "<option value='$k[id_ukur]' selected>$k[nama_ukur]</option>";
                                                }
                                                else{
                                                    echo "<option value='$k[id_ukur]'>$k[nama_ukur]</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Satuan stok keluar
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" name="keluar_produk">
                                        <option>- Pilih Satuan keluar -</option>
                                        <option value="unit" <?php echo $p['keluar_produk'] = 'unit' ? 'selected' : '' ;?>>Unit</option>
                                        <option value="ukur" <?php echo $p['keluar_produk'] = 'ukur' ? 'selected' : '' ;?>>Ukur</option>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Minimum stok keluar
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu" style="width: 130px">
                                <input type="text" class="form-control" width="90px" value="<?=$p['minim_produk'];?>" placeholder="..." name="minim_produk" style="width: 90px">
                                <span class="judul satuan">
                                    <!-- Kg -->
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Peringatan stok minimum
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu" style="width: 130px">
                                <input type="text" class="form-control" width="90px" value="<?=$p['peringatan_produk'];?>" placeholder="..." name="peringatan_produk" style="width: 90px">
                                <span class="judul satuan">
                                    <!-- Kg -->
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block counter-block mb-12 col-sm-12 mb20">
                    <div class="bold title mb-20">
                        <h4>Varian ukuran produk</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Satuan Ukur
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                        <?php
                            $ukur = "";
                            $qwy = "SELECT *,SUM(stok_masuk) as jumlah, (SUM(stok_masuk)) as total, s.tgl as tglstok  FROM `stok` s LEFT JOIN produk p on s.id_produk=p.produkId left join unit u on p.id_unit=u.id_unit LEFT join ukur uk on p.id_ukur=uk.id_ukur WHERE `id_produk` = $_GET[id] and exp >= NOW() - INTERVAL 1 DAY and stok_masuk <> 0 GROUP by ukuran";
                            $stok = mysqli_query($koneksi, $qwy);
                            while ($s = mysqli_fetch_array($stok)) {
                                $unit = $s['nama_unit'];
                                $ukur .= "<option value='@$s[ukuran]'>@$s[ukuran]</option>";
                                
                                echo "
                                    <div class='col-md-3 col-sm-6'>
                                        <div class='judul'>
                                            - @$s[ukuran]
                                        </div>
                                    </div>
                                    
                                    <div class='col-md-12 col-sm-12 mb5'></div>
                                ";    
                            }
                            
                        ?>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                        </div>
                    </div>
                </div>


                <div class="col-md-12 col-sm-12 mb-100"></div>
            </div>
        </div>
    </div>
</form>