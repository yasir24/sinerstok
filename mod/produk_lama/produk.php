<?php 

    if ($_GET['mod'] == 'add') {
        $kat = 1;
    }
    else{
        $kat = 2;
        $de = mysqli_query($koneksi, "SELECT * from $folder where id_$folder = $_GET[id]");
        $d  = mysqli_fetch_array($de);
    }
 ?>
<form method="post" enctype="multipart/form-data" action="mod/<?php echo"$folder/aksi.php?mod=$mod&kat=$kat&url=$_GET[url]&folder=$_GET[folder]"; ?>">
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 0px ">
            <div class="row">
                <div class="col-lg-12 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <div class="row">
                            <div class="bold col-4">
                                <div class="pull-left mr20">
                                    <a href="#" class="menukekiri pull-left"></a>
                                    <a href="#" class="menukekanan pull-left"></a>
                                </div>
                                <h4 class="arialbold mt5">Produk baru</h4>
                            </div>
                            <div class="col-8 text-right">
                                <input type="submit" name="save" value="" class="btn btn-default simpan mr10">
                                <a href="#" class="btn btn-default close"></a>
                            </div>
                        </div>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="">
                <div class="block counter-block mb-12 col-sm-12 mb20">
                    <div class="bold title mb-20">
                        <h4 class="text-capitalize">Informasi Produk</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Gambar Produk <br>
                                <spon class="f-abu f11">Rekomendasi 2-4 Gambar produk.</spon>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-image abu">
                                <label for="imgInp">
                                    <img id="blah" alt="your image" src="img/icon/imgku.png" width="100px" />  
                                </label>
                                <input type="file" name="fupload" id="imgInp" style="display: none" />
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Nama Produk
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" class="form-control" width="80%" placeholder="..." name="nama_produk">
                                <!-- <span class="judul f-merah">
                                    Nama barang sudah digunakan
                                </span> -->
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Kode Produk
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" class="form-control" width="80%" placeholder="..." name="kode_produk">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Kategori
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" name='id_kategori'>
                                        <option>- Pilih Kategori -</option>
                                        <?php
                                            while ($k = mysqli_fetch_array($kategori)) {
                                                echo "<option value='$k[id_kategori]'>$k[nama_kategori]</option>";
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Type Produk
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" name="id_type">
                                        <option>- Pilih Type Produk -</option>
                                        <?php
                                            while ($k = mysqli_fetch_array($type)) {
                                                echo "<option value='$k[id_type]'>$k[nama_type]</option>";
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                                <div class="judul abu">
                                    Deskripsi
                                </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <textarea name="deskripsi" placeholder="Tulis deskripsi untuk keterangan produk ..."></textarea><br>
                                <p class="f11 f-abu">Max. 320 karakter</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block counter-block mb-12 col-sm-12 mb20">
                    <div class="bold title mb-20">
                        <h4>klien
                            <a href="add-klien-3.htm" class="btn btn-default pull-right iconplus"></a>
                        </h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <?php
                                    while ($s = mysqli_fetch_array($klien)) {
                                        echo "
                                            <div class='col-sm-4'>
                                                <label class='custom-control custom-radio'>
                                                    <input id='radioStacked3' name='klien[]' value='$s[id_klien]' class='custom-control-input' type='checkbox'>
                                                    <span class='custom-control-indicator'></span>
                                                    <span class='custom-control-description'>$s[nama_klien]</span>
                                                </label>
                                            </div>
                                        ";
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block counter-block mb-12 col-sm-12 mb20">
                    <div class="bold title mb-20">
                        <h4>Mesin <a href="add-mesin-4.htm" class="btn btn-default pull-right iconplus"></a></h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <?php
                                    while ($s = mysqli_fetch_array($mesin)) {
                                        echo "
                                            <div class='col-sm-4'>
                                                <label class='custom-control custom-radio'>
                                                    <input id='radioStacked3' name='mesin[]' value='$s[id_mesin]' class='custom-control-input' type='checkbox'>
                                                    <span class='custom-control-indicator'></span>
                                                    <span class='custom-control-description'>$s[nama_mesin]</span>
                                                </label>
                                            </div>
                                        ";
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block counter-block mb-12 col-sm-12 mb20">
                    <div class="bold title mb-20">
                        <h4>Keterangan stok</h4>
                        <!-- <div class="onoffswitch inp-switch pull-right">
                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
                            <label class="onoffswitch-label" for="myonoffswitch">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>

                        </div> -->
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Satuan Unit
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" name="id_unit">
                                        <option>- Pilih Satuan Unit -</option>
                                        <?php
                                            while ($k = mysqli_fetch_array($unit)) {
                                                echo "<option value='$k[id_unit]'>$k[nama_unit]</option>";
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Satuan Ukur
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" name="id_ukur" id="sat_ukur">
                                        <option>- Pilih Ukur Produk -</option>
                                        <?php
                                            while ($k = mysqli_fetch_array($ukur)) {
                                                echo "<option value='$k[id_ukur]'>$k[nama_ukur]</option>";
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Satuan stok keluar
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" name="keluar_produk">
                                        <option>- Pilih Satuan keluar -</option>
                                        <option value="unit">Unit</option>
                                        <option value="ukur">Ukur</option>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Minimum stok keluar
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu" style="width: 130px">
                                <input type="text" class="form-control" width="90px" placeholder="..." name="minim_produk" style="width: 90px">
                                <span class="judul satuan">
                                    Kg
                                </span>
                                <!-- <div class="onoffswitch msk pull-left">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="msk" checked>
                                    <label class="onoffswitch-label" for="msk">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div> -->
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Peringatan stok minimum
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu" style="width: 130px">
                                <input type="text" class="form-control" width="90px" placeholder="..." name="peringatan_produk" style="width: 90px">
                                <span class="judul satuan">
                                    Kg
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block counter-block mb-12 col-sm-12 mb20">
                    <div class="bold title mb-20">
                        <h4>Varian ukuran produk</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Satuan Ukur
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <div class="inp-text abu pull-left" style="width: 130px">
                                <input type="text" class="form-control" width="90px" value="1" name="" style="width: 90px">
                                <span class="judul">
                                    Kg
                                </span>
                            </div>
                            <i class="btn btn-default pull-left close ml10"></i>
                            <span class="judul f-merah mt5 ml10" style="position: absolute;">
                                Masih ada produk dengan berat tersebut
                            </span><br>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                        </div>
                        <div class="col-md-8 col-sm-6">
                            <a href="#" class="btn btn-primary">Semua</a>
                        </div>
                    </div>
                </div>


                <div class="col-md-12 col-sm-12 mb-100"></div>
            </div>
        </div>
    </div>
</form>