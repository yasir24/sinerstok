<?php
    function tulis_cekboxa($field,$koneksi,$judul) {
        $query ="select * from mesin order by nama_mesin";
        $r = mysqli_query($koneksi,$query);
        $_arrNilai = explode('+', $field);
        $str = '';
        $no=1;
        while ($w = mysqli_fetch_array($r)) {
            $_ck = (array_search($w[0], $_arrNilai) === false)? '' : 'checked';
            $str .= "
            <tr>
                <td class='abu'>$w[1]</td>
                <td>
                    <div class='checkbox'>
                        <label class='custom-control custom-checkbox' style='width: 100%'>
                            <input type='checkbox' name='mesin[]' id='checkItem' value='$w[0]' $_ck class='custom-control-input'>
                            <span class='custom-control-indicator'></span>
                        </label>
                    </div>
                </td>
            </tr> 
            ";
            $no++;
        }
        return $str;
    }

    $p     = mysqli_fetch_array(mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from produk p 
                                                  left join kategori k on p.id_kategori=k.id_kategori 
                                                  left join type t on p.id_type=t.id_type 
                                                  left join unit u on p.id_unit=u.id_unit 
                                                  left join ukur r on p.id_ukur=r.id_ukur 
                                                  LEFT JOIN stok s on p.produkId=id_produk
                                                  where produkId = $_GET[id]
                                                  GROUP by produkId 
                                                  order by produkId desc"));
?>

<form method="post" enctype="multipart/form-data" action="mod/<?php echo"$folder/aksi.php?mod=$mod&url=$_GET[url]&folder=$_GET[folder]"; ?>">
<input type="hidden" name="status" value="setmesin">
<input type="hidden" name="produk" value="<?=$_GET['id'];?>">
<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <ol class="breadcrumb">
                    <div class="row"> 
                        <div class="bold col-8">
                            <div class="pull-left mr20">
                                <a href="#" class="menukekiri pull-left"></a>
                                <a href="#" class="menukekanan pull-left"></a>
                            </div>
                            <h4 class="arialbold mt5"><?=$p['nama_produk'];?> - Stok Opname</h4>
                        </div>
                        <div class="bold col-4">
                            <input type="submit" class="pull-right btn btn-default w90 mr5" value='Update'>
                        </div>
                    </div>
                </ol>
            </div>
            <div class="col-12 col-md-12 d-lg-none">
                <div class="row">
                        <div class="cari">
                            <i class="fa fa-search"></i>
                            <input type="text" name="cari" placeholder="Search ...">
                        </div>

                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="">
            <div class="block counter-block mb-12 col-sm-12 mb20">
                <div class="bold title mb-20">
                    <h4 class="text-capitalize">Rekap stok produk</h4>
                </div>
                <div class=" mt20 p20">
                    <div class="row ">
                        <div class="col">
                            <table class="table mr10 tableku">
                                <tr>
                                    <th>@ukr <br> <?=$p['nama_unit'];?></th>
                                    <th>T. unit <br> buah</th>
                                    <th>T. ukr <br> <?=$p['nama_unit'];?></th>
                                </tr>
                                <?php
                                    $ukur = "";
                                    $total = "";
                                    $jumlah ="";
                                    $qwy = "SELECT *,SUM(stok_masuk) as jumlah, COUNT(ukuran) as total, s.tgl as tglstok   FROM `stok` s LEFT JOIN produk p on s.id_produk=p.produkId left join unit u on p.id_unit=u.id_unit LEFT join ukur uk on p.id_ukur=uk.id_ukur WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 GROUP by ukuran";
                                    $stok = mysqli_query($koneksi, $qwy);
                                    while ($s = mysqli_fetch_array($stok)) {
                                        $unit = $s['nama_unit'];
                                        $ukur .= "<option value='@$s[ukuran]'>@$s[ukuran]</option>";
                                        $total +=$s['total'];
                                        $jumlah +=$s['jumlah'];
                                        echo "
                                            <tr> 
                                                <td>$s[ukuran] </td>
                                                <td>$s[total]</td>
                                                <td>$s[jumlah]</td>
                                            </tr>
                                            
                                        ";    
                                    }
                                    
                                ?>
                                <tr>
                                    <td>Stok Terkini</td>
                                    <td><?=$total;?></td>
                                    <td><?=$jumlah;?></td>
                                </tr>
                            </table>
                        </div>
                        

                        <div class="col">
                            <table class="table mr10 tableku">
                                <thead>
                                    <tr>
                                        <th>@ukr <br> <?=$p['nama_unit'];?></th>
                                        <th>T. unit <br> buah</th>
                                        <th>T. ukr <br> <?=$p['nama_unit'];?></th>
                                    </tr>
                                </thead>
                                <tbody class='opnameganti'>
                                    <?php
                                        $ukur = "";
                                        $total = "";
                                        $jumlah ="";
                                        $qwy = "SELECT *,SUM(stok_masuk) as jumlah, COUNT(ukuran) as total, s.tgl as tglstok   FROM `stok` s LEFT JOIN produk p on s.id_produk=p.produkId left join unit u on p.id_unit=u.id_unit LEFT join ukur uk on p.id_ukur=uk.id_ukur WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 GROUP by ukuran";
                                        $stok = mysqli_query($koneksi, $qwy);
                                        while ($s = mysqli_fetch_array($stok)) {
                                            $unit = $s['nama_unit'];
                                            $ukur .= "<option value='@$s[ukuran]'>@$s[ukuran]</option>";
                                            $total +=$s['total'];
                                            $jumlah +=$s['jumlah'];
                                            echo "
                                                <tr> 
                                                    <td>$s[ukuran] </td>
                                                    <td>$s[total]</td>
                                                    <td>$s[jumlah]</td>
                                                </tr>
                                                
                                            ";    
                                        }                                    
                                    ?>
                                </tbody>
                                <tr>
                                    <td>Stok Opname</td>
                                    <td><?=$total;?></td>
                                    <td><input type="text" name="" id="totalakhir" value='<?=$jumlah;?>' style='width: 45px; border: none; background: no-repeat;'></td>
                                </tr>
                            </table>
                        </div>
                        
                    </div>
                    
                </div>
            </div>


            <div class="block counter-block mb-12 col-sm-12 mb20">
                <div class="bold title mb-20">
                    <h4 class="text-capitalize">Posisi stok</h4>
                </div>
                <div class=" mt20 p20">
                    <div class="row ">
                    <table class="tableku table">
                        <thead class="text-middle">
                            <tr>
                                <th width="20%">Seri</th>
                                <th width="6%" class="no-sort">Gdg</th>
                                <th width="6%" class="no-sort">Rak</th>
                                <th width="6%" class="no-sort">Box</th>
                                <th width="6%" class="no-sort">Tin</th>
                                <th width="6%" class="no-sort">Lbl</th>
                                <th width="7%" class="no-sort">Unit</th>
                                <th width="7%">Ukuran</th>
                                <th width="7%">Total Ukuran</th>
                                <th width="7%">W. Wdg<br> Hari</th>
                                <th>Exp</th>
                            </tr>
                        </thead>
                        <tbody class="text-middle text-capitalize">
                            <?php
                                $now = time(); 
                                $qwyku = "SELECT *, s.tgl as tglstok FROM `stok` s 
                                        LEFT JOIN produk p on s.id_produk=p.produkId 
                                        left join unit u   on p.id_unit=u.id_unit 
                                        LEFT join ukur uk  on p.id_ukur=uk.id_ukur
                                        LEFT JOIN gudang g on s.gudang=g.id_gudang
                                        LEFT JOIN rak r on s.rak=r.id_rak
                                        LEFT JOIN box b on s.box=b.id_box
                                        LEFT JOIN kalengplastik kp on s.kalengplastik=kp.id_kalengplastik
                                        WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 ORDER BY `s`.`exp`  ASC";
                                if (isset($_GET['type']) && $_GET['type'] == 'single') {
                                    $qwyku = "SELECT *, s.tgl as tglstok FROM `stok` s 
                                        LEFT JOIN produk p on s.id_produk=p.produkId 
                                        left join unit u   on p.id_unit=u.id_unit 
                                        LEFT join ukur uk  on p.id_ukur=uk.id_ukur
                                        LEFT JOIN gudang g on s.gudang=g.id_gudang
                                        LEFT JOIN rak r on s.rak=r.id_rak
                                        LEFT JOIN box b on s.box=b.id_box
                                        LEFT JOIN kalengplastik kp on s.kalengplastik=kp.id_kalengplastik
                                        WHERE `id_produk` = $_GET[produk] and stok_masuk <> 0 ORDER BY `s`.`exp`  ASC";
                                }
                                $stok = mysqli_query($koneksi, $qwyku);
                                // echo $qwyku;
                                // echo "<br><br>";
                                $total = "";
                                $totalku = "";


                                while ($s = mysqli_fetch_array($stok)) {

                                    
                                    $total += $s['stok_masuk'];

                                    // echo "total : ".$total;
                                    // echo "<br>";
                                    // echo "totalku :".$totalku;
                                    // echo "<br>";

                                
                                        $lamanya = strtotime($s['tglstok']);
                                        $lamadatediff =  ($now - $lamanya);
                                        $lama = round($lamadatediff / (60 * 60 * 24)) + 1;

                                        $expnya = strtotime($s['exp']);
                                        $expdatediff =  ($expnya - $now);
                                        $exp = round($expdatediff / (60 * 60 * 24));

                                        if ($exp < 1 ) {
                                                $warning = "f-merah";
                                        }
                                        else {
                                                $warning = "f-hijau";
                                        }
                                    
                                                echo "
                                                    <tr id='hapusku$s[id_stok]'>
                                                        <td>
                                                            ".date("Ymd", strtotime($s['tglstok']))."-$s[id_stok]
                                                        </td>
                                                        <td>$s[nama_gudang]</td>
                                                        <td>$s[nama_rak]</td>
                                                        <td>$s[nama_box]</td>
                                                        <td>$s[nama_kalengplastik]</td>
                                                        <td>-</td>
                                                        <td>$s[nama_unit]</td>
                                                        <td>@$s[ukuran]</td>
                                                        <td class='inp-text'><input type='text' class='form-control kecil opname' data-id='$s[id_stok]' value='$s[stok_masuk]' style='width: 65px'></td>
                                                        <td>".$lama."</td>
                                                        <td class='$warning'>";
                                                            if ($p['sort_nama'] !== 'single') {
                                                                echo date("d.m.Y", strtotime($s['exp']));
                                                            }
                                                            echo "
                                                            <div class='opnamedelete pull-right' data-produk='$_GET[id]' data-id='$s[id_stok]'><i class='btn btn-default delete'></i></div>
                                                            <div class='pull-right' id='opnamesave$s[id_stok]' data-hasil=''><i data-id='$s[id_stok]' data-produk='$_GET[id]' class='btn btn-default simpan mr10'></i></div>
                                                        </td>
                                                    </tr>
                                                ";
                                        }
                                        
                                
                            ?>
                        </tbody>
                    </table>
                        
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>
</form>