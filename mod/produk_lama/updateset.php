<form method="post" enctype="multipart/form-data" action="mod/<?php echo"$folder/aksi.php?mod=$mod&url=$_GET[url]&folder=$_GET[folder]"; ?>">
<input type="hidden" name="status" value="set">
<input type="hidden" name="produk" value="<?=$_GET['id'];?>">
<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="bold col-4">
                            <div class="pull-left mr20">
                                <a href="#" class="menukekiri pull-left"></a>
                                <a href="#" class="menukekanan pull-left"></a>
                            </div>
                            <h4 class="arialbold mt5">G9/305IMP-045 - set posisi</h4>
                        </div>
                        <div class="bold col-8">
                            <input type="submit" class="pull-right btn btn-default w90 mr5" value='Update'>
                        </div>
                    </div>
                </ol>
            </div>
            <div class="col-12 col-md-12 d-lg-none">
                <div class="row">
                        <div class="cari">
                            <i class="fa fa-search"></i>
                            <input type="text" name="cari" placeholder="Search ...">
                        </div>

                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="">
            <div class="block counter-block mb-12 col-sm-12 mb20">
                <div class="bold title mb-20">
                    <h4 class="text-capitalize">Set Produk - Update</h4>
                </div>
                <div class="row mt20">
                    <div class="table-responsive text-no-wrap">
                        <table class="table tableku" id="dataTable1" data-table="data-table-search">
                            <thead class="text-middle">
                                <tr>
                                    <th width="7%" class="no-sort ysku"></th>
                                    <th width="25%">Kode</th>
                                    <th width="33%">Nama</th>
                                    <th width="11%" class="no-sort">Kategori</th>
                                    <th width="5%" class="no-sort">Qty</th>
                                    <th width="5%" class="no-sort">Stn</th>
                                    <th width="5%" class="no-sort">
                                        <div class='checkbox'>
                                            <label class='custom-control custom-checkbox' style='width: 100%'>
                                                <input type='checkbox' id="checkAll" class='custom-control-input'>
                                                <span class='custom-control-indicator'></span>
                                            </label>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="text-middle">
                            <?php
                                $produkku   = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from produk p 
                                                left join kategori k on p.id_kategori=k.id_kategori 
                                                left join type t on p.id_type=t.id_type 
                                                left join unit u on p.id_unit=u.id_unit 
                                                left join ukur r on p.id_ukur=r.id_ukur 
                                                LEFT JOIN stok s on p.produkId=id_produk
                                                where p.id_type <> 4
                                                GROUP by produkId
                                                order by produkId desc");
                                while($p = mysqli_fetch_array($produkku)){
                                    echo"
                                        <tr>
                                            
                                            <td class='text-center'>
                                                <img src='assets\images\produk/$p[gambar_produk]' style='border-radius: 5px' width='30px'>
                                            </td>
                                            <td>$p[kode_produk]</td>
                                            <td>$p[nama_produk]</td>
                                            <td class='abu'>$p[nama_kategori]</td>
                                            <td class='f-hijau'>$p[total]</td>
                                            <td class='abu'>$p[nama_unit]</td>
                                            
                                            <td>
                                                <div class='checkbox'>
                                                    <label class='custom-control custom-checkbox' style='width: 100%'>
                                                        <input type='checkbox' name='idpro[]' id='checkItem' value='$p[produkId]'"; 
                                                            $u = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from paket where id_produk = $_GET[id] and produk_id = $p[produkId]"));
                                                            if($u == 1){
                                                                echo "checked ";
                                                            }                                                        
                                                        echo" class='custom-control-input'>
                                                        <span class='custom-control-indicator'></span>
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    ";
                                }
                            ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
                            </form>