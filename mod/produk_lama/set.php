<div class="mb-12 mb20">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <a href="updateset-produk-<?=$_GET['id'];?>-2.htm" class="pull-right btn btn-danger w70 mr10 ml10">Update</a>
            <div class="cari pull-right ml10">
                <i class="iconcari"></i>
                <input type="text" id="input-filter" name="cari" placeholder="Search ...">
            </div>            
        </div>
        <div class="table-responsive text-no-wrap mt10">
            <table class="table " id="dataTable1" data-table="data-table-polos-disfirst">
                <thead class="text-middle">
                    <tr>
                        <th width="7%" class="no-sort ysku"></th>
                        <th width="20%">Kode</th>
                        <th width="38%">Nama</th>
                        <th width="11%" class="no-sort">Kategori</th>
                        <th width="5%" class="no-sort">Qty</th>
                        <th width="5%" class="no-sort">Stn</th>
                    </tr>
                </thead>
                <tbody class="text-middle">
                    <?php 
                        $pak = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from paket p 
                                                        left join produk pr on p.produk_id=pr.produkId 
                                                        left join unit u on pr.id_unit=u.id_unit 
                                                        left join type t on pr.id_type=t.id_type 
                                                        left join kategori k on pr.id_kategori=k.id_kategori 
                                                        LEFT JOIN stok s on pr.produkId=s.id_produk 
                                                        where p.id_produk = $_GET[id] GROUP by produkId");
                        while($p = mysqli_fetch_array($pak)){
                            echo"
                                <tr class='clickable' data-href='detail-produk-$p[produkId]-2.htm'>
                                    <td class='text-center'>
                                        <img src='assets\images\produk/$p[gambar_produk]' style='border-radius: 5px' width='30px'>
                                    </td>
                                    <td>$p[kode_produk]</td>
                                    <td>$p[nama_produk]</td>
                                    <td class='abu'>$p[nama_kategori]</td>
                                    <td class='f-hijau'>$p[total]</td>
                                    <td class='abu'>$p[nama_unit]</td>
                                </tr>
                            ";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
        