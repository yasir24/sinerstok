
<div class="row mb40">
    <div class="col-md-12 col-sm-12">
        <div class="cari pull-right ml10" style='margin-top:-3px'>
            <i class="iconcari"></i>
            <input type="text" id="input-filter" name="cari" placeholder="Search ...">
        </div>
        <span class="select-yasir pull-right mb20">
            <?php
                $ukur = "";
                $qwy = "SELECT *,SUM(stok_masuk) as jumlah, (SUM(stok_masuk)/ukuran) as total, s.tgl as tglstok  FROM `stok` s LEFT JOIN produk p on s.id_produk=p.produkId left join unit u on p.id_unit=u.id_unit LEFT join ukur uk on p.id_ukur=uk.id_ukur WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 GROUP by ukuran";
                $stok = mysqli_query($koneksi, $qwy);
                while ($s = mysqli_fetch_array($stok)) {
                    $ukur .= "<option value='@$s[ukuran]'>@$s[ukuran]</option>";
                }
            ?>
            <select class="btn btn-yasir"  id="table-filter">
                <option value="">- @ukuran -</option>
                <?=$ukur;?>
            </select>
        </span>
        <div class="table-responsive text-no-wrap mt10">
            <table class="table" id="dataTable1" data-table="data-table-polos-disfirst">
                <thead class="text-middle">
                    <tr>
                        <th width="20%">Seri</th>
                        <th width="6%" class="no-sort">Gdg</th>
                        <th width="6%" class="no-sort">Rak</th>
                        <th width="6%" class="no-sort">Box</th>
                        <th width="6%" class="no-sort">Tin</th>
                        <th width="6%" class="no-sort">Lbl</th>
                        <th width="7%" class="no-sort">Unit</th>
                        <th width="7%">Ukuran</th>
                        <th width="7%">Total Ukuran</th>
                        <th width="7%">W. Wdg Hari</th>
                        <?php if ($p['sort_nama'] !== 'single') {
                            echo"<th width='10%'>Exp</th>";
                        }?>
                    </tr>
                </thead>
                <tbody class="text-middle text-capitalize">
                    <?php
                        $now = time(); 
                        $qwyku = "SELECT *, s.tgl as tglstok FROM `stok` s 
                                LEFT JOIN produk p on s.id_produk=p.produkId 
                                left join unit u   on p.id_unit=u.id_unit 
                                LEFT join ukur uk  on p.id_ukur=uk.id_ukur
                                LEFT JOIN gudang g on s.gudang=g.id_gudang
                                LEFT JOIN rak r on s.rak=r.id_rak
                                LEFT JOIN box b on s.box=b.id_box
                                LEFT JOIN kalengplastik kp on s.kalengplastik=kp.id_kalengplastik
                                WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 ORDER BY `s`.`exp`  ASC";
                        if (isset($_GET['type']) && $_GET['type'] == 'single') {
                            $qwyku = "SELECT *, s.tgl as tglstok FROM `stok` s 
                                LEFT JOIN produk p on s.id_produk=p.produkId 
                                left join unit u   on p.id_unit=u.id_unit 
                                LEFT join ukur uk  on p.id_ukur=uk.id_ukur
                                LEFT JOIN gudang g on s.gudang=g.id_gudang
                                LEFT JOIN rak r on s.rak=r.id_rak
                                LEFT JOIN box b on s.box=b.id_box
                                LEFT JOIN kalengplastik kp on s.kalengplastik=kp.id_kalengplastik
                                WHERE `id_produk` = $_GET[produk] and stok_masuk <> 0 ORDER BY `s`.`exp`  ASC";
                        }
                        $stok = mysqli_query($koneksi, $qwyku);
                        // echo $qwyku;
                        // echo "<br><br>";
                        $total = "";
                        $totalku = "";


                        while ($s = mysqli_fetch_array($stok)) {

                            
                            $total += $s['stok_masuk'];

                            // echo "total : ".$total;
                            // echo "<br>";
                            // echo "totalku :".$totalku;
                            // echo "<br>";

                        
                                $lamanya = strtotime($s['tglstok']);
                                $lamadatediff =  ($now - $lamanya);
                                $lama = round($lamadatediff / (60 * 60 * 24)) + 1;

                                $expnya = strtotime($s['exp']);
                                $expdatediff =  ($expnya - $now);
                                $exp = round($expdatediff / (60 * 60 * 24));

                                if ($exp < 1 ) {
                                        $warning = "f-merah";
                                }
                                else {
                                        $warning = "f-hijau";
                                }
                            
                                        echo "
                                            <tr>
                                                <td>
                                                    ".date("Ymd", strtotime($s['tglstok']))."-$s[id_stok]
                                                </td>
                                                <td>$s[nama_gudang]</td>
                                                <td>$s[nama_rak]</td>
                                                <td>$s[nama_box]</td>
                                                <td>$s[nama_kalengplastik]</td>
                                                <td>-</td>
                                                <td>$s[nama_unit]</td>
                                                <td>@$s[ukuran]</td>
                                                <td>$s[stok_masuk]</td>
                                                <td>".$lama."</td>";
                                                if ($p['sort_nama'] !== 'single') {
                                                    echo "<td class='$warning'>".date("d.m.Y", strtotime($s['exp']))."</td>";
                                                }
                                                echo "
                                            </tr>
                                        ";
                                }
                                
                        
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>