<?php 
    date_default_timezone_set('Asia/Jakarta');
    $pro = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from produk p 
                                                  left join kategori k on p.id_kategori=k.id_kategori 
                                                  left join type t on p.id_type=t.id_type 
                                                  left join unit u on p.id_unit=u.id_unit 
                                                  left join ukur r on p.id_ukur=r.id_ukur 
                                                  LEFT JOIN stok s on p.produkId=id_produk
                                                  where produkId = '$_GET[id]'
                                                  GROUP by produkId
                                                  order by produkId desc");
    $p               = mysqli_fetch_array($pro);
    
    $ukuranku        = mysqli_num_rows(mysqli_query($koneksi, "SELECT ukuran,tgl FROM `stok` WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 GROUP by ukuran"));
    $lama            = mysqli_fetch_array(mysqli_query($koneksi, "SELECT tgl FROM `stok` WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 order by tgl asc"));

    $now             = time();
    $lamanya         = strtotime($lama['tgl']);
    $lamadatediff    =  ($now - $lamanya);
    $lamaku          = round($lamadatediff / (60 * 60 * 24)) - 1;

    $ot              = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] ORDER BY `tgl` desc limit 1"));
    $lamanya_ot      = strtotime($ot['tgl']);
    $lamadatediff_ot =  ($now - $lamanya_ot);
    $lamaku_ot       = round($lamadatediff_ot / (60 * 60 * 24)) -1;

    $ohi             = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] and tgl = CURDATE()"));
    $obi             = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] and MONTH(tgl) = MONTH(CURRENT_DATE())"));
    $oyi             = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] and YEAR(tgl) = YEAR(CURRENT_DATE())"));
    $ratas           = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] and YEAR(tgl) = YEAR(CURRENT_DATE())")) / 12;

    $rata            = round($ratas, 1);
?>
<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="bold col-6">
                            <div class="pull-left mr20">
                                <a href="#" class="menukekiri pull-left"></a>
                                <a href="#" class="menukekanan pull-left"></a>
                            </div>
                            <h4 class="arialbold mt5"><?php echo $p['nama_produk']; ?></h4>
                        </div>
                        <div class=" col-6">
                            
                            <a href="#" class="btn icondownload pull-right"></a>
                            <a href="opname-<?=$p['sort_nama'];?>-<?=$p['produkId'];?>-2.htm" class="btn iconchange mr10 pull-right"></a>
                            <a href="update-produk-<?=$p['produkId'];?>-2.htm" class="btn btn-default update mr10 pull-right"></a>
                            <a href="add-stok-<?=$p['sort_nama'];?>-<?=$p['produkId'];?>-2.htm" class="pull-right btn btn-default keluar mr10"></a>
                            <a href="out-stok-<?=$p['sort_nama'];?>-<?=$p['produkId'];?>-2.htm" class="pull-right btn btn-default masuk mr10"></a>
                        </div>
                    </div>

                </ol>
            </div>
            <div class="col-12 col-md-12 d-lg-none">
                <div class="row">
                    <form class="col-12 text-right">
                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">

        <div class="block counter-block mb-12 col-sm-12 mb-20">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <img alt="playgroundku" src="assets/images/produk/<?php echo $p['gambar_produk']; ?>" style="border-radius: 5px;" width="100%"></a>
                </div>
                <div class="col-md-9 col-sm-6">
                    <div class="detailkanan">
                        <div class="judulku"><?php echo $p['nama_produk']; ?></div><br>
                        <ul>
                            <li>
                                <span class="abu w100" style="display: inline-block;">Code</span>
                                <span><?php echo $p['kode_produk']; ?></span>
                            </li>
                            <li>
                                <span class="abu w100" style="display: inline-block;">Set Produk</span>
                                <span class="f-biru"><?php echo $p['nama_produk']; ?></span></li>
                            <li>
                                <span class="abu w100" style="display: inline-block;">Kategori</span>
                                <span class="text-capitalize"><?php echo $p['nama_kategori']; ?></span></li>
                            <li>
                                <span class="abu w100" style="display: inline-block;">Type Produk</span>
                                <span><?php echo $p['nama_type']; ?></span>
                            </li>
                            <li>
                                <span class="abu w100" style="display: inline-block;">Sat. Keluar</span>
                                <span><?php echo $p['nama_ukur']; ?></span>
                            </li>
                            <li><br></li>
                            <li>
                                <span class="abu w100" style="display: inline-block;">Terakhir update</span>
                                <span class="abu">
                                    <?php 
                                        if ($p['uptgl'] == '0000-00-00') {
                                            echo "-";
                                        }
                                        else {
                                            echo $p['uptgl'];
                                        }  
                                    ?>
                                </span>
                            </li>
                        </ul>
                        <div class="col-sm-12 mb-10"></div>
                    </div>
                </div>
            </div>
        </div>

        
        <div class="mb-12 col-sm-12 mb-10">
            <div class="row">
                <a href="detail-produk-<?=$_GET['id'];?>-2.htm" class="btn <?php echo ($_GET['page'] == 'info') ? 'btn-primary' : 'btn-default';?>  w70 mr10 ">Info</a>
                <a href="posisi-produk-<?=$_GET['id'];?>-2.htm" class="btn <?php echo ($_GET['page'] == 'posisi') ? 'btn-primary' : 'btn-default';?> w70 mr10">Posisi</a>
                <a href="set-produk-<?=$_GET['id'];?>-2.htm" class="btn <?php echo ($_GET['page'] == 'set') ? 'btn-primary' : 'btn-default';?> w70 mr10">Set</a>
                <a href="order-produk-<?=$_GET['id'];?>-2.htm"  class="btn <?php echo ($_GET['page'] == 'order') ? 'btn-primary' : 'btn-default';?> w70 mr10">Order</a>
                <!-- <a href="#" class="btn btn-default w70 mr10">User</a> -->

                 <div class="mb30 col-sm-12"></div>
            </div>
        </div>

        <?php
            include $_GET['page'].".php";
        ?>

    </div>
</div>
