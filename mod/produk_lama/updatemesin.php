<?php
    function tulis_cekboxa($field,$koneksi,$judul) {
        $query ="select * from mesin order by nama_mesin";
        $r = mysqli_query($koneksi,$query);
        $_arrNilai = explode('+', $field);
        $str = '';
        $no=1;
        while ($w = mysqli_fetch_array($r)) {
            $_ck = (array_search($w[0], $_arrNilai) === false)? '' : 'checked';
            $str .= "
            <tr>
                <td class='abu'>$w[1]</td>
                <td>
                    <div class='checkbox'>
                        <label class='custom-control custom-checkbox' style='width: 100%'>
                            <input type='checkbox' name='mesin[]' id='checkItem' value='$w[0]' $_ck class='custom-control-input'>
                            <span class='custom-control-indicator'></span>
                        </label>
                    </div>
                </td>
            </tr>
            ";
            $no++;
        }
        return $str;
    }
?>
<form method="post" enctype="multipart/form-data" action="mod/<?php echo"$folder/aksi.php?mod=$mod&url=$_GET[url]&folder=$_GET[folder]"; ?>">
<input type="hidden" name="status" value="setmesin">
<input type="hidden" name="produk" value="<?=$_GET['id'];?>">
<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <ol class="breadcrumb">
                    <div class="row"> 
                        <div class="bold col-4">
                            <div class="pull-left mr20">
                                <a href="#" class="menukekiri pull-left"></a>
                                <a href="#" class="menukekanan pull-left"></a>
                            </div>
                            <h4 class="arialbold mt5">Update Mesin</h4>
                        </div>
                        <div class="bold col-8">
                            <input type="submit" class="pull-right btn btn-default w90 mr5" value='Update'>
                        </div>
                    </div>
                </ol>
            </div>
            <div class="col-12 col-md-12 d-lg-none">
                <div class="row">
                        <div class="cari">
                            <i class="fa fa-search"></i>
                            <input type="text" name="cari" placeholder="Search ...">
                        </div>

                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="">
            <div class="block counter-block mb-12 col-sm-12 mb20">
                <div class="bold title mb-20">
                    <h4 class="text-capitalize">Set Produk - Update</h4>
                </div>
                <div class="row mt20">
                    <div class="table-responsive text-no-wrap">
                        <table class="table tableku" id="dataTable1" data-table="data-table-search">
                            <thead class="text-middle">
                                <tr>
                                    <th>Nama</th>
                                    <th width="5%" class="no-sort">
                                        <div class='checkbox'>
                                            <label class='custom-control custom-checkbox' style='width: 100%'>
                                                <input type='checkbox' id="checkAll" class='custom-control-input'>
                                                <span class='custom-control-indicator'></span>
                                            </label>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="text-middle">
                            <?php
                                $pku = mysqli_fetch_array(mysqli_query($koneksi,"select * from produk where produkId = $_GET[id]"));
                                    $fasilitas = tulis_cekboxa($pku['mesin'],$koneksi,'mesin');
                                    echo "$fasilitas";
                            ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
                            </form>