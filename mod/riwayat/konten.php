<?php 
    $pro = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total, SUM(s.ukuran) as total_ukuran  from produk p
                                                  left join kategori k on p.id_kategori=k.id_kategori
                                                  left join type t on p.id_type=t.id_type
                                                  left join unit u on p.id_unit=u.id_unit
                                                  left join ukur r on p.id_ukur=r.id_ukur
                                                  LEFT JOIN stok s on p.produkId=s.id_produk
                                                  GROUP by produkId
                                                  order by produkId desc");
    
    $p               = mysqli_fetch_array($pro);

?>
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 0px ">
            <div class="row">
                <div class="col-lg-12 d-none d-lg-block">

                    <?php
                        $aktif = "riwayat";
                        include "inc/menu.php";
                    ?>

                </div>            
            </div>
        </div>
    </div>
<div class="content sm-gutter">
<div class="row mb40">
    
    <div class="col-md-12 col-sm-12">
        
        
        <div class="table-responsive text-no-wrap mt10">
            <table class="table" id="dataTable1" data-table="data-table-polos-disfirst">
                <thead class="text-middle">
                    <tr>
                        <th width="13%" class="no-sort">Transaksi</th>
                        <th width="5%" class="no-sort">PRODUK</th>
                        <th width="15%" class="no-sort">No. SJ</th>
                        <th width="22%" class="no-sort">Klien</th>
                        <th width="5%" class="no-sort">Last</th>
                        <th width="5%" class="no-sort">IN</th>
                        <th width="5%" class="no-sort">OUT</th>
                        <th width="5%" class="no-sort">Qty</th>
                        <th width="10%" class="no-sort">Date</th>
                    </tr>
                </thead>
                <tbody class="text-middle">
                    <?php 
                        if ($p['nama_type'] == 'Paket') {

                            $feeda       = mysqli_query($koneksi, "SELECT * FROM `feed` f LEFT JOIN status_feed s on f.status=s.id_status ORDER BY `id_feed` DESC");
                            
                            while($fee = mysqli_fetch_array($feeda)){
                                $feedaku       = mysqli_query($koneksi, "SELECT * FROM `feed` f LEFT JOIN status_feed s on f.status=s.id_status where id = $fee[id] ORDER BY `id_feed` DESC");
                                while($f=mysqli_fetch_array($feedaku)){
                                    if ($f['page'] == 'stok' && $f['status'] == 1) {
                                        $fku = mysqli_query($koneksi, "SELECT * from stok s 
                                                                        left join produk p on s.id_produk=p.produkId  
                                                                        left join ukur u on p.id_ukur=u.id_ukur
                                                                        left join klien sup on s.id_klien=sup.id_klien 
                                                                        where id_stok    = $f[id]");

                                        while ($fk = mysqli_fetch_array($fku)) {
                                            $real = $f['sebelum'] - $f['sesudah'];
                                            echo "
                                                <tr>
                                                    <td>$f[nama_st] $f[page]</td>
                                                    <td>$fk[nama_produk]</td>
                                                    <td>$fk[surat_jalan]</td>
                                                    <td>$fk[nama_klien]</td>
                                                    <td>-</td>
                                                    <td class='f-hijau'>$f[sesudah]</td>
                                                    <td class='f-merah'>0</td>
                                                    <td class='f-hijau'>$f[sesudah]</td>
                                                    <td>".date("d.m.Y", strtotime($f['tgl']))."</td>
                                                </tr>
                                            ";
                                        }    
                                    }
                                    if ($f['page'] == 'stok' && $f['status'] == 3) {
                                        $fout = mysqli_query($koneksi, "SELECT * from stokout s 
                                                                        left join produk p on s.id_produk=p.produkId  
                                                                        left join ukur u on p.id_ukur=u.id_ukur
                                                                        left join klien sup on s.id_klienout=sup.id_klien 
                                                                        where id_stokout = $f[id]");
                                        while ($fo = mysqli_fetch_array($fout)) {
                                            $real = $f['sebelum'] - $f['sesudah'];
                                            echo "
                                                <tr>
                                                    <td>$f[nama_st] $f[page]</td>
                                                    <td>$fo[nama_produk]</td>
                                                    <td>$fo[surat_jalan]</td>
                                                    <td>$fo[nama_klien]</td>
                                                    <td>$f[sebelum]</td>
                                                    <td class='f-hijau'>-</td>
                                                    <td class='f-merah'>$f[sesudah]</td>
                                                    <td class='f-hijau'>-</td>
                                                    <td>".date("d.m.Y", strtotime($f['tgl']))."</td>
                                                </tr>
                                            ";
                                        }    
                                    }
                                                                        
                                }
                            }
                        }
                        else{
                            // echo "SELECT * FROM `feed` f LEFT JOIN status_feed s on f.status=s.id_status where id_produk = $_GET[id] ORDER BY `id_feed` DESC";
                            $feeda       = mysqli_query($koneksi, "SELECT * FROM `feed` f LEFT JOIN status_feed s on f.status=s.id_status where id_produk = $_GET[id] ORDER BY `id_feed` DESC");
                            while($f=mysqli_fetch_array($feeda)){
                                if ($f['page'] == 'stok' && $f['status'] == 1) {
                                    $fku = mysqli_query($koneksi, "SELECT * from stok s 
                                                                    left join produk p on s.id_produk=p.produkId  
                                                                    left join ukur u on p.id_ukur=u.id_ukur
                                                                    left join klien sup on s.id_klien=sup.id_klien 
                                                                    where id_stok = $f[id]");
                                    while ($fk = mysqli_fetch_array($fku)) {
                                        $real = $f['sebelum'] - $f['sesudah'];
                                        echo "
                                            <tr>
                                                <td>$f[nama_st] $f[page]</td>
                                                <td>$fk[nama_produk]</td>
                                                <td>$fk[surat_jalan]</td>
                                                <td>$fk[nama_klien]</td>
                                                <td>-</td>
                                                <td class='f-hijau'>$f[sesudah]</td>
                                                <td class='f-merah'>0</td>
                                                <td class='f-hijau'>$f[sesudah]</td>
                                                <td>".date("d.m.Y", strtotime($f['tgl']))."</td>
                                            </tr>
                                        ";
                                    }    
                                }
                                if ($f['page'] == 'stok' && $f['status'] == 3) {

                                    
                                    $fout = mysqli_query($koneksi, "SELECT * from stokout s 
                                                                    left join produk p on s.id_produk=p.produkId  
                                                                    left join ukur u on p.id_ukur=u.id_ukur
                                                                    left join klien sup on s.id_klien=sup.id_klien 
                                                                    where id_stokout = $f[id]");
                                    while ($fo = mysqli_fetch_array($fout)) {
                                        $real = $f['sebelum'] - $f['sesudah'];
                                        echo "
                                            <tr>
                                                <td>$f[nama_st] $f[page]</td>
                                                <td>$fo[nama_produk]</td>
                                                <td>$fo[surat_jalan]</td>
                                                <td>$fo[nama_klien]</td>
                                                <td>$f[sebelum]</td>
                                                <td class='f-hijau'>-</td>
                                                <td class='f-merah'>$f[sesudah]</td>
                                                <td class='f-hijau'>$real</td>
                                                <td>".date("d.m.Y", strtotime($f['tgl']))."</td>
                                            </tr>
                                        ";
                                    }    
                                }
                                                                    
                            }
                        }
                        
                        ?>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>