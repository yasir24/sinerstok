<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">

                <?php
                    $aktif = "master";
                    include "inc/menu.php";
                ?>

            </div>            
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="row">
            <div class="col-12 text-left">
                <span class="select-yasir">
                    <select class="btn btn-yasir table-filter">
                        <option value="">All master</option>
                        <option value='GUDANG_'>GUDANG</option>
                        <option value='RAK_'>RAK</option>
                        <option value='BOX_'>BOX</option>
                        <option value='KALENG_'>KALENG</option>
                        <option value='UKURAN_'>UKURAN</option>
                        <option value='UNIT_'>UNIT</option>
                        <option value='KLIEN_'>KLIEN</option>
                        <option value='MESIN_'>MESIN</option>
                    </select>
                </span>  
                <span class="mt5 abu" style="margin-top: 5px;position: absolute;margin-left: 10px;">
                    <span class="cir-bold f14">
                        <?php
                            echo $num = mysqli_num_rows($femas);
                        ?>
                    </span> Produk ditemukan 
                </span>      
                <!-- <a href="addgudang-<?=$folder;?>-2.htm" class="ml10 btn iconplus pull-right"></a> -->
                <div class="btn btn-primary pull-right h24 w100 ml10" data-target="#exampleModal1" data-toggle="modal">Add</div>
                
                
                <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal1" role="dialog" tabindex="-1">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="mb40"></div>
                                <div class="row">
                                    <div class="col-sm-12 table-responsive text-no-wrap text-center">
                                        <a href="addgudang-gudang-5.htm" class='btn btn-primary w100 mr10 f-putih'>GUDANG</a>
                                        <a href="addgudang-rak-5.htm" class='btn btn-primary w100 mr10 f-putih'>RAK</a>
                                        <a href="addgudang-box-5.htm" class='btn btn-primary w100 mr10 f-putih'>BOX</a>
                                        <a href="addgudang-kalengplastik-5.htm" class='btn btn-primary w100 f-putih'>KALENG</a>
                                    </div>
                                    <div class="col-sm-12 mt15 table-responsive text-no-wrap text-center">
                                        <a href="addgudang-ukur-7.htm" class='btn btn-primary w100 mr10 f-putih'>UKURAN</a>
                                        <a href="addgudang-unit-6.htm" class='btn btn-primary w100 mr10 f-putih'>UNIT</a>
                                        <a href="add-klien-3.htm" class='btn btn-primary w100 mr10 f-putih'>KLIEN</a>
                                        <a href="addgudang-mesin-4.htm" class='btn btn-primary w100 f-putih'>MESIN</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="cari pull-right">
                    <i class="iconcari"></i>
                    <input type="text" id="input-filter" name="cari" placeholder="Kata kunci ...">
                </div>
                <span class="select-yasir pull-right">
                    <select class="btn btn-yasir"  id="urutkan" style="margin-top:0">
                        <option value="0">- Urutkan -</option>
                        <option value="1">Nama</option>
                    </select>
                </span>
                <hr class='pull-left' style="width: 100%;">
            </div>
            <div class="col-sm-12 table-responsive text-no-wrap">
                <table class="table" id="dataTable1" data-table="data-table-polos-disfirst">
                    <thead class="text-middle">
                        <tr>
                            <th width="57%" class="no-sort">Master</th>
                            <th class="no-sort">Kategori</th>
                            <th class="no-sort" width="10%"></th>
                        </tr>
                    </thead>
                    <tbody class="text-middle">
                        <?php
                            $se = mysqli_query($koneksi, "SELECT * from femas order by id_femas desc");
                            while ($s = mysqli_fetch_array($se)) { 
                                if ($s['kategori'] == 'klien') {
                                    echo"<tr class='clickable' data-href='edit-klien-$s[id]-2.htm'>";
                                }
                                else{
                                    echo"<tr class='clickable' data-href='editgudang-$s[kategori]-$s[id]-2.htm'>";
                                }
                        ?>                                
                        
                            <td style="height: 50px;"><?php echo $s['nama']; ?></td>
                            <td style="height: 50px;"><?php echo $s['kategori']; ?>_</td>
                            <td style="height: 50px;">
                                <?php
                                if ($s['kategori'] == 'klien') {
                                    echo "
                                    <a onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\" href='mod/klien/aksi.php?mod=mod&id=$s[id]&kat=3&url=3&folder=klien' class='btn btn-default pull-right close'></a>
                                    ";
                                }
                                else{
                                    echo "
                                    <a onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\" href='mod/gudang/aksi.php?id=$s[id]&kat=3&folder=$s[kategori]' class='btn btn-default pull-right close'></a>
                                    ";
                                }
                                ?>
                                
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="col-12">
                
                
            <div class="mb40"></div>
            </div>
        </div>
    </div>
</div>
