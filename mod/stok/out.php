<input type="hidden" name="statusstok" value="out">
<?php 
    if($p['id_type']<>'4'){
?>
<input type="hidden" name="stok_keluar" value="<?=$p['keluar_produk'];?>">
<div class="counter-block mb-12 col-sm-12 mb-20 p0">
    <h4 class="arialbold mt5 mb30 text-capitalize">Lokasi stok masuk</h4>
    <div class="table-responsive text-no-wrap">
        <table class="table">
            <thead class="text-middle">
                <tr>
                    <th width="105px" class="no-sort">Gudang</th>
                    <th width="105px" class="no-sort">Rak</th>
                    <th width="105px" class="no-sort">Box</th>
                    <th width="105px" class="no-sort">Kaleng</th>
                    <th width="170px" class="no-sort">No. Batch</th>
                    <th width="140px" class="no-sort">Label</th>
                    <th width="140px" class="no-sort">Exp</th>
                    <th width="95px" class="no-sort">Unit</th>
                    <th width="84px" class="no-sort">Ukur</th>
                    <th width="150px" class="no-sort">Keluar</th>
                </tr>
            </thead>
            <tbody class="text-middle text-capitalize" id="tempat">
                <?php 
                    $stok = mysqli_query($koneksi, "SELECT * FROM stok s
                                                    left join gudang g on s.gudang=g.id_gudang
                                                    left join rak r     on s.rak=r.id_rak
                                                    left join box b     on s.box=b.id_box
                                                    left join kalengplastik kp on s.kalengplastik=kp.id_kalengplastik
                                                    where id_produk = $_GET[id] and stok_masuk <> 0");
                    while($s=mysqli_fetch_array($stok)){
                        if($p['keluar_produk'] == 'ukur'){
                            $tot    = $s['ukuran'];
                            $maxku  = $s['ukuran'];
                            $masuk  = $s['ukuran'];
                            $varian = $p['varian'];
                        }
                        else{
                            $varian = $p['varian'];
                            $tot    = $s['stok_masuk'] * $p['varian'];
                            $masuk  = $s['stok_masuk'];
                            $maxku  = $s['stok_masuk'];
                        }
                        echo"
                            <tr>
                                <td>
                                    $s[nama_gudang]
                                    <input type='hidden' value='$s[id_stok]' name='id_stokku[]'>
                                    <input type='hidden' value='$s[gudang]' name='gudangku[]'>
                                    <input type='hidden' value='$s[rak]' name='rakku[]'>
                                    <input type='hidden' value='$s[box]' name='boxku[]'>
                                    <input type='hidden' value='$s[kalengplastik]' name='kalengplastikku[]'>
                                    <input type='hidden' placeholder='$s[batch]' name='nobatchku[]'>
                                    <input type='hidden' placeholder='$s[label]' name='labelku[]'>
                                    <input type='hidden' value='' placeholder='$s[exp]' name='expku[]'>
                                    <input type='hidden' value='$masuk' name='stok_masuk[]'>
                                </td>
                                <td>
                                    $s[nama_rak]
                                </td>
                                <td>
                                    $s[nama_box]
                                </td>
                                <td>
                                    $s[nama_kalengplastik]
                                </td>
                                <td>
                                    $s[batch]
                                </td>
                                <td>
                                    $s[label]                                         
                                </td>
                                <td>
                                    $s[exp]                        
                                </td>
                                <td>$s[stok_masuk]</td>
                                <td>$tot</td>
                                <td>
                                    <div class='inp-text abu' style='width:80px'>
                                        <input type='text' style='width:80px' class='form-control keluarku' data-type='$p[keluar_produk]' data-maxku='$maxku' placeholder='...' name='stok_out[]'  data-varian='$varian' >
                                    </div>                                                
                                </td>
                            </tr>
                        ";
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>

    <?php } else{ ?>

<div class="counter-block mb-12 col-sm-12 mb-20 pt0 pl0">
    <h4 class="arialbold mt5 mb30 text-capitalize">Lokasi produk set keluar</h4>
    <div class="table-responsive text-no-wrap">
        <table class="table" style="width: 1200px">
            <thead class="text-middle">
                <tr>
                    <th width="7%" class="no-sort ysku"></th>
                    <th width="160px" class="no-sort">Kode</th>
                    <th width="330px" class="no-sort">Nama</th>
                    <th width="130px" class="no-sort">Kategori</th>
                    <th width="130px" class="no-sort">Qty</th>
                    <th width="5%" class="no-sort">Varian @</th>
                    <th class="no-sort"></th>
                </tr>
            </thead>
            <tbody class="text-middle text-capitalize" id="tempat">
                <?php 

                    $produk      = mysqli_query($koneksi, "SELECT *,k.id_produk as produkku, SUM(see.stok_masuk) as total , p.klien as klienku FROM `paket` k 
                    left join produk p on k.produk_id=p.produkId
                    left join type t on p.id_type=t.id_type 
                    left join unit u on p.id_unit=u.id_unit 
                    LEFT JOIN stok see on p.produkId=see.id_produk 
                    left join ukur r on p.id_ukur=r.id_ukur 
                    left join kategori kat on p.id_kategori=kat.id_kategori
                    WHERE k.id_produk = $_GET[id] GROUP by k.produk_id");
                    while($ku = mysqli_fetch_array($produk)){

                        $setpaket = mysqli_fetch_array(mysqli_query($koneksi,"SELECT *  FROM `setpaket` WHERE `id_paket` = $ku[produkku]  AND `id_produk` = $ku[produkId]"));
                        // echo "SELECT *  FROM `setpaket` WHERE `id_paket` = $ku[produkku]  AND `id_produk` = $ku[produkId]";

                        $gambar = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM gambar_produk where id_produk=$ku[produkId]"));
                        
                        echo "
                        <tr class='clickable' data-href='setpaket-$ku[produkId]-$ku[produkku]-$paket[total].htm'>
                            <td class='text-center'>
                                <img src='assets/images/produk/$gambar[nama_gambar]' style='border-radius: 5px; float:left' width='24px'>
                            </td>
                            <td>$ku[kode_produk]</td>
                            <td>$ku[nama_produk]</td>
                            <td class='abu'>$ku[nama_kategori]</td>
                            <td class='f-hijau'>$ku[total] Buah</td>
                            <td class='abu'>$ku[varian] $ku[nama_ukur]</td>
                            <td class='text-right'>

                                ";
                                if(!empty($setpaket['id_stok'])){
                                    echo "<span class='titikhijau'></span>"; 
                                }
                                // <span class="titikhijau"></span>
                                echo "
                            </td>
                        </tr>";
                    }
                ?>
            </tbody>
        </table>
    </div>
</div>
<?php } ?>