<div class="block counter-block mb-12 col-sm-12 mb-10">
    <div class="bold title mb-20">
        <h4 class="text-capitalize">Stok Masuk</h4>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="judul">
                Stok Masuk
            </div>
        </div>
        <div class="col-md-9 col-sm-6">
            <div class="inp-text abu">
                <input type="text" class="form-control kecil" placeholder="..." name="stok_masuk[]">
                <span class="judul">Buah</span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 mb15"></div>
        <div class="col-md-3 col-sm-6">
            <div class="judul">
                Ukuran
            </div>
        </div>
        <div class="col-md-9 col-sm-6">
            <div class="inp-text abu">
                <input class="form-control f-hitam kecil mr5" type="text" placeholder="..." name="ukuran_baru[]">
                <span class="judul">Kg</span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 mb15"></div>
        <div class="col-md-3 col-sm-6">
            <div class="judul">
                Posisi - Gudang
            </div>
        </div>
        <div class="col-md-9 col-sm-6">
            <div class="inp-text abu">
                <span class="select-yasira kecil">
                    <select class="btn btn-yasir" name="gudang[]">
                        <option>- Pilih -</option>
                        <?php 
                            $gudang = mysqli_query($koneksi,"SELECT * from gudang");
                            while ($u = mysqli_fetch_array($gudang)) {
                                echo "<option value='$u[id_gudang]'>$u[nama_gudang]</option>";
                            }
                        ?>
                    </select>
                </span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 mb15"></div>
        <div class="col-md-3 col-sm-6">
            <div class="judul">
                Posisi - Rak
            </div>
        </div>
        <div class="col-md-9 col-sm-6">
            <div class="inp-text abu">
                <span class="select-yasira kecil">
                    <select class="btn btn-yasir" name="rak[]">
                        <option>- Pilih -</option>
                        <?php 
                            $rak = mysqli_query($koneksi,"SELECT * from rak");
                            while ($u = mysqli_fetch_array($rak)) {
                                echo "<option value='$u[id_rak]'>$u[nama_rak]</option>";
                            }
                        ?>
                    </select>
                </span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 mb15"></div>
        <div class="col-md-3 col-sm-6">
            <div class="judul">
                Posisi - Box
            </div>
        </div>
        <div class="col-md-9 col-sm-6">
            <div class="inp-text abu">
                <span class="select-yasira kecil">
                    <select class="btn btn-yasir" name="box[]">
                        <option>- Pilih -</option>
                        <?php 
                            $box = mysqli_query($koneksi,"SELECT * from box");
                            while ($u = mysqli_fetch_array($box)) {
                                echo "<option value='$u[id_box]'>$u[nama_box]</option>";
                            }
                        ?>
                    </select>
                </span>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 mb15"></div>
        <div class="col-md-3 col-sm-6">
            <div class="judul">
                Posisi - <yasir id="kaleng">Kaleng</yasir>
            </div>
        </div>
        <div class="col-md-9 col-sm-6">
            <div class="inp-text abu">
                <div class="besar pull-left">
                    <span class="select-yasira kecil">
                        <select class="btn btn-yasir" name="kalengplastik[]">
                            <option>- Pilih -</option>
                            <?php 
                                $kalengplastik = mysqli_query($koneksi,"SELECT * from kalengplastik");
                                while ($u = mysqli_fetch_array($kalengplastik)) {
                                    echo "<option value='$u[id_kalengplastik]'>$u[nama_kalengplastik]</option>";
                                }
                            ?>
                        </select>
                    </span>
                    <div class="w75 pull-right">
                        <div class="onoffswitch msk pull-left">
                            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="msk">
                            <label class="onoffswitch-label" for="msk">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>                                         
                        </div>
                        <span class="judul pull-right mt5">Plastik</span>    
                    </div>                                
                </div>
            </div>
            <div class="btn linkiconplus pull-right duplikat"></div>
        </div>
    </div>
</div>

<div style="display: none">
    <div class="block counter-block mb-12 col-sm-12 mb-10" id="asliku">
        <div class="bold title mb-20">
            <h4 class="text-capitalize">Stok Masuk</h4>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="judul">
                    Stok Masuk
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text abu">
                    <input type="text" class="form-control kecil" placeholder="..." name="stok_masuk[]">
                    <span class="judul">Buah</span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 mb15"></div>
            <div class="col-md-3 col-sm-6">
                <div class="judul">
                    Ukuran
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text abu">
                    <input class="form-control f-hitam kecil mr5" type="text" placeholder="..." name="ukuran_baru[]">
                    <span class="judul">Kg</span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 mb15"></div>
            <div class="col-md-3 col-sm-6">
                <div class="judul">
                    Posisi - Gudang
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text abu">
                    <span class="select-yasira kecil">
                        <select class="btn btn-yasir" name="gudang[]">
                            <option>- Pilih -</option>
                            <?php 
                                $gudang = mysqli_query($koneksi,"SELECT * from gudang");
                                while ($u = mysqli_fetch_array($gudang)) {
                                    echo "<option value='$u[id_gudang]'>$u[nama_gudang]</option>";
                                }
                            ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 mb15"></div>
            <div class="col-md-3 col-sm-6">
                <div class="judul">
                    Posisi - Rak
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text abu">
                    <span class="select-yasira kecil">
                        <select class="btn btn-yasir" name="rak[]">
                            <option>- Pilih -</option>
                            <?php 
                                $rak = mysqli_query($koneksi,"SELECT * from rak");
                                while ($u = mysqli_fetch_array($rak)) {
                                    echo "<option value='$u[id_rak]'>$u[nama_rak]</option>";
                                }
                            ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 mb15"></div>
            <div class="col-md-3 col-sm-6">
                <div class="judul">
                    Posisi - Box
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text abu">
                    <span class="select-yasira kecil">
                        <select class="btn btn-yasir" name="box[]">
                            <option>- Pilih -</option>
                            <?php 
                                $box = mysqli_query($koneksi,"SELECT * from box");
                                while ($u = mysqli_fetch_array($box)) {
                                    echo "<option value='$u[id_box]'>$u[nama_box]</option>";
                                }
                            ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 mb15"></div>
            <div class="col-md-3 col-sm-6">
                <div class="judul">
                    Posisi - <yasir id="kaleng">Kaleng</yasir>
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text abu">
                    <div class="besar pull-left">
                        <span class="select-yasira kecil">
                            <select class="btn btn-yasir" name="kalengplastik[]">
                                <option>- Pilih -</option>
                                <?php 
                                    $kalengplastik = mysqli_query($koneksi,"SELECT * from kalengplastik");
                                    while ($u = mysqli_fetch_array($kalengplastik)) {
                                        echo "<option value='$u[id_kalengplastik]'>$u[nama_kalengplastik]</option>";
                                    }
                                ?>
                            </select>
                        </span>
                        <div class="w75 pull-right">
                            <div class="onoffswitch msk pull-left">
                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="msk">
                                <label class="onoffswitch-label" for="msk">
                                    <span class="onoffswitch-inner"></span>
                                    <span class="onoffswitch-switch"></span>
                                </label>                                         
                            </div>
                            <span class="judul pull-right mt5">Plastik</span>    
                        </div>                                
                    </div>
                </div>
                <div class="btn linkicondel pull-right delduplikat"></div>
                <div class="btn linkiconplus pull-right duplikat"></div>
            </div>
        </div>
    </div>
</div>


<div id="tempat"></div>