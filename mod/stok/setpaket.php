<?php 
if(isset($_GET['id'])){
    echo "<form method='post' enctype='multipart/form-data' action='mod/stok/aksi.php?mod=$mod&url=$_GET[id]'>";
    $p     = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * from produk where produkId=$_GET[id]"));
    echo"
        <input type='hidden' name='status' value='setpaket'>
        <input type='hidden' name='produk' value='$p[produkId]'>
        <input type='hidden' name='idpaket' value='$_GET[idpaket]'>";

}
?>
<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <?php
                    $aktif = "produk";
                    include "inc/menu.php";
                ?>
            </div>            
        </div> 
    </div>
</div>


<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">

            
            <div class="">
                <div class="d-none d-lg-block">                    
                    <div class="row">
                        <div class="bold col-4">
                            <h4 class="arialbold mt5">Pilih Lokasi</h4>
                        </div>
                    </div>
                    <hr>
                </div>
                
                <div class="col-12">
                    <div class="row">
                        <div class="table-responsive text-no-wrap mt10">
                            <table class="table" id="dataTable1" data-table="data-table-polos-disfirst">
                                <thead class="text-middle">
                                    <tr>
                                        <th width="60px" class="no-sort"></th>
                                        <th width="10%" class="no-sort">Gudang</th>
                                        <th width="10%" class="no-sort">Rak</th>
                                        <th width="10%" class="no-sort">Box</th>
                                        <th width="10%" class="no-sort">Tin</th>
                                        <th width="10%" class="no-sort">No. Batch</th>
                                        <th width="10%" class="no-sort">Label</th>
                                        <th width="10%" class="no-sort">Exp</th>
                                        <th width="10%" class="no-sort">Unit</th>
                                        <th width="" class="no-sort">Ukuran</th>
                                    </tr>
                                </thead>
                                <tbody class="text-middle text-capitalize">
                                    <?php
                                        $now = time(); 
                                        $qwyku = "SELECT *, s.tgl as tglstok FROM `stok` s 
                                                LEFT JOIN produk p on s.id_produk=p.produkId 
                                                left join unit u   on p.id_unit=u.id_unit 
                                                LEFT join ukur uk  on p.id_ukur=uk.id_ukur
                                                LEFT JOIN gudang g on s.gudang=g.id_gudang
                                                LEFT JOIN rak r on s.rak=r.id_rak
                                                LEFT JOIN box b on s.box=b.id_box
                                                LEFT JOIN kalengplastik kp on s.kalengplastik=kp.id_kalengplastik
                                                WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 ORDER BY `s`.`exp`  ASC";
                                        
                                        $stok = mysqli_query($koneksi, $qwyku);
                                        // echo $qwyku;
                                        // echo "<br><br>";
                                        $total = "";
                                        $totalku = "";

                                        $sku = mysqli_fetch_array(mysqli_query($koneksi, "SELECT *  FROM `setpaket` WHERE `id_paket` = $_GET[idpaket] AND `id_produk` = $p[produkId]"));

        
                                        $ex_cat   = array_unique(explode("+", $sku['id_stok'])) ;
                                       
                                        while ($s = mysqli_fetch_array($stok)) {

                                            $selected = (in_array($s['id_stok'], $ex_cat)) ? ' checked' : '';

                                            $total += $s['stok_masuk'];
                                        
                                                $lamanya = strtotime($s['tglstok']);
                                                $lamadatediff =  ($now - $lamanya);
                                                $lama = round($lamadatediff / (60 * 60 * 24)) + 1;

                                                $expnya = strtotime($s['exp']);
                                                $expdatediff =  ($expnya - $now);
                                                $exp = round($expdatediff / (60 * 60 * 24));

                                                if ($exp < 1 ) {
                                                        $warning = "f-merah";
                                                }
                                                else {
                                                        $warning = "f-hijau";
                                                }
                                                        if(!empty($s['exp'])){
                                                            $tgl = date("d.m.Y", strtotime($s['exp']));
                                                        }
                                                        else{
                                                            $tgl = "";
                                                        }
                                                        $sku = "";
                                                        echo "
                                                            <tr>
                                                                <td width='50px'>
                                                                    <input type='hidden' name='keluar[]' value='$s[stok_masuk]'>
                                                                    <label class='custom-control custom-radio float-right'>
                                                                        <input id='radioStacked3' name='setpaket[]' $selected value='$s[id_stok]' class='custom-control-input' type='checkbox'>
                                                                        <span class='custom-control-indicator'></span>
                                                                        <span class='custom-control-description'></span>
                                                                    </label>
                                                                </td>
                                                                <td>$s[nama_gudang]</td>
                                                                <td>$s[nama_rak]</td>
                                                                <td>$s[nama_box]</td>
                                                                <td>$s[nama_kalengplastik]</td>
                                                                <td>$s[batch]</td>
                                                                <td>$s[label]</td>
                                                                <td>$tgl</td>
                                                                <td>$s[nama_unit]</td>
                                                                <td>$s[stok_masuk]</td>
                                                            </tr>
                                                        ";
                                                }
                                                
                                        
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>
