<?php 

    $produk     = mysqli_query($koneksi, "SELECT * from produk p 
                                              left join kategori k on p.id_kategori=k.id_kategori 
                                              left join type t on p.id_type=t.id_type 
                                              left join unit u on p.id_unit=u.id_unit 
                                              left join ukur r on p.id_ukur=r.id_ukur 
                                              where produkId = $_GET[id]");
    
    $p = mysqli_fetch_array($produk);
   


    if($p['id_type'] == 4){
        $klien = '';
        $produk      = mysqli_query($koneksi, "SELECT *, p.klien as klienku FROM `paket` k 
                    left join produk p on k.produk_id=p.produkId
                    left join type t on p.id_type=t.id_type 
                    left join unit u on p.id_unit=u.id_unit 
                    left join ukur r on p.id_ukur=r.id_ukur 
                    left join kategori kat on p.id_kategori=kat.id_kategori 
                    WHERE k.id_produk = $_GET[id] ");
        while($ku = mysqli_fetch_array($produk)){
            $klien = $ku['klienku'].'+'.$klien;
        }
        
    }
    
   
    
    
?>

<form method="post"action="mod/<?php echo"$folder/aksi.php?mod=$mod&url=$_GET[url]&folder=$_GET[folder]"; ?>">
    <input type="hidden" name="id_produk" value="<?php echo "$p[produkId]";?>">
    <input type="hidden" name="varian" value="<?php echo "$p[varian]";?>">
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 0px ">
            <div class="row">
                <div class="col-lg-12 d-none d-lg-block">

                    <?php
                        $aktif = "produk";
                        include "inc/menu.php";
                    ?>

                </div>            
            </div>
        </div>
    </div>


    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="">
                <div class="d-none d-lg-block">                    
                    <div class="row">
                        <div class="bold col-6">
                            <h4 class="arialbold mt5">Stok <?php if($_GET['mod'] == 'add'){ echo "Masuk"; } else{ echo "Keluar";}?></h4>
                        </div>
                    </div>
                    <hr>
                </div>

                <div class="row mb40 m-10">
                    <?php if(isset($_GET['folderku']) && $_GET['folderku'] == 'paket'){ ?>
                    <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="counter-block pl0 pt0 pb0">
                            <div class="label m0 cir-bold f-hitam">Stok Keluar</div>  
                            <div class="value mb15 f-hitam ukurku"> 0 </div>
                            <div class="f-hitam f14 f-hitam" style="color: #fff">Set</div>        
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3 col-lg-3 pt0">
                        <div class="counter-block pt0 pl0 pb0">
                            <div class="label m0 cir-bold f-hitam">Set Produk</div>  
                            <div class="value mb15 f-hitam">
                                <?php 
                                    $total = "";
                                    $pak = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from paket p 
                                    left join produk pr on p.produk_id=pr.produkId 

                                    left join unit u on pr.id_unit=u.id_unit 
                                    left join type t on pr.id_type=t.id_type 
                                    left join kategori k on pr.id_kategori=k.id_kategori 
                                    LEFT JOIN stok s on pr.produkId=s.id_produk 
                                    where p.id_produk = $_GET[id] GROUP by produkId
                                    ORDER BY `total` ASC limit 1");


                                    $pkiis = mysqli_fetch_array($pak);
                                    
                                    echo $pkiis['total'];
                                ?>
                            </div>
                            <div class="f-hitam f14 f-hitam" style="color: #fff">Produk</div>        
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3 col-lg-3 pt0">
                        <div class="counter-block pt0 pl0 pb0">
                            <div class="label m0 cir-bold f-hitam">Terverifikasi</div>  
                            <div class="value mb15 f-hitam"> 
                                <?php
                                    echo mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM `setpaket` WHERE `id_paket` = $_GET[id] and id_stok<> 0"));

                                ?>
                            </div>
                            <div class="f-hitam f14 f-hitam" style="color: #fff">Produk</div>        
                        </div>
                    </div>
                    <?php } else {?>
                    <div class="col-12 col-sm-6 col-md-3 col-lg-3 pl10 w250">
                        <div class="counter-block pl0 pt0 pb0 pb0">
                            <div class="label m0 f-hitam cir-bold">Total stok unit
                            </div>  
                                <div class="value mb15 f-hitam unitku"> 0 </div>
                                <div class="f-hitam f13 f-hitam" style="color: #fff"><?php echo"$p[nama_unit]"; ?></div>    
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3 col-lg-3 pl10 w250">
                        <div class="counter-block pl0 pt0 pb0 pb0 cir-bold">
                            <div class="label m0 f-hitam">Total stok ukur / bobot</div>  
                            <div class="value mb15 f-hitam ukurku"> <?php echo (isset($p['total_ukuran'])) ? $p['total_ukuran'] : '0' ; ?></div>
                            <div class="f-hitam f13 f-hitam" style="color: #fff"><?php echo"$p[nama_ukur]"; ?></div>
                        </div>
                    </div>
                    <?php } ?>

                </div>
                
                <hr style="width: 100%">

                
                <div class="counter-block mb-12 col-sm-12 mb-20 pt0 pl0">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 w200">
                            <div class="judul cir-book">
                                Klien
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <div class="select-yasir w250">
                                    <select class="btn btn-yasir" name="klien" id="klien" required>
                                        <option value="">Pilih klien</option>
                                        
                                         
                                        <?php
                                            $klien = mysqli_query($koneksi, "SELECT * from klien");
                                            while($k = mysqli_fetch_array($klien)){
                                                echo"<option value='$k[id_klien]'>$k[nama_klien]</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                    <a href="add-klien-3.htm" class="btn btn-primary">Add new</a>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12"></div>
                        <div class="col-md-3 col-sm-6"></div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu" style="padding: 8px;">
                                <div id="alamat"></div>
                                <div id="tlp"></div>
                                <div id="propinsi"></div>
                                <div id="kabupaten"></div>
                                <div id="kecamatan"></div>
                                <div id="kelurahan"></div>
                                <div id="kode_pos"></div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul cir-book">
                                No. Surat Jalan (DN)
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" class="form-control w500" id="nsj" placeholder="..." name="surat_jalan">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul cir-book">
                                No. Referensi
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" id="noref" class="form-control w500" placeholder="..." name="noref">
                            </div>
                        </div>

                        <?php
                        if($p['id_type'] == 4){
                            ?>
                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6">
                            <div class="judul cir-book">
                                Stok Keluar
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                        <div class="inp-text abu">
                            <div class="besar pull-left">
                                <?php
                                $paket = mysqli_fetch_array(mysqli_query($koneksi, "SELECT SUM(s.stok_masuk) as total FROM `paket` p LEFT JOIN produk pr on p.produk_id=pr.produkId LEFT JOIN stok s on pr.produkId=s.id_produk WHERE p.id_produk = $_GET[id] GROUP by pr.produkId
                                ORDER BY `total` ASC LIMIT 1"));
                                echo"    
                                    <input type='number' max='$paket[total]' class='form-control kecil totalpaket' data-varian='1' data-maxkurang='$paket[total]'  data-produk='$_GET[id]'  placeholder='...' name='jumpaket'>
                                ";?>    
                                <span class="judul abu">set</span>
                            </div>
                            
                        </div>
                        <?php
                        } ?>
                        
                    </div>
                    </div>
                </div>
                
                
                


                

                <?php 
                    if ($mod == 'add') {
                        include "add.php";    
                    }
                    else{
                        include "out.php";
                    }                
                ?>

                <div class="col-md-12 col-sm-12 mb-100"></div>
            </div>
        </div>
    </div>
</form>
