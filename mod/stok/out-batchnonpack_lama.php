<div class="block counter-block mb-12 col-sm-12 mb-10">
    <div class="bold title mb-20">
        <h4 class="text-capitalize">Stok Keluar - "Tanpa Kemasan"</h4>
    </div>
    <div class="row">

    <?php
        $ukur = "";
        $qwy = "SELECT *,SUM(stok_masuk) as jumlah, (SUM(stok_masuk)/ukuran) as total, s.tgl as tglstok  FROM `stok` s LEFT JOIN produk p on s.id_produk=p.produkId left join unit u on p.id_unit=u.id_unit LEFT join ukur uk on p.id_ukur=uk.id_ukur WHERE `id_produk` = $_GET[id] and exp >= NOW() - INTERVAL 1 DAY GROUP by ukuran";
        $stok = mysqli_query($koneksi, $qwy);
        while ($s = mysqli_fetch_array($stok)) {
            $ukur .= "<option value='@$s[ukuran]'>@$s[ukuran]</option>";
            
            echo "
                <div class='col-md-3 col-sm-6'>
                    <div class='judul'>
                        Stok Keluar - @$s[ukuran]
                    </div>
                </div>
                <div class='col-md-9 col-sm-6'>
                    <div class='inp-text abu'>
                        <div class='besar pull-left'>
                            <input type='text' class='form-control kecil totalku' data-produk='$s[id_produk]' data-ukuran='$s[ukuran]' data-id='$s[id_stok]' placeholder='...' name=''>
                            <span class='judul'>$s[nama_ukur]</span>
                        </div>
                        <div class='judul'>Ready $s[total] $s[nama_unit], $s[jumlah] $s[nama_ukur]</div>
                    </div>
                </div>
                <div class='col-md-12 col-sm-12 mb15'></div>
            ";    
        }
        
    ?>
        

<!--         <div class="col-md-3 col-sm-6">
            <div class="judul">
                Stok Keluar - @100
            </div>
        </div>
        <div class="col-md-9 col-sm-6">
            <div class="inp-text abu">
                <div class="besar pull-left">
                    <input type="text" class="form-control kecil bor-merah" value="0" name="">
                    <span class="judul">liter</span>
                </div>
                <div class="judul">Ready 10 drum, 500 liter</div>
            </div>
        </div> -->

        <div class="col-md-12 col-sm-12 mb15"></div>
        <div class="col-md-3 col-sm-6">
            <div class="judul">
                Total Stok Keluar
            </div>
        </div>
        <div class="col-md-9 col-sm-6">
            <div class="inp-text abu">
                <div class="besar pull-left">
                    <input type="text" class="form-control kecil bor-non" id="totalakhir">
                    <span class="judul">liter</span>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="block counter-block mb-12 col-sm-12 mb-10">
    <div class="bold title mb-20">
        <h4 class="text-capitalize">
            Posisi stok
            <a href="#" class="btn btn-default pull-right w100">Edit</a>
            <span class="select-yasira pull-right w100 mr10">
                <select class="btn btn-yasir" id="table-filter">
                    <option value="">@ Ukur</option>
                    <?php 
                        echo $ukur; 
                    ?>
                </select>
            </span>
        </h4>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 p0">
            <table class="table tableku" id="dataTable1" data-table="data-table-polos-disfirst">
                <thead class="text-middle">
                    <tr>
                        <th width="20%">Seri</th>
                        <th width="12%" class="no-sort">Out Stok</th>
                        <th width="6%" class="no-sort">Gdg</th>
                        <th width="6%" class="no-sort">Rak</th>
                        <th width="6%" class="no-sort">Box</th>
                        <th width="6%" class="no-sort">Tin</th>
                        <th width="6%" class="no-sort">Lbl</th>
                        <th width="7%" class="no-sort">Unit</th>
                        <th width="7%" class="no-sort" >@ukr <br>Liter</th>
                        <th width="7%">T. Ukr Liter</th>
                        <th width="7%">W. Gdg Hari</th>
                        <th width="10%">Exp</th>
                    </tr>
                </thead>
                <tbody class="text-middle text-capitalize">
                <?php
                    $now = time(); 
                    $qwyku = "SELECT *, s.tgl as tglstok FROM `stok` s 
                              LEFT JOIN produk p on s.id_produk=p.produkId 
                              left join unit u   on p.id_unit=u.id_unit 
                              LEFT join ukur uk  on p.id_ukur=uk.id_ukur
                              LEFT JOIN gudang g on s.gudang=g.id_gudang
                              LEFT JOIN rak r on s.rak=r.id_rak
                              LEFT JOIN box b on s.box=b.id_box
                              LEFT JOIN kalengplastik kp on s.kalengplastik=kp.id_kalengplastik
                               WHERE `id_produk` = $_GET[id] and exp >= NOW() - INTERVAL 1 DAY ORDER BY `s`.`exp`  ASC";
                    $stok = mysqli_query($koneksi, $qwyku);
                    while ($s = mysqli_fetch_array($stok)) {
                        
                        $lamanya = strtotime($s['tglstok']);
                        $lamadatediff =  ($now - $lamanya);
                        $lama = round($lamadatediff / (60 * 60 * 24)) + 1;

                        $expnya = strtotime($s['exp']);
                        $expdatediff =  ($expnya - $now);
                        $exp = round($expdatediff / (60 * 60 * 24));

                        if ($exp < 1 ) {
                            $warning = "f-merah";
                        }
                        else {
                            $warning = "f-hijau";
                        }

                        echo "
                            <tr>
                                <td>".date("Ymd", strtotime($s['tglstok']))."-$s[id_stok]</td>
                                <td class='f-merah'>???</td>
                                <td>$s[nama_gudang]</td>
                                <td>$s[nama_rak]</td>
                                <td>$s[nama_box]</td>
                                <td>$s[nama_kalengplastik]</td>
                                <td>-</td>
                                <td>$s[nama_unit]</td>
                                <td>@$s[ukuran]</td>
                                <td>$s[stok_masuk]</td>
                                <td>".$lama."</td>
                                <td class='$warning'>".date("d.m.Y", strtotime($s['exp']))."</td>
                            </tr>
                        ";
                    }
                ?>
                </tbody>
            </table>
            <div class="mb-10"></div>
            
        </div>                    
    </div>
</div>

<div class="block counter-block mb-12 col-sm-12 mb-10">
    <div class="bold title mb-20">
        <h4 class="text-capitalize">Ringkasan order</h4>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <table class="table tableku">
                <tr>
                    <th width="13%">Transaksi</th>
                    <th width="15%">Produk</th>
                    <th width="15%">No. Surat Jalan</th>
                    <th width="22%">Kepada / Dari</th>
                    <th width="5%">Last</th>
                    <th width="5%">Msk</th>
                    <th width="5%">Klr</th>
                    <th width="5%">Qty</th>
                    <th width="5%">Stn</th>
                    <th width="10%">Date</th>
                </tr>
                <tr>
                    <td class="f-merah">Stok Keluar</td>
                    <td>G9/305IMP-045</td>
                    <td>1763051287</td>
                    <td>Bintang Cakra Kenc...</td>
                    <td>125</td>
                    <td class="f-hijau">0</td>
                    <td class="f-merah">25</td>
                    <td class="f-hijau">100</td>
                    <td>Litter</td>
                    <td class="f-hijau">07.08.2018</td>
                </tr>
            </table>
            <div class="mb-10"></div>
        </div>                    
    </div>
</div>

