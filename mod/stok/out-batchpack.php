<input type="hidden" name="statusstok" value="out">
<div class="block counter-block mb-12 col-sm-12 mb-10">
    <div class="bold title mb-20">
        <h4 class="text-capitalize">Stok Keluar - "Dengan Kemasan"</h4>
    </div>
    <div class="row">
        <?php
            $ukur = "";
            $qwy = "SELECT *,SUM(stok_masuk) as jumlah, (SUM(stok_masuk)) as total, s.tgl as tglstok  FROM `stok` s LEFT JOIN produk p on s.id_produk=p.produkId left join unit u on p.id_unit=u.id_unit LEFT join ukur uk on p.id_ukur=uk.id_ukur WHERE `id_produk` = $_GET[id] and exp >= NOW() - INTERVAL 1 DAY and stok_masuk <> 0 GROUP by ukuran";
            $stok = mysqli_query($koneksi, $qwy);
            while ($s = mysqli_fetch_array($stok)) {
                $unita = $s['nama_unit'];
                $ukur .= "<option value='@$s[ukuran]'>@$s[ukuran]</option>";
                
                echo "
                    <div class='col-md-3 col-sm-6'>
                        <div class='judul'>
                            Stok Keluar - @$s[ukuran]
                        </div>
                    </div>
                    <div class='col-md-9 col-sm-6'>
                        <div class='inp-text abu'>
                            <div class='besar pull-left'>
                                <input type='text' class='form-control kecil totalku' data-produk='$s[id_produk]' data-ukuran='$s[ukuran]' data-id='$s[id_stok]' placeholder='...' name=''>
                                <span class='judul'>$s[nama_unit]</span>
                            </div>
                            <div class='judul'>Ready $s[total] $s[nama_unit]</div>
                        </div>
                    </div>
                    <div class='col-md-12 col-sm-12 mb15'></div>
                ";    
            }
            
        ?>


        <div class="col-md-12 col-sm-12 mb15"></div>
        <div class="col-md-3 col-sm-6">
            <div class="judul">
                Total Stok Keluar
            </div>
        </div>
        <div class="col-md-9 col-sm-6">
            <div class="inp-text abu">
                <div class="besar pull-left">
                    <input type="text" class="form-control kecil bor-non" id="totalakhir">
                    <span class="judul">
                        <?php if(isset($unita)){ echo $unita;}; ?>
                </span>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="block counter-block mb-12 col-sm-12 mb-10">
    <div class="bold title mb-20">
        <h4 class="text-capitalize">
            Posisi stok
            <a href="#" class="btn btn-default pull-right w100">Edit</a>
            
        </h4>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <table class="table tableku">
                <thead>
                    <tr>
                        <th width="20%">Seri</th>
                        <th width="12%">Out Stok</th>
                        <th width="6%">Gdg</th>
                        <th width="6%">Rak</th>
                        <th width="6%">Box</th>
                        <th width="6%">Tin</th>
                        <th width="6%">Lbl</th>
                        <th width="7%">Unit</th>
                        <th width="7%">Ukuran</th>
                        <th width="7%">Total Ukuran</th>
                        <th width="7%">W. Wdg Hari</th>
                        <th width="10%">Exp</th>
                    </tr>
                </thead>
                <tbody class="text-middle text-capitalize" id="datastok">
                    
                </tbody>
                
                
            </table>
            <div class="mb-10"></div>
            
        </div>                    
    </div>
</div>

<div class="block counter-block mb-12 col-sm-12 mb-10">
    <div class="bold title mb-20">
        <h4 class="text-capitalize">Ringkasan order</h4>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <table class="table tableku">
                <tr>
                    <th width="13%">Transaksi</th>
                    <th width="15%">Produk</th>
                    <th width="15%">No. Surat Jalan</th>
                    <th width="22%">Kepada / Dari</th>
                    <th width="5%">Last</th>
                    <th width="5%">Msk</th>
                    <th width="5%">Klr</th>
                    <th width="5%">Qty</th>
                    <th width="5%">Stn</th>
                    <th width="10%">Date</th>
                </tr>
                <tr>
                    <td class="f-merah">Stok Keluar</td>
                    <td>G9/305IMP-045</td>
                    <td>1763051287</td>
                    <td>Bintang Cakra Kenc...</td>
                    <td>125</td>
                    <td class="f-hijau">0</td>
                    <td class="f-merah">25</td>
                    <td class="f-hijau">100</td>
                    <td>Litter</td>
                    <td class="f-hijau">07.08.2018</td>
                </tr>
            </table>
            <div class="mb-10"></div>
        </div>                    
    </div>
</div>

<div class="col-md-12 col-sm-12 mb-100"></div>
