<div class="counter-block mb-12 col-sm-12 mb-20 p0">
    <h4 class="arialbold mt5 mb30 text-capitalize">Lokasi stok masuk</h4>
    <div class="table-responsive text-no-wrap">
        <table class="table">
            <thead class="text-middle">
                <tr>
                    <th width="105px" class="no-sort">Gudang</th>
                    <th width="105px" class="no-sort">Rak</th>
                    <th width="105px" class="no-sort">Box</th>
                    <th width="105px" class="no-sort">Kaleng</th>
                    <th width="170px" class="no-sort">No. Batch</th>
                    <th width="140px" class="no-sort">Label</th>
                    <th width="140px" class="no-sort">Exp</th>
                    <th width="95px" class="no-sort">Unit</th>
                    <th width="84px" class="no-sort">Ukur</th>
                    <th width="150px" class="no-sort">Masuk</th>
                </tr>
            </thead>
            <tbody class="text-middle text-capitalize" id="tempat">
                <?php 
                    $stok = mysqli_query($koneksi, "SELECT * FROM stok s 
                                                    LEFT JOIN gudang g on s.gudang=g.id_gudang
                                                    LEFT JOIN rak r on s.rak=r.id_rak
                                                    LEFT JOIN box b on s.box=b.id_box
                                                    LEFT JOIN kalengplastik k on s.kalengplastik=k.id_kalengplastik
                                                    where id_produk = $_GET[id] and stok_masuk <> 0");
                    while($s=mysqli_fetch_array($stok)){
                        if($p['keluar_produk'] == 'ukur'){
                            $varian = 1;
                            $tot    = $s['ukuran'];
                        }
                        else{
                            $varian = $p['varian'];
                            $tot    = $s['stok_masuk'] * $p['varian'];
                        }
                        echo"
                            <tr>
                                <td>
                                    $s[nama_gudang]
                                    <input type='hidden' value='$s[id_stok]' class='form-control' placeholder='...' name='id_stokku[]'>
                                    <input type='hidden' value='$s[gudang]' class='form-control' placeholder='...' name='gudangku[]'>
                                </td>
                                <td>
                                    $s[nama_rak]
                                    <input type='hidden' value='$s[rak]' class='form-control' placeholder='...' name='rakku[]'>
                                </td>
                                <td>
                                    $s[nama_box]
                                    <input type='hidden' value='$s[box]' class='form-control' placeholder='...' name='boxku[]'>
                                </td>
                                <td>
                                    $s[nama_kalengplastik]
                                    <input type='hidden' value='$s[kalengplastik]' class='form-control' placeholder='...' name='kalengplastikku[]'>
                                </td>
                                <td>
                                    <div class='inp-text abu' style='width:150px'>
                                        <input type='hidden' value='$s[batch]' class='form-control' placeholder='...' name='nobatchku_lama[]'>
                                        <input type='text' style='width:150px' class='form-control' placeholder='$s[batch]' name='nobatchku[]'>
                                    </div>
                                </td>
                                <td>
                                    <div class='inp-text abu' style='width:120px'>
                                        <input type='hidden' value='$s[label]' class='form-control' placeholder='...' name='labelku_lama[]'>
                                        <input type='text' style='width:120px' class='form-control' placeholder='$s[label]' name='labelku[]'>
                                    </div>                                                
                                </td>
                                <td>
                                    <div class='inp-text abu tglicon' style='width:120px'>
                                        <input type='hidden' value='$s[exp]' class='form-control' placeholder='$s[exp]' name='expku_lama[]'>
                                        <input class='single-date-picker  form-control sedang' style='width:120px' data-color='primary' type='text' value='' placeholder='$s[exp]' name='expku[]'>
                                    </div>                                                
                                </td>
                                <td>$s[stok_masuk]</td>
                                <td>$tot</td>
                                <td>
                                    <div class='inp-text abu' style='width:100px'>
                                        <input type='hidden' value='$s[exp]' class='form-control' placeholder='...' name='masuk_lama[]'>
                                        <input type='text' style='width:80px' class='form-control masukku' placeholder='...' name='masukku[]'  data-varian='$p[varian]'>
                                    </div>                                                
                                </td>
                            </tr>
                        ";
                    }
                ?>
                <tr id="asliku">
                    <td>
                        <span class="select-yasira kecil" style='width:80px'>
                            <select class="btn btn-yasir" name="gudang[]">
                                <option value=''>—</option>
                                <?php 
                                    $gudang = mysqli_query($koneksi,"SELECT * from gudang");
                                    while ($u = mysqli_fetch_array($gudang)) {
                                        echo "<option value='$u[id_gudang]'>$u[nama_gudang]</option>";
                                    }
                                ?>
                            </select>
                        </span>
                    </td>
                    <td>
                        <span class="select-yasira kecil" style='width:80px'>
                            <select class="btn btn-yasir" name="rak[]">
                                <option value=''>—</option>
                                <?php 
                                    $rak = mysqli_query($koneksi,"SELECT * from rak");
                                    while ($u = mysqli_fetch_array($rak)) {
                                        echo "<option value='$u[id_rak]'>$u[nama_rak]</option>";
                                    }
                                ?>
                            </select>
                        </span>
                    </td>
                    <td>
                        <span class="select-yasira kecil" style='width:80px'>
                            <select class="btn btn-yasir" name="box[]">
                                <option value=''>—</option>
                                <?php 
                                    $box = mysqli_query($koneksi,"SELECT * from box");
                                    while ($u = mysqli_fetch_array($box)) {
                                        echo "<option value='$u[id_box]'>$u[nama_box]</option>";
                                    }
                                ?>
                            </select>
                        </span>
                    </td>
                    <td>
                        <span class="select-yasira kecil" style='width:80px'>
                            <select class="btn btn-yasir" name="kalengplastik[]">
                                <option value=''>—  </option>
                                <?php 
                                    $kalengplastik = mysqli_query($koneksi,"SELECT * from kalengplastik");
                                    while ($u = mysqli_fetch_array($kalengplastik)) {
                                        echo "<option value='$u[id_kalengplastik]'>$u[nama_kalengplastik]</option>";
                                    }
                                ?>
                            </select>
                        </span>
                    </td>
                    <td>
                        <div class="inp-text abu" style='width:150px'>
                            <input type="text" style='width:150px' class="form-control" placeholder="..." name="nobatch[]">
                        </div>
                    </td>
                    <td>
                        <div class="inp-text abu" style='width:120px'>
                            <input type="text" style='width:120px' class="form-control" placeholder="..." name="label[]">
                        </div>
                    </td>
                    <td>
                        <div class="inp-text abu tglicon" style='width:120px'>
                            <input class="single-date-picker  form-control sedang" style='width:120px' data-color="primary" type="text" placeholder="—" name="exp[]">
                        </div>
                    </td>
                    <td></td>
                    <td></td>
                    <td>
                        <div class="inp-text abu" style='width:125px'>
                            <input type="text" style='width:80px' class="form-control masukku" data-varian='<?=$p['varian'];?>' data-keluar='<?=$p['keluar_produk'];?>' placeholder="—" name="masuk[]">
                            <div class="hapus_varian btn btn-default pull-right close" style="float: right; display: none"></div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="btn btn-primary mt10 duplikat w100">Add</div>   
</div>