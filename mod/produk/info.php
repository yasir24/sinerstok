<div class="counter-block mb-12 col-sm-12 mb-20 pl0 pt0">
    <div class="">      
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="judul cir-book">
                    Gambar <br>
                    <!-- <spon class="f-abu f11">Rekomendasi 2-4 Gambar produk.</spon> -->
                </div>
            </div>

            <div class="col-md-9 col-sm-6">
                <div class="inp-image abu">
                    <div class="menu_gambar">
                        <?php
                            $jum = mysqli_num_rows($gambar); 
                            while ($g = mysqli_fetch_array($gambar)) { ?>
                            <div class="" style="display: inline-block;margin-right: 0;">
                                <div class="hapusku">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <label>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-preview upload" data-trigger="fileinput">
                                                    <img src="<?php echo $url."/assets/images/produk/".$g['nama_gambar']; ?>">
                                                </div>
                                                <input type="file" name="nama_gambar[]" class="imgInp" class="hidden" style="display: none">
                                                <input type="hidden" name="id_gambar[]" class="hidden" style="display: none" value="<?php echo $g['id_gambar']; ?>">
                                            </div>
                                        </label>
                                    </div>
                                    <a class="hapusku_gambar" onclick="return confirm('apakah anda yakin akan menghapus data ini ?')" href="<?php echo "mod/produk/aksi.php?gambar=$g[nama_gambar]&pro=$_GET[id]&status=hapus_gambar&id=$g[id_gambar]";?>"><i class="btn btn-default close mt15"></i></a>
                                </div>
                            </div>
                        <?php  }  ?>
                    </div>
                    <!-- <div class="tambah_gambar mb15 btn btn-success">Add More Photo</div> -->
                </div>
                <?php
                    $as = 5 - $jum;
                    for ($i=0; $i < $as; $i++) { 
                        echo "
                        <div class='hapusku'>
                            <div class='fileinput fileinput-new' data-provides='fileinput'>
                                <label>
                                    <div class='fileinput fileinput-new' data-provides='fileinput'>
                                        <div class='fileinput-preview upload' data-trigger='fileinput'></div>
                                        <input type='file' name='nama_gambar[]' class='imgInp' class='hidden' style='display: none'>
                                    </div>
                                </label>
                            </div>
                        </div>";
                    }
                ?>
            </div>

            <div class="col-md-3 mb20 col-sm-6">
                <div class="judul cir-book">
                    Nama barang
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text abu">
                    <input type="text" class="form-control w500" value='<?php echo $p['nama_produk']; ?>' name="nama_produk">
                </div>
            </div>

            <div class="col-md-3 mb20 col-sm-6">
                <div class="judul cir-book">
                    Kode
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text ">
                    <input type="text" class="form-control w500" value='<?php echo $p['kode_produk']; ?>' name="kode_produk">
                </div>
            </div>

            <?php 
                if ($p['id_type']  == 1) {
            ?>
            <div class="col-md-3 col-sm-6 mb20">
                <div class="judul cir-book mb-10">
                    Varian
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text" style="vertical-align: middle;display: table-cell;">
                    <?php 
                        $fa=mysqli_query($koneksi, "select * from ukur where id_ukur='$p[id_ukur]'");
                        $f=mysqli_fetch_array($fa);                                
                        echo $p['varian'].' '.ucfirst($f['nama_ukur']);
                    ?>
                </div>
            </div>
            <?php } ?>
            

            <div class="col-md-3 mb20 col-sm-6">
                <div class="judul cir-book">
                Katagori barang
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text">
                    <div class="select-yasir w250">
                        <select class="btn btn-yasir" name='id_kategori'>
                            <option>- Pilih Kategori -</option>
                            <?php
                                $sel = "";
                                while ($k = mysqli_fetch_array($kategori)) {
                                    if ($k['id_kategori'] == $p['id_kategori']) {
                                        echo "<option value='$k[id_kategori]' selected>$k[nama_kategori]</option>";
                                    }
                                    else{
                                        echo "<option value='$k[id_kategori]'>$k[nama_kategori]</option>";
                                    }

                                }
                            ?>
                        </select> 
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb20 col-sm-6">
                <div class="judul cir-book">
                    Deskripsi barang
                </div>
            </div>
            <div class="col-md-9 col-sm-6">
                <div class="inp-text abu w500">
                    <textarea name="deskripsi" class="w500" placeholder="Tulis deskripsi untuk keterangan produk ..."><?php echo "$p[deskripsi]"; ?></textarea>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="counter-block mb-12 col-sm-12 pl0 pt0">
    <div class="bold title mb-20">
        <h4>Mesin </h4>
    </div>
    <div class="row">
        <div class="col-md-3 mb20 col-sm-6">
            <div class="judul cir-book">
                Mesin
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="">
                <?php
                    
                    if ($p['id_type'] == '4') {
                        $mesinku = "";
                        $mpaket = mysqli_query($koneksi, "SELECT *,pr.mesin as mesin FROM paket p left join produk pr on p.produk_id=pr.produkId WHERE p.id_produk = $_GET[id]");
                        // echo "SELECT * FROM paket p left join produk pr on p.id_produk=pr.produkId WHERE p.id_produk = $_GET[id]";
                        while($mp = mysqli_fetch_array($mpaket)){
                            $mesinku = $mesinku.'+'.$mp['mesin'];
                        }

                        $result     = array_unique(explode("+", $mesinku));  
                        foreach($result as $newvalue){
                            if (!empty($newvalue)) {
                                $query  = "SELECT * from mesin where id_mesin='$newvalue' order by nama_mesin asc"; 
                                $result = mysqli_query($koneksi, $query);
                                $row    = mysqli_fetch_array($result);
                                echo"
                                    <div class='iconxku mr10 text-left btn btn-default mb10' style='border: 1px solid #ccc'>
                                        <input type='hidden' value='$row[id_mesin]' name='mesin[]'>    
                                        $row[nama_mesin]
                                        <span class='iconx'></span>
                                    </div>
                                ";
                            }
                        }
                    }
                    else{
                        if (!empty($p['mesin'])) {
                            $result     = explode("+", $p['mesin']);   
                            foreach($result as $newvalue){
                                $query  = "SELECT * from mesin where id_mesin='$newvalue' order by nama_mesin asc"; 
                                $result = mysqli_query($koneksi, $query);
                                $row    = mysqli_fetch_array($result);
                                echo"
                                    <div class='iconxku mr10 text-left btn btn-default mb10' style='border: 1px solid #ccc'>
                                        <input type='hidden' value='$row[id_mesin]' name='mesin[]'>    
                                        $row[nama_mesin]
                                        <span class='iconx'></span>
                                    </div>
                                ";
                            }
                        }
                        echo "<a href='mesin-$p[produkId].htm' class='btn btn-primary mb10 f14 w100' style='border: 1px solid #ccc;'>Add</a>";

                    }
                ?>
            </div>
        </div>
    </div>
</div>




<?php
                    
    if($p['id_type'] == '4'){
?>

<?php
    } else {
?>

<!--<div class="block counter-block mb-12 col-sm-12 mb20">-->
<!--    <div class="bold title mb-20">-->
<!--        <h4>klien-->
            
<!--        </h4>-->
<!--    </div>-->
<!--    <div class="row">-->
<!--        <div class="col-md-12 col-sm-12">-->
<!--            <div class="">-->

                    <!--// $result     = explode("+", $p['klien']);   -->
                    <!--// foreach($result as $newvalue){-->
                    <!--//     $query  = "SELECT * from klien where id_klien='$newvalue' order by nama_klien asc"; -->
                    <!--//     $result = mysqli_query($koneksi, $query);-->
                    <!--//     $row    = mysqli_fetch_array($result);-->
                    <!--//     echo"-->
                    <!--//         <div class='iconxku mr10 text-left btn btn-default mb10' style='border: 1px solid #ccc'>-->
                    <!--//             <input type='hidden' value='$row[id_klien]' name='klien[]'>    -->
                    <!--//             $row[nama_klien]-->
                    <!--//             <span class='iconx'></span>-->
                    <!--//         </div>-->
                    <!--//     ";-->
                    <!--// }-->
<!--                ?>-->
<!--                <a href="klien-<?php echo"$p[produkId]"; ?>.htm" class="btn btn-default mb10 f14" style="border: 1px solid #ccc;">Tambah klien</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->


<div class="counter-block mb-12 col-sm-12 mb20 pl0 pt0">
    <div class="bold title mb-20">
        <h4>Keterangan stok</h4>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="judul cir-book">
                Satuan Unit
            </div>
        </div>
        <div class="col-md-8 col-sm-6">
            <div class="inp-text abu">
                <div class="select-yasir w250">
                    <select class="btn btn-yasir" id="sat_unit" name="id_unit">
                        <option>- Pilih Satuan Unit -</option>
                        <?php
                            while ($k = mysqli_fetch_array($unit)) {
                                if ($k['id_unit'] == $p['id_unit']) {
                                    echo "<option value='$k[id_unit]' selected>$k[nama_unit]</option>";
                                }
                                else{
                                    echo "<option value='$k[id_unit]'>$k[nama_unit]</option>";
                                }                                        
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 mb15"></div>
        <div class="col-md-3 col-sm-6">
            <div class="judul cir-book">
                Satuan Ukur
            </div>
        </div>
        <div class="col-md-8 col-sm-6">
            <div class="inp-text abu">
                <div class="select-yasir w250">
                    <select class="btn btn-yasir"id="sat_ukur" name="id_ukur">
                        <option>- Pilih Ukur Produk -</option>
                        <?php
                            while ($k = mysqli_fetch_array($ukur)) {
                                if ($k['id_ukur'] == $p['id_ukur']) {
                                    echo "<option value='$k[id_ukur]' selected>$k[nama_ukur]</option>";
                                }
                                else{
                                    echo "<option value='$k[id_ukur]'>$k[nama_ukur]</option>";
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 mb15"></div>
        <div class="col-md-3 col-sm-6">
            <div class="judul cir-book">
                Satuan stok keluar
            </div>
        </div>
        <div class="col-md-8 col-sm-6">
            <div class="inp-text abu">
                <div class="select-yasir w250">
                    <select class="btn btn-yasir" id="sat_keluar" name="keluar_produk">
                        <option>- Pilih Satuan keluar -</option>
                        <option value="unit" <?php echo ($p['keluar_produk']=='unit') ? 'selected' : '' ;?>>Unit</option>
                        <option value="ukur" <?php echo ($p['keluar_produk']=='ukur') ? 'selected' : '' ;?>>Ukur</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 mb15"></div>
        <div class="col-md-3 col-sm-6">
            <div class="judul cir-book">
                Peringatan stok minimum
            </div>
        </div>
        <div class="col-md-8 col-sm-6">
            <div class="inp-text abu" style="width: 190px">
                <input type="text" class="form-control" width="90px" placeholder="..." name="peringatan_produk" style="width: 90px" value='<?php echo $p['peringatan_produk']; ?>'>
                <span class="judul cir-book satuan m0"> 
                    <?php
                        if($p['keluar_produk'] == 'ukur'){
                            $fa=mysqli_query($koneksi, "select * from ukur where id_ukur='$p[id_ukur]'");
                            $f=mysqli_fetch_array($fa);
                        
                            echo ucfirst($f['nama_ukur']);
                        }
                        else{
                            $fa=mysqli_query($koneksi, "select * from unit where id_unit='$p[id_unit]'");
                            $f=mysqli_fetch_array($fa);
                        
                            echo ucfirst($f['nama_unit']);
                        }
                    ?>
                </span>
            </div>
        </div>
    </div>
</div>
<div class="bold title mb-20">
    <div class="bold title mb-20">
        <h4>Varian Ukur / Bobot</h4>
    </div>
    <!-- <h4 style="font-family: cir-book;margin-bottom: 5px;margin-top: 30px;"></h4> -->
</div>

<div class="table-responsive text-no-wrap">                        
    <div class="">
        <table class="dataTable no-footer" style="width: 100%">
            <?php 
                $var = mysqli_query($koneksi, "SELECT * FROM produk where hub='$p[hub]'");
                while($v=mysqli_fetch_array($var)){
                    echo"
                        <tr>
                            <td class='h50 pl0'  style='background: none;padding: 0 !important;'>
                                <div class='inp-text abu pull-left col-sm-12 pl0'>
                                    <input type='text' class='form-control' disabled width='90px' value='$v[varian]' name='varian[]' style='width: 90px'>
                                    <span class='judul cir-book satuanukurku f-hitam'>$p[nama_ukur]</span>
                                </div>
                            </td>
                        </tr>
                    ";
                }
            ?>
            <tbody class="menu_varian">
            </tbody>
        </table>
    </div>
</div>
<button type="button" class="tambah_varian btn btn-primary w100">+ VARIAN</button>
<div class="mb40"></div>
<?php } ?>