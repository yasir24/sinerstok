<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="bold col-4">
                            <div class="pull-left mr20">
                                <a href="#" class="menukekiri pull-left"></a>
                                <a href="#" class="menukekanan pull-left"></a>
                            </div>
                            <h4 class="arialbold mt5">Produk baru - paket</h4>
                        </div>
                        <form class="col-8 text-right">
                            <a href="#" class="btn btn-default simpan mr10"></a>
                            <a href="#" class="btn btn-default close"></a>
                        </form>
                    </div>
                </ol>
            </div>
            <div class="col-12 col-md-12 d-lg-none">
                <div class="row">
                    <form class="col-12 text-right">
                        <div class="cari">
                            <i class="fa fa-search"></i>
                            <input type="text" name="cari" placeholder="Search ...">
                        </div>
                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="">
            <div class="block counter-block mb-12 col-sm-12 mb20">
                <div class="bold title mb-20">
                    <h4 class="text-capitalize">Informasi Produk</h4>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="judul abu">
                            Gambar Produk <br>
                            <spon class="f-abu f11">Rekomendasi 2-4 Gambar produk.</spon>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-6">
                        <div class="inp-image abu">
                            <img src="img/icon/imgku.png" width="60px">
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 mb15"></div>
                    <div class="col-md-3 col-sm-6">
                        <div class="judul abu">
                            Nama barang
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-6">
                        <div class="inp-text abu">
                            <input type="text" class="form-control bor-merah" width="80%" value="Filtering mat 250 x 470mm Technotrans FK-C ..." name="">
                            <span class="judul f-merah">
                                Nama barang sudah digunakan
                            </span>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 mb15"></div>
                    <div class="col-md-3 col-sm-6">
                        <div class="judul abu">
                            Kode barang
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-6">
                        <div class="inp-text abu">
                            <input type="text" class="form-control bor-hijau" width="80%" value="G9/305IMP-045" name="">
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 mb15"></div>
                    <div class="col-md-3 col-sm-6">
                        <div class="judul abu">
                            Kategori
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-6">
                        <div class="inp-text abu">
                            <span class="select-yasira">
                                <select class="btn btn-yasir">
                                    <option>Pilih Kategori</option>
                                    <option>Sparepart</option>
                                    <option>Coating</option>
                                    <option>Lem</option>
                                    <option>Consumable</option>
                                </select>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 mb15"></div>
                    <div class="col-md-3 col-sm-6">
                        <div class="judul abu">
                            Type Produk
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-6">
                        <div class="inp-text abu">
                            <span class="select-yasira">
                                <select class="btn btn-yasir">
                                    <option>Jenis Produk</option>
                                </select>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 mb15"></div>
                    <div class="col-md-3 col-sm-6">
                            <div class="judul abu">
                                Deskripsi
                            </div>
                    </div>
                    <div class="col-md-9 col-sm-6">
                        <div class="inp-text abu">
                            <textarea placeholder="Tulis deskripsi untuk keterangan produk ..."></textarea><br>
                            <p class="f11 f-abu">Max. 320 karakter</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-12 col-sm-12 mb-100"></div>
        </div>
    </div>
</div>
