<?php
    date_default_timezone_set('Asia/Jakarta');
    $pro = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total, SUM(s.ukuran) as total_ukuran  from produk p
                                                  left join kategori k on p.id_kategori=k.id_kategori
                                                  left join type t on p.id_type=t.id_type
                                                  left join unit u on p.id_unit=u.id_unit
                                                  left join ukur r on p.id_ukur=r.id_ukur
                                                  LEFT JOIN stok s on p.produkId=s.id_produk
                                                  where produkId = '$_GET[id]'
                                                  GROUP by produkId
                                                  order by produkId desc");
                                                  

    
    $p               = mysqli_fetch_array($pro);
    
    // echo $p['nama_produk'];

    $ukuranku        = mysqli_num_rows(mysqli_query($koneksi, "SELECT ukuran,tgl FROM `stok` WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 GROUP by ukuran"));
    $lama            = mysqli_fetch_array(mysqli_query($koneksi, "SELECT tgl FROM `stok` WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 order by tgl asc"));

    $now             = time();
    $lamanya         = strtotime($lama['tgl']);
    $lamadatediff    =  ($now - $lamanya);
    $lamaku          = round($lamadatediff / (60 * 60 * 24)) - 1;

    $ot              = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] ORDER BY `tgl` desc limit 1"));
    $lamanya_ot      = strtotime($ot['tgl']);
    $lamadatediff_ot =  ($now - $lamanya_ot);
    $lamaku_ot       = round($lamadatediff_ot / (60 * 60 * 24)) -1;

    $ohi             = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] and tgl = CURDATE()"));
    $obi             = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] and MONTH(tgl) = MONTH(CURRENT_DATE())"));
    $oyi             = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] and YEAR(tgl) = YEAR(CURRENT_DATE())"));
    $ratas           = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] and YEAR(tgl) = YEAR(CURRENT_DATE())")) / 12;

    $gambar = mysqli_query($koneksi, "SELECT * FROM gambar_produk where id_produk=$p[produkId]");
    $rata            = round($ratas, 1);

?>
<form method="post" enctype="multipart/form-data" action="mod/<?php echo"$folder/aksi.php?mod=$mod&url=$_GET[id]&hub=$p[hub]&folder=$_GET[folder]"; ?>">

    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 0px ">
            <div class="row">
                <div class="col-lg-12 d-none d-lg-block">

                    <?php
                        $aktif = "produk";
                        include "inc/menu.php";
                    ?>

                </div>            
            </div>
        </div>
    </div>
<input type="hidden" name="status" value="update">
<input type="hidden" name="hub" value="<?=$p['hub'];?>">
<input type="hidden" name="id_type" value="<?=$p['id_type'];?>">
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="d-none d-lg-block">
            <div class="row">
                <div class="bold col-4">
                    <h4 class="arialbold mt5 text-capitalize"><?=$_GET['page']; ?> Produk</h4>
                </div>
                <div class="col-8 text-right">
               
                        <a href="detail-produk-<?=$_GET['id'];?>-2.htm" class="btn <?php echo ($_GET['page'] == 'info') ? 'btn-abu' : 'btn-default';?>  w100 mr10 ">Info</a>
                        <?php 
                            if($p['id_type']<>'4'){
                        ?>
                        <a href="posisi-produk-<?=$_GET['id'];?>-2.htm" class="btn <?php echo ($_GET['page'] == 'posisi') ? 'btn-abu' : 'btn-default';?> w100 mr10">Posisi</a>
                        <?php } 
                            if($p['id_type']=='4'){
                        ?>
                        <a href="set-produk-<?=$_GET['id'];?>-2.htm" class="btn <?php echo ($_GET['page'] == 'set') ? 'btn-abu' : 'btn-default';?> w100 mr10">Set</a>
                        <?php  } ?>
                        <a href="order-produk-<?=$_GET['id'];?>-2.htm"  class="btn <?php echo ($_GET['page'] == 'order') ? 'btn-abu' : 'btn-default';?> w100 mr10">Riwayat</a>
                        <!-- <a href="#" class="btn btn-default w70 mr10">User</a> -->
                   
                    <?php
                        if($p['id_type'] == 1){
                            echo "<a href='add-stok-$p[sort_nama]-$p[produkId]-2.htm' class='btn btn-default mr10 masuk mr5'></a>";
                        }
                        echo "<a href='out-stok-$p[sort_nama]-$p[produkId]-2.htm' class='btn btn-default mr10 text-lowercase keluar'></a>";
                        echo "<a href='opname-$p[sort_nama]-$p[produkId]-2.htm' class='btn iconchange pull-right'></a>";
                        // echo "<a href='update-produk-$p[produkId]-2.htm' class='btn btn-default update mr10 pull-right'></a>";
                    ?>
                </div>
            </div>
            <hr>
        </div>
        <?php 
            if ($_GET['page'] !== 'set') { 
        ?>
        <div class="row mb40 m-10">
            <div class="col-12 col-sm-6 col-md-3 col-lg-3 pl10 w250">
                <div class="counter-block pl0 pt0 pb0">
                    <div class="label m0 f-hitam cir-bold">Total stok unit
                    </div>  
                    <?php
                        if($p['id_type'] == '4'){
                    ?>
                        <div class="value mb15 f-hitam">
                            <?php 
                            
                                $totala = "0";
                                $pak = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from paket p 
                                left join produk pr on p.produk_id=pr.produkId 
                                left join unit u on pr.id_unit=u.id_unit 
                                left join ukur uk on pr.id_ukur=uk.id_ukur 
                                left join type t on pr.id_type=t.id_type 
                                left join kategori k on pr.id_kategori=k.id_kategori 
                                LEFT JOIN stok s on pr.produkId=s.id_produk 
                                where p.id_produk = $_GET[id] GROUP by produkId
                                ORDER BY `total` ASC limit 1");

                                $pk = mysqli_fetch_array($pak);  
                                
                                if (!empty($pk['total'])) {
                                   echo $pk['total'];
                                   $total_min_paket = $pk['total'];
                                   $total_min_varian = $pk['varian']*$pk['total']; 
                                   $nama_min_ukur   = $pk['nama_ukur'];
                                }  
                                else{
                                    echo "0";
                                    $total_min_varian = "0"; 
                                }

                                
                            ?>
                        </div>
                        <div class="f-hitam f14 f-hitam" style="color: #fff">Paket</div>    
                    <?php } else{ ?>
                        <div class="value mb15 f-hitam"> <?php echo (isset($p['total'])) ? $p['total'] : '0' ; ?></div>
                        <div class="f-hitam f14 f-hitam" style="color: #fff"><?php echo"$p[nama_unit]"; ?></div>    
                    <?php
                        }
                    ?>
                        
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 col-lg-3 pl10 w250">
                <div class="counter-block pl0 pt0 pb0">
                    <div class="label m0 f-hitam cir-bold">Total stok ukur / bobot
                    </div>  
                    <?php
                        if($p['id_type'] == '4'){
                            $varku = "";
                            $pak = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from paket p 
                            left join produk pr on p.produk_id=pr.produkId 
                            left join ukur uk on pr.id_ukur=uk.id_ukur 
                            left join unit u on pr.id_unit=u.id_unit 
                            left join type t on pr.id_type=t.id_type 
                            left join kategori k on pr.id_kategori=k.id_kategori 
                            LEFT JOIN stok s on pr.produkId=s.id_produk 
                            where p.id_produk = $_GET[id] GROUP by produkId");


                            while($pu = mysqli_fetch_array($pak)){
                                $varku = $varku + $pu['varian'];
                            }
                            

                    ?>
                        <div class="value mb15 f-hitam">
                            <?php 
                                echo $total_min_paket*$varku;
                            ?>
                        </div>
                        <div class="f-hitam f14 f-hitam" style="color: #fff"><?php echo (isset($nama_min_ukur)) ? $nama_min_ukur : ' ' ; ?></div>
                    <?php }
                    
                    else { ?>
                        <div class="value mb15 f-hitam"> <?php echo (isset($p['total_ukuran'])) ? $p['total_ukuran'] : '0' ; ?></div>
                        <div class="f-hitam f14 f-hitam" style="color: #fff"><?php echo"$p[nama_ukur]"; ?></div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <hr style="width: 100%">
        <?php } ?>

        <?php 
            include $_GET['page'].".php";
        ?>

    </div>
</div>
</form>

    <script type="text/javascript" src="assets/js/jquery-1.10.2.min.js"></script>                   
    <script>
        $(document).ready(function(){
            sessionStorage.clear();
        });
    </script>