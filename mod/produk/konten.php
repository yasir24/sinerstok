<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <?php
                    $aktif = "produk";
                    include "inc/menu.php";
                ?>
            </div>            
        </div>
    </div>
</div>

<!-- <div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="bold col-4">
                            <div class="pull-left mr20">
                                <a href="#" class="menukekiri pull-left"></a>
                                <a href="#" class="menukekanan pull-left"></a>
                            </div>
                            <h4 class="arialbold mt5">Produk</h4>
                        </div>
                        <form class="col-8 text-right">
                            <span class="select-yasir">
                                <select class="btn btn-yasir"  id="table-filter">
                                    <option value="">- Kategori -</option>
                                    <option>sparepart</option>
                                    <option>coating</option>
                                    <option>lem</option>
                                    <option>consumable</option>
                                </select>
                            </span>

                            <form class="col-6 text-right">
                                <div class="cari">
                                    <i class="iconcari"></i>
                                    <input type="text" id="input-filter" name="cari" placeholder="Search ...">
                                </div>
                                <a href="add-produk-2.htm" class="ml10 btn iconplus pull-right"></a>
                                <a href="#" class="ml10 btn icondownload pull-right"></a>
                                <a href="#" class="ml10 btn iconwarning pull-right"></a>
                            </form>

                        </form>
                    </div>
                </ol>
            </div>
            <div class="col-12 col-md-12 d-lg-none">
                <div class="row">
                    <form class="col-12 text-right">
                        <div class="cari">
                            <i class="fa fa-search"></i>
                            <input type="text" name="cari" placeholder="Search ...">
                        </div>
                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="row">
            <div class="col-12 text-left">
                <span class="select-yasir">
                    <select class="btn btn-yasir table-filter">
                        <option value="">All Product</option>
                        <option>sparepart</option>
                        <option>coating</option>
                        <option>lem</option>
                        <option>consumable</option>
                    </select>
                </span>       
                <span class="mt5 abu" style="margin-top: 3px;position: absolute;margin-left: 10px;">
                    <span class="cir-bold f14 abu">
                        <?php
                            echo $num = mysqli_num_rows($produk);
                        ?>
                    </span> Produk ditemukan 
                </span>      

                <a href="add-produk-2.htm" class="ml10 btn btn-primary w100 pull-right">Add</a>
                <a href="#" class="ml10 btn btn-dark pull-right w100">.Pdf</a>
                <span class="select-yasir pull-right">
                    <select class="btn btn-yasir"  id="urutkan" style="margin-top:0;margin-left: 10px; margin-right: 0">
                        <option value="0">- Urutkan -</option>
                        <option value="1">Kode</option>
                        <option value="2">Nama</option>
                        <option value="3">Kategory</option>
                        <option value="4">QTY</option>
                    </select>
                </span>
                <div class="cari pull-right ml10">
                    <i class="iconcari"></i>
                    <input type="text" id="input-filter" name="cari" placeholder="Kata kunci ...">
                </div>
                
                <hr>
            </div>
            
            <div class="col-12">
                <div class="row">
                    <div class="container-fluid p0">
                    <div class="table-responsive text-no-wrap">
                        <table class="table" id="dataTable1" data-table="data-table-polos-disfirst">
                            <thead class="text-middle">
                                <tr>
                                    <th width="7%" class="no-sort ysku"></th>
                                    <th width="17%" class="no-sort">Kode</th>
                                    <th width="30%" class="no-sort">Nama</th>
                                    <th width="10%" class="no-sort">Kategori</th>
                                    <th width="5%" class="no-sort">Qty</th>
                                    <th width="" class="no-sort">Varian @</th>
                                    <th width="" class="no-sort"></th>
                                </tr>
                            </thead>
                            <tbody class="text-middle text-capitalize">
                                <?php
                                    while ($p = mysqli_fetch_array($produk)) {
                                        $gambar = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM gambar_produk where id_produk=$p[produkId]"));
                                        echo "<tr class='clickable' data-href='detail-produk-$p[produkId]-2.htm'>
                                                    <td class='text-left'>
                                                    <img src='assets/images/produk/$gambar[nama_gambar]' style='border-radius: 2px' width='24px'>
                                                    </td>
                                                    <td>$p[kode_produk]</td>
                                                    <td>$p[nama_produk]</td>
                                                    <td>$p[nama_kategori]</td>
                                                    <td>";
                                                        if($p['id_type'] == '4'){
                                                            $paket = mysqli_fetch_array(mysqli_query($koneksi, "SELECT SUM(s.stok_masuk) as total FROM `paket` p LEFT JOIN produk pr on p.produk_id=pr.produkId LEFT JOIN stok s on pr.produkId=s.id_produk WHERE p.id_produk = $p[produkId] GROUP by pr.produkId
                                                            ORDER BY `total` ASC LIMIT 1"));
                                                            echo $paket['total'];
                                                        }
                                                        else{
                                                            if($p['keluar_produk'] == 'unit'){
                                                                echo"$p[total] $p[nama_unit]";                                                                
                                                            }
                                                            else{
                                                                echo $p['total']*$p['varian'].' '.$p['nama_ukur'];
                                                            }
                                                        }
                                                    echo"
                                                    </td>
                                                    <td>$p[varian] $p[nama_ukur]</td>
                                                    <td class='text-right'>";
                                                        if($p['id_type'] == '4'){
                                                            echo "
                                                                <a href='#' class='btn btn-default text-lowercase paket'></a>
                                                            ";
                                                        }
                                                        else{
                                                            if ($p['total'] <= $p['minim_produk']) {
                                                                echo "<a href='#' class='btn warning mr5'></a>";
                                                            }
                                                        }
                                                        if ($p['id_type'] <> 4) {
                                                            echo "
                                                            <a href='add-stok-$p[sort_nama]-$p[produkId]-2.htm' class='btn btn-default text-lowercase masuk'></a>
                                                                
                                                            ";
                                                        }
                                                        else {
                                                            echo "<a href='out-stok-$p[sort_nama]-$p[produkId]-2.htm' class='btn btn-default keluar'></a>
";
                                                        }
                                                        if (!empty($p['total'])) {
                                                            echo"
                                                            <a href='out-stok-$p[sort_nama]-$p[produkId]-2.htm' class='btn btn-default keluar'></a>
                                                            ";
                                                        }
                                                        else{
                                                            echo "
                                                            ";
                                                        }
                                                        echo "
                                                    </td>
                                                </tr>
                                        ";
                                    }
                                ?>

                                <!-- <tr>
                                    <td class="text-center">
                                        <img src="img/produk/sinerstock-img-01-produk-sample.jpg" style="border-radius: 5px" width="30px">
                                    </td>
                                    <td>G9/305IMP-045</td>
                                    <td>Terragloss UV Gloss Varnish</td>
                                    <td class="abu">Sparepart</td>
                                    <td class="f-hijau">65</td>
                                    <td class="abu">Buah</td>
                                    <td class="text-right">
                                        <a href="#" class="btn warning mr5"></a>
                                        <a href="#" class="btn btn-default masuk mr5"></a>
                                        <a href="#" class="btn btn-default keluar"></a>
                                    </td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
