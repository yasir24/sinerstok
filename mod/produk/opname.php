<?php
    function tulis_cekboxa($field,$koneksi,$judul) {
        $query ="select * from mesin order by nama_mesin";
        $r = mysqli_query($koneksi,$query);
        $_arrNilai = explode('+', $field);
        $str = '';
        $no=1;
        while ($w = mysqli_fetch_array($r)) {
            $_ck = (array_search($w[0], $_arrNilai) === false)? '' : 'checked';
            $str .= "
            <tr>
                <td class='abu'>$w[1]</td>
                <td>
                    <div class='checkbox'>
                        <label class='custom-control custom-checkbox' style='width: 100%'>
                            <input type='checkbox' name='mesin[]' id='checkItem' value='$w[0]' $_ck class='custom-control-input'>
                            <span class='custom-control-indicator'></span>
                        </label>
                    </div>
                </td>
            </tr> 
            ";
            $no++;
        }
        return $str;
    }

    $p     = mysqli_fetch_array(mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total, SUM(s.ukuran) as total_ukuran from produk p 
                                                  left join kategori k on p.id_kategori=k.id_kategori 
                                                  left join type t on p.id_type=t.id_type 
                                                  left join unit u on p.id_unit=u.id_unit 
                                                  left join ukur r on p.id_ukur=r.id_ukur 
                                                  LEFT JOIN stok s on p.produkId=id_produk
                                                  where produkId = $_GET[id]
                                                  GROUP by produkId 
                                                  order by produkId desc"));
?>

<form method="post" enctype="multipart/form-data" action="mod/produk/<?php echo"aksiopname.php?mod=$mod&url=$_GET[url]&folder=$_GET[folder]"; ?>">
<input type="hidden" name="status" value="setmesin">
<input type="hidden" name="id_produk" value="<?=$_GET['id'];?>">
<input type="hidden" name="keluar_produk" value="<?=$p['keluar_produk'];?>">
<input type="hidden" name="varian" value="<?=$p['varian'];?>">
<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">

                <?php
                    $aktif = "produk";
                    include "inc/menu.php";
                ?>

            </div>            
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="d-none d-lg-block">
            <div class="row">
                <div class="bold col-4">
                    <h4 class="arialbold mt5">Stok Opname</h4>
                </div>
            </div>
            <hr>
        </div>
        <div class="row mb40 m-10">
            <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10 p0 w250">
                <div class="counter-block p0">
                    <div class="label f14 m0 f-hitam cir-bold">Stok unit program</div>  
                    <div class="value mb15 f-hitam"> <?php echo (isset($p['total'])) ? $p['total'] : '0' ; ?></div>
                    <div class="f-hitam f13 f-hitam" style="color: #fff"><?php echo"$p[nama_unit]"; ?></div>        
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10 p0 w250">
                <div class="counter-block p0">
                    <div class="label f14 m0 f-hitam cir-bold">Stok ukur program</div>  
                    <div class="value mb15 f-hitam"> <?php echo (isset($p['total_ukuran'])) ? $p['total_ukuran'] : '0' ; ?></div>
                    <div class="f-hitam f13 f-hitam" style="color: #fff"><?php echo"$p[nama_ukur]"; ?></div>        
                </div>
            </div>
        </div>
        <hr>
        <div class="">
        <div class=" counter-block mb-12 p0 col-sm-12 mb-20">
            <div class="table-responsive text-no-wrap">
                <table class="table">
                    <thead class="text-middle">
                        <tr>
                            <th width="105px" class="no-sort">Gudang</th>
                            <th width="105px" class="no-sort">Rak</th>
                            <th width="105px" class="no-sort">Box</th>
                            <th width="105px" class="no-sort">Kaleng</th>
                            <th width="170px" class="no-sort">No. Batch</th>
                            <th width="140px" class="no-sort">Label</th>
                            <th width="140px" class="no-sort">Exp</th>
                            <?php
                                if ($p['keluar_produk'] == 'unit') {
                                    echo "
                                        <th width='95px' class='no-sort'>Unit</th>
                                    ";
                                }
                                else{
                                    echo "
                                        <th width='95px' class='no-sort'>Unit</th>

                                        <th width='84px' class='no-sort'>Ukur</th>
                                    ";
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody class="text-middle text-capitalize" id="tempat">
                        <?php 
                            $stok = mysqli_query($koneksi, "SELECT * FROM stok where id_produk = $_GET[id]");
                            while($s=mysqli_fetch_array($stok)){
                                if($p['keluar_produk'] == 'ukur'){
                                    $varian = 1;
                                    $tot    = $s['ukuran'];
                                    $masuk  = $s['ukuran'];
                                }
                                else{
                                    $varian = $p['varian'];
                                    $tot    = $s['stok_masuk'] * $p['varian'];
                                    $masuk  = $s['stok_masuk'];
                                }
                                echo"
                                    <tr>
                                        <td>
                                            <input type='hidden' value='$s[id_stok]' name='id_stokku[]'>

                                            <span class='select-yasira kecil' style='width:80px'>
                                                <select class='btn btn-yasir' name='gudangku[]'>
                                                    <option value=''>- Pilih -</option>";
                                                        $gudang = mysqli_query($koneksi,'SELECT * from gudang');
                                                        while ($u = mysqli_fetch_array($gudang)) {
                                                            if($u['id_gudang'] == $s['gudang']){
                                                                echo "<option value='$u[id_gudang]' selected>$u[nama_gudang]</option>";
                                                            }
                                                            else{
                                                                echo "<option value='$u[id_gudang]'>$u[nama_gudang]</option>";
                                                            }                                                            
                                                        }
                                                   echo "
                                                </select>
                                            </span>
                                        </td>
                                        <td>
                                            <span class='select-yasira kecil' style='width:80px'>
                                                <select class='btn btn-yasir' name='rakku[]'>
                                                    <option value=''>- Pilih -</option>";
                                                        $rak = mysqli_query($koneksi,'SELECT * from rak');
                                                        while ($u = mysqli_fetch_array($rak)) {
                                                            if($u['id_rak'] == $s['rak']){
                                                                echo "<option value='$u[id_rak]' selected>$u[nama_rak]</option>";
                                                            }
                                                            else{
                                                                echo "<option value='$u[id_rak]'>$u[nama_rak]</option>";
                                                            }
                                                        }
                                                    echo"
                                                </select>
                                            </span>
                                        </td>
                                        <td>
                                            <span class='select-yasira kecil' style='width:80px'>
                                                <select class='btn btn-yasir' name='boxku[]'>
                                                    <option value=''>- Pilih -</option>";
                                                    $box = mysqli_query($koneksi,'SELECT * from box');
                                                    while ($u = mysqli_fetch_array($box)) {
                                                        if($u['id_box'] == $s['box']){
                                                            echo "<option value='$u[id_box]' selected>$u[nama_box]</option>";
                                                        }
                                                        else{
                                                            echo "<option value='$u[id_box]'>$u[nama_box]</option>";
                                                        }
                                                    }
                                                    echo"
                                                </select>
                                            </span>
                                        </td>
                                        <td>
                                            <span class='select-yasira kecil' style='width:80px'>
                                                <select class='btn btn-yasir' name='kalengplastikku[]'>
                                                    <option value=''>- Pilih -</option>";
                                                    $kalengplastik = mysqli_query($koneksi,'SELECT * from kalengplastik');
                                                    while ($u = mysqli_fetch_array($kalengplastik)) {
                                                        if($u['id_kalengplastik'] == $s['kalengplastik']){
                                                            echo "<option value='$u[id_kalengplastik]' selected>$u[nama_kalengplastik]</option>";
                                                        }
                                                        else{
                                                            echo "<option value='$u[id_kalengplastik]'>$u[nama_kalengplastik]</option>";
                                                        }
                                                    }
                                                    echo"
                                                </select>
                                            </span>
                                        </td>
                                        <td>
                                            <input type='text' class='form-control' style='width:150px' value='$s[batch]' placeholder='..' name='nobatchku[]'>
                                        </td>
                                        <td>
                                            <input type='text' class='form-control' style='width:120px' value='$s[label]' placeholder='..' name='labelku[]'>
                                        </td>
                                        <td>
                                            <input class='single-date-picker  form-control sedang' value='$s[exp]' style='width:120px' data-color='primary' type='text' placeholder='—' name='expku[]'>
                                        </td>";
                                       if ($p['keluar_produk'] == 'unit') {
                                            echo "
                                                <td>
                                                    <input type='text' class='form-control' value='$s[stok_masuk]' placeholder='..' name='stok_masukku[]'>
                                                </td>
                                            ";
                                        }
                                        else{
                                            echo "
                                                <td>
                                                    <input type='text' class='form-control' value='$s[stok_masuk]' placeholder='..' name='stok_masukku[]'>
                                                </td>
                                                <td>
                                                    <input type='text' class='form-control' value='$s[ukuran]' placeholder='..' data-awal='$tot' name='ukuranku[]'>
                                                </td>
                                            ";
                                        }
                                        echo "
                                        <td>
                                            <a href='mod/produk/aksiopname.php?idstok=$s[id_stok]&idpro=$s[id_produk]' class='btn btn-default pull-right close'></a>
                                        </td>
                                    </tr>
                                ";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            
        </div>
        </div>
    </div>
</div>
</form>