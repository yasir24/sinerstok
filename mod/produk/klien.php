<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <?php
                    $aktif = "produk";
                    include "inc/menu.php";
                ?>
            </div>            
        </div>
    </div>
</div>

<!-- <div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="bold col-4">
                            <div class="pull-left mr20">
                                <a href="#" class="menukekiri pull-left"></a>
                                <a href="#" class="menukekanan pull-left"></a>
                            </div>
                            <h4 class="arialbold mt5">Produk</h4>
                        </div>
                        <form class="col-8 text-right">
                            <span class="select-yasir">
                                <select class="btn btn-yasir"  id="table-filter">
                                    <option value="">- Kategori -</option>
                                    <option>sparepart</option>
                                    <option>coating</option>
                                    <option>lem</option>
                                    <option>consumable</option>
                                </select>
                            </span>

                            <form class="col-6 text-right">
                                <div class="cari">
                                    <i class="iconcari"></i>
                                    <input type="text" id="input-filter" name="cari" placeholder="Search ...">
                                </div>
                                <a href="add-produk-2.htm" class="ml10 btn iconplus pull-right"></a>
                                <a href="#" class="ml10 btn icondownload pull-right"></a>
                                <a href="#" class="ml10 btn iconwarning pull-right"></a>
                            </form>

                        </form>
                    </div>
                </ol>
            </div>
            <div class="col-12 col-md-12 d-lg-none">
                <div class="row">
                    <form class="col-12 text-right">
                        <div class="cari">
                            <i class="fa fa-search"></i>
                            <input type="text" name="cari" placeholder="Search ...">
                        </div>
                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <?php 
            if(isset($_GET['id'])){
                echo "<form method='post' enctype='multipart/form-data' action='mod/produk/aksi.php?mod=$mod&url=$_GET[id]'>";
                $p     = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * from produk where produkId=$_GET[id]"));
                echo"
                    <input type='hidden' name='status' value='setklien'>
                    <input type='hidden' name='produk' value='$p[produkId]'>";

            }
            else{
                echo"<form action='add-produk-2.htm' method='post'>";
            }
        ?>
            <div class="">
                <div class="d-none d-lg-block">                    
                    <div class="row">
                        <div class="bold col-4">
                            <h4 class="arialbold mt5">Tambah klien</h4>
                        </div>
                        <div class="col-8 text-right">
                            <a href="add-produk-2.htm" class='btn w100 btn-default mr10 f-hijau'>BATAL</a>
                            <input type="submit" name="save" value="UPDATE" class="w100 btn btn-primary mr10">
                        </div>
                    </div>
                    <hr>
                </div>
                
                <div class="col-12">
                    <div class="row">
                        <div class="table-responsive text-no-wrap">
                            <table class="table">
                                <thead class="text-middle">
                                    <tr>
                                        <th width="50px"></th>
                                        <th>Nama klien</th>
                                    </tr>
                                </thead>
                                <tbody class="text-middle">
                                    <?php 
                                        $sku = "";
                                        $ex_cat = explode('+', $p['klien']);
                                        if(isset($_SESSION['klien'])){
                                            $ex_cat   = explode("+", $_SESSION['klien']);
                                        }
                                        while($s=mysqli_fetch_array($klien)){
                                            $selected = (in_array($s['id_klien'], $ex_cat)) ? ' checked' : '';
                                            echo"
                                                <tr>
                                                    <td width='50px'>
                                                        <label class='custom-control custom-radio'>
                                                            <input id='radioStacked3' $selected name='klien[]' value='$s[id_klien]' class='custom-control-input' type='checkbox'>
                                                            <span class='custom-control-indicator'></span>
                                                            <span class='custom-control-description'></span>
                                                        </label>
                                                    </td>
                                                    <td>$s[nama_klien]</td>
                                                </tr>
                                            ";
                                            
                                        }   
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="mb40"></div>
                </div>
            </div>
        </form>
    </div>
</div>
