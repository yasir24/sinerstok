<form method="post" enctype="multipart/form-data" action="mod/<?php echo"$folder/aksi.php?mod=$mod&url=$_GET[url]&folder=$_GET[folder]"; ?>">
<input type="hidden" name="status" value="set">
<input type="hidden" name="produk" value="<?=$_GET['id'];?>">

<div class="header fixed-header">
        <div class="container-fluid" style="padding: 0px ">
            <div class="row">
                <div class="col-lg-12 d-none d-lg-block">

                    <?php
                        $aktif = "produk";
                        include "inc/menu.php";
                    ?>

                </div>            
            </div>
        </div>
    </div>

<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10 mb-20">
    <div class="d-none d-lg-block">                    
            <div class="row">
                <div class="bold col-4">
                    <h4 class="arialbold mt5">Tambah produk Set</h4>
                </div>
                <div class="bold col-8">
                    <div class="cari pull-right">
                        <i class="iconcari"></i>
                        <input type="text" id="input-filter" name="cari" placeholder="Kata kunci ...">
                    </div>   
                    <span class="select-yasir pull-right">
                        <select class="btn btn-yasir table-filter">
                            <option value="">All Kategori</option>
                            <option value="Sparepart">Sparepart</option>
                            <option value="Coating">Coating</option>
                            <option value="Lem">Lem</option>
                            <option value="Consumable">Consumable</option>
                        </select>
                    </span>                 
                </div>
            </div>
            <hr>
        </div>
        <div class="">
            
            <div class="counter-block mb-12 col-sm-12 mb20 pt0">
                <div class="row mt20">
                    <div class="table-responsive text-no-wrap">
                        <table class="table" data-table="data-table-polos-disfirst">
                            <thead class="text-middle">
                                <tr>
                                    <th width="60px" class="no-sort">
                                        <div class='checkbox'>
                                            <!-- <label class='custom-control custom-checkbox' style='width: 100%'>
                                                <input type='checkbox' id="checkAll" class='custom-control-input'>
                                                <span class='custom-control-indicator'></span>
                                            </label> -->
                                            <label class='custom-control custom-radio'>
                                                <input   id="checkAll" name='idpro[]' class='custom-control-input' type='checkbox'>

                                                <span class='custom-control-indicator'></span>
                                                <span class='custom-control-description'></span>
                                            </label>     
                                        </div>
                                    </th>
                                    <th width="60px" class="no-sort ysku"></th>
                                    <th width="160px" class="no-sort">Kode</th>
                                    <th width="340px" class="no-sort">Nama</th>
                                    <th width="120px" class="no-sort">Kategori</th>
                                    <th width="120px" class="no-sort">Qty</th>
                                    <th width="" class="no-sort">Varian @</th>
                                    
                                </tr>
                            </thead>
                            <tbody class="text-middle">
                            <?php
                                $produkku   = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from produk p 
                                                left join kategori k on p.id_kategori=k.id_kategori 
                                                left join type t on p.id_type=t.id_type 
                                                left join unit u on p.id_unit=u.id_unit 
                                                left join ukur r on p.id_ukur=r.id_ukur 
                                                LEFT JOIN stok s on p.produkId=id_produk
                                                where p.id_type <> 4
                                                GROUP by produkId
                                                order by produkId desc");
                                while($p = mysqli_fetch_array($produkku)){
                                    $gambar = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM gambar_produk where id_produk=$p[produkId]"));
                                    echo"
                                        <tr>
                                            <td>
                                                <div class='checkbox'>
                                                    <label class='custom-control custom-radio'>
                                                        <input  id='checkItem' name='idpro[]' value='$p[produkId]' class='custom-control-input' type='checkbox' ";
                                                        $u = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from paket where id_produk = $_GET[id] and produk_id = $p[produkId]"));
                                                        if($u == 1){
                                                            echo "checked ";
                                                        }  
                                                        echo">
                                                        <span class='custom-control-indicator'></span>
                                                        <span class='custom-control-description'></span>
                                                    </label>                                                    
                                                </div>
                                            </td>
                                            <td class='text-center'>
                                                <img src='assets\images\produk/$gambar[nama_gambar]' style='border-radius: 5px' width='30px'>
                                            </td>
                                            <td>$p[kode_produk]</td>
                                            <td>$p[nama_produk]</td>
                                            <td class='abu'>$p[nama_kategori]</td>
                                            <td class='f-hijau'>$p[total] $p[nama_unit]</td>
                                            <td class='abu'>$p[varian] $p[nama_ukur]</td>
                                            
                                            
                                        </tr>
                                    ";
                                }
                            ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
                            </form>


                            
                            <!-- <label class='custom-control custom-checkbox' style='width: 100%'>
                                                        <input type='checkbox' name='idpro[]' id='checkItem' value='$p[produkId]'"; 
                                                            $u = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from paket where id_produk = $_GET[id] and produk_id = $p[produkId]"));
                                                            if($u == 1){
                                                                echo "checked ";
                                                            }                                                        
                                                        echo" class='custom-control-input'>
                                                        <span class='custom-control-indicator'></span>
                                                    </label> -->