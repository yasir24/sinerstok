<div class="mb-12 mb20">
    <div class="">
        <div class="table-responsive text-no-wrap p0 mt10">
            <table class="table " id="dataTable1" data-table="data-table-polos-disfirst">
                <thead class="text-middle">
                    <tr>
                        <th width="24px" class="no-sort ysku"></th>
                        <th width="160px" class="no-sort">Kode</th>
                        <th width="340px" class="no-sort">Nama</th>
                        <th width="120px" class="no-sort">Kategori</th>
                        <th width="120px" class="no-sort">Qty</th>
                        <th width="300px" class="no-sort">Varian @</th>
                        <th class="no-sort"></th>
                    </tr>
                </thead>
                <tbody class="text-middle">
                    <?php 
                        $pak = mysqli_query($koneksi, "SELECT *, SUM(s.stok_masuk) as total from paket p 
                                                        left join produk pr on p.produk_id=pr.produkId 
                                                        left join ukur uk on pr.id_ukur=uk.id_ukur 
                                                        left join unit u on pr.id_unit=u.id_unit 
                                                        left join type t on pr.id_type=t.id_type 
                                                        left join kategori k on pr.id_kategori=k.id_kategori 
                                                        LEFT JOIN stok s on pr.produkId=s.id_produk 
                                                        where p.id_produk = $_GET[id] GROUP by produkId");
                            

                        while($p = mysqli_fetch_array($pak)){
                            
                            $gambar = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM gambar_produk where id_produk=$p[produkId]"));
                            echo"
                                <tr class='clickable' data-href='detail-produk-$p[produkId]-2.htm'>
                                    <td class='text-center'>
                                        <img src='assets/images/produk/$gambar[nama_gambar]' style='border-radius: 5px; float:left' width='24px'>
                                    </td>
                                    <td>$p[kode_produk]</td>
                                    <td>$p[nama_produk]</td>
                                    <td class='abu'>$p[nama_kategori]</td>
                                    <td class='f-hijau'>$p[total] Buah</td>
                                    <td class='abu'>$p[varian] $p[nama_ukur]</td>
                                    <td class='text-right'>
                                        <a onClick=\"return confirm('apakah anda yakin akan menghapus data ini ?')\" href='mod/produk/aksi.php?mod=produk&status=hapus_setproduk&id=$p[id_paket]&paket=$_GET[id]' class='btn btn-default pull-right close'></a>
                                    </td>
                                </tr>
                            ";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <a href="updateset-produk-<?=$_GET['id'];?>-2.htm" class="btn btn-primary w100 pull-left">Add Set</a>
</div>
        