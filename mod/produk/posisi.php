
<div class="row mb40">
    
        <div class="table-responsive text-no-wrap mt10">
            <table class="table" id="dataTable1" data-table="data-table-polos-disfirst">
                <thead class="text-middle">
                    <tr>
                        <th width="6%" class="no-sort">Gudang</th>
                        <th width="6%" class="no-sort">Rak</th>
                        <th width="6%" class="no-sort">Box</th>
                        <th width="6%" class="no-sort">Tin</th>
                        <th width="6%" class="no-sort">No. Batch</th>
                        <th width="6%" class="no-sort">Label</th>
                        <th width="7%" class="no-sort">Exp</th>
                        <th width="7%" class="no-sort">Unit</th>
                        <th width="7%" class="no-sort">Ukuran</th>
                    </tr>
                </thead>
                <tbody class="text-middle text-capitalize">
                    <?php
                        $now = time(); 
                        $qwyku = "SELECT *, s.tgl as tglstok FROM stok s 
                                                    LEFT JOIN gudang g on s.gudang=g.id_gudang
                                                    LEFT JOIN rak r on s.rak=r.id_rak
                                                    LEFT JOIN box b on s.box=b.id_box
                                                    LEFT JOIN kalengplastik k on s.kalengplastik=k.id_kalengplastik
                                                    where id_produk = $_GET[id] and stok_masuk <> 0";

                        $stok = mysqli_query($koneksi, $qwyku);
                        // echo $qwyku;
                        // echo "<br><br>";
                        $total = "";
                        $totalku = "";


                        while ($s = mysqli_fetch_array($stok)) {

                            
                            $total += $s['stok_masuk'];

                            // echo "total : ".$total;
                            // echo "<br>";
                            // echo "totalku :".$totalku;
                            // echo "<br>";

                        
                                $lamanya = strtotime($s['tglstok']);
                                $lamadatediff =  ($now - $lamanya);
                                $lama = round($lamadatediff / (60 * 60 * 24)) + 1;

                                $expnya = strtotime($s['exp']);
                                $expdatediff =  ($expnya - $now);
                                $exp = round($expdatediff / (60 * 60 * 24));

                                if ($exp < 1 ) {
                                        $warning = "f-merah";
                                }
                                else {
                                        $warning = "f-hijau";
                                }
                                if($p['keluar_produk'] == 'ukur'){
                                    $varian = 1;
                                    $tot    = $s['ukuran'];
                                }
                                else{
                                    $varian = $p['varian'];
                                    $tot    = $s['stok_masuk'] * $p['varian'];
                                }
                                        $tgl = date("d.m.Y", strtotime($s['exp']));
                                        if (empty($s['exp'])) {
                                            $tgl = "";
                                        }
                                        echo "
                                            <tr>
                                                <td>$s[nama_gudang]</td>
                                                <td>$s[nama_rak]</td>
                                                <td>$s[nama_box]</td>
                                                <td>$s[nama_kalengplastik]</td>
                                                <td>$s[batch]</td>
                                                <td>$s[label]</td>
                                                <td>$tgl</td>
                                                <td>$s[stok_masuk]</td>
                                                <td>$tot</td>
                                            </tr>
                                        ";
                                }
                                
                        
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>