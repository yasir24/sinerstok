<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">

                <?php
                    $aktif = "produk";
                    include "inc/menu.php";
                ?>

            </div>            
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-sm-12 table-responsive text-no-wrap">
                        <table class="table">
                            <thead class="text-middle">
                                <tr>
                                    <th>Data Gudang ku</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody class="text-middle">
                                                                
                                <tr>
                                    <td>Gudang</td>
                                    <td>
                                        <a href="modgudang-gudang-5.htm" class="btn btn-default pull-right iconchange"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Rak</td>
                                    <td>
                                        <a href="modgudang-rak-5.htm" class="btn btn-default pull-right iconchange"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Box</td>
                                    <td>
                                        <a href="modgudang-box-5.htm" class="btn btn-default pull-right iconchange"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kaleng / Plastik</td>
                                    <td>
                                        <a href="modgudang-kalengplastik-5.htm" class="btn btn-default pull-right iconchange"></a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
