<?php 
    if ($_GET['mod'] == 'add') {
        $kat = 1;
    }
    else{
        $kat = 2;
        $de = mysqli_query($koneksi, "SELECT * from mesin where id_mesin = $_GET[id]");
        $d  = mysqli_fetch_array($de);
    }
 ?>
<form method="post" action="mod/<?php echo"$folder/aksi.php?mod=$mod&kat=$kat&url=$_GET[url]&folder=$_GET[folder]"; ?>">
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 0px ">
            <div class="row">
                <div class="col-lg-12 d-none d-lg-block">
                    <ol class="breadcrumb">
                        <div class="row">
                            <div class="bold col-4">
                                <div class="pull-left mr20">
                                    <a href="#" class="menukekiri pull-left"></a>
                                    <a href="#" class="menukekanan pull-left"></a>
                                </div>
                                <h4 class="arialbold mt5">Master - mesin</h4>
                            </div>
                            <div class="col-8 text-right">                    
                                <div class="text-right">
                                    <input type="submit" name="save" value="" class="btn btn-default pull-right simpan mr10">
                                </div>
                            </div>
                        </div>
                    </ol>
                </div>
                <div class="col-12 col-md-12 d-lg-none">
                    <div class="row">
                        <div class="cari">
                            <i class="fa fa-search"></i>
                            <input type="text" name="cari" placeholder="Search ...">
                        </div>
                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="">
                <div class="block counter-block mb-12 col-sm-12 mb-20">
                    <div class="bold title mb-20">
                        <h4 class="text-capitalize">Tambah mesin</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-3 col-sm-6 mb15">
                            <div class="judul">
                                Nama mesin
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <?php 
                                    if ($_GET['mod'] == 'add') {
                                ?>                                    
                                    <input type="text" class="form-control" width="80%" placeholder="..." name="mesin[]">
                                <?php } else { ?>
                                    <input type="hidden" name="id" value="<?php echo $d['id_mesin']; ?>">
                                    <input type="text" class="form-control" width="80%" placeholder="..." value="<?php echo $d['nama_mesin']; ?>" name="mesin">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="menu_wrap col-sm-12"></div>
                        <?php 
                            if ($_GET['mod'] == 'add') {
                        ?>
                        <div class="col-sm-12 mt20">
                            <button type="button" class="tambah_menu iconplus btn btn-default"></button>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
