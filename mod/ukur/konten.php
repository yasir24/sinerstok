<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px 25px">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">
                <ol class="breadcrumb">
                    <div class="row">
                        <div class="bold col-4">
                            <div class="pull-left mr20">
                                <a href="#" class="menukekiri pull-left"></a>
                                <a href="#" class="menukekanan pull-left"></a>
                            </div>
                            <h4 class="arialbold mt5">Master - ukur</h4>
                        </div>
                        <div class="col-8 text-right">                    
                            <div class="text-right">
                                <a href="add-ukur-3.htm" class="ml10 btn iconplus pull-right"></a>
                            </div>
                        </div>
                    </div>
                </ol>
            </div>
            <div class="col-12 col-md-12 d-lg-none">
                <div class="row">
                    <form class="col-12 text-right">
                        <div class="cari">
                            <i class="fa fa-search"></i>
                            <input type="text" name="cari" placeholder="Search ...">
                        </div>

                        <a id="toggle-navigation" href="javascript:void(0);" class="icon-btn mr-3"><i class="fa fa-bars"></i></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-sm-12 table-responsive text-no-wrap">
                        <table class="table">
                            <thead class="text-middle">
                                <tr>
                                    <th>Suplier</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody class="text-middle">
                                <?php
                                    $se = mysqli_query($koneksi, "SELECT * from ukur order by nama_ukur asc");
                                    while ($s = mysqli_fetch_array($se)) { 
                                ?>                                
                                <tr>
                                    <td><?php echo $s['nama_ukur']; ?></td>
                                    <td>
                                        <a href="<?php echo"mod/$folder/aksi.php?mod=$mod&id=$s[id_ukur]&kat=3&url=$_GET[url]&folder=$_GET[folder]"; ?>" class="btn btn-default pull-right close"></a>
                                        <a href="<?php echo"edit-$folder-$s[id_ukur]-$_GET[url].htm"; ?>" class="btn btn-default pull-right iconchange mr10"></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
