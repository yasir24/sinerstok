<?php 
     $sparepart       = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(id_kategori) as jumlah, id_kategori FROM produk WHERE id_kategori = 1  GROUP by id_kategori"));
     $coating         = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(id_kategori) as jumlah, id_kategori FROM produk WHERE id_kategori = 2  GROUP by id_kategori"));
     $lem             = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(id_kategori) as jumlah, id_kategori FROM produk WHERE id_kategori = 3  GROUP by id_kategori"));
     $consumable      = mysqli_fetch_array(mysqli_query($koneksi, "SELECT COUNT(id_kategori) as jumlah, id_kategori FROM produk WHERE id_kategori = 4  GROUP by id_kategori"));
    
    //  $lama            = mysqli_fetch_array(mysqli_query($koneksi, "SELECT tgl FROM `stok` WHERE `id_produk` = $_GET[id] and stok_masuk <> 0 order by tgl asc"));
 
    //  $now             = time();
    //  $lamanya         = strtotime($lama['tgl']);
    //  $lamadatediff    =  ($now - $lamanya);
    //  $lamaku          = round($lamadatediff / (60 * 60 * 24)) - 1;
 
    //  $ot              = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * from feed WHERE id_produk = $_GET[id] ORDER BY `tgl` desc limit 1"));
    //  $lamanya_ot      = strtotime($ot['tgl']);
    //  $lamadatediff_ot =  ($now - $lamanya_ot);
    //  $lamaku_ot       = round($lamadatediff_ot / (60 * 60 * 24)) -1;
 
     $ohi                = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE tgl = CURDATE()"));
     $obi             = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE MONTH(tgl) = MONTH(CURRENT_DATE())"));
     $oyi             = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE YEAR(tgl) = YEAR(CURRENT_DATE())"));
     $ratas           = mysqli_num_rows(mysqli_query($koneksi, "SELECT * from feed WHERE YEAR(tgl) = YEAR(CURRENT_DATE())")) / 12;
 
     $rata            = round($ratas, 1);
?>
<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px "> 
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">

                <?php
                    $aktif = "dashboard";
                    include "inc/menu.php";
                ?>

            </div>            
        </div>
    </div>
</div>

<div class="content sm-gutter">
    <div class="container-fluid sm-padding-10">
        <div class="row m-10">
            
            <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10 mb20">
                <div class="block counter-block biru">
                    <div class="row m-20">
                        <div class="col-sm-12 text-left p20">
                            <div class="valueicon">
                                <img width="30px" src="<?php echo "$url/img/garnis/sn0001-icon-sparepart.svg";?>" width="30px">
                            </div>
                            <p class="label f14">Sparepart</p>
                            <div class="value">
                                <?php 
                                    if (empty($sparepart['jumlah'])) {
                                        echo "0";
                                    }
                                    else{
                                        echo $sparepart['jumlah'];
                                    }
                                ?>
                            </div>
                            <div class='satuan'>Produk</div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10 mb20">
                <div class="block counter-block hijau">
                    <div class="row">
                        <div class="col-sm-12 text-left p20">
                            <div class="valueicon">
                                <img width="30px" src="<?php echo "$url/img/garnis/sn0002-icon-glue.svg";?>">
                            </div>
                            <p class="label f14">Coating</p>
                            <div class="value">
                                <?php 
                                    if (empty($coating['jumlah'])) {
                                        echo "0";
                                    }
                                    else{
                                        echo $coating['jumlah'];
                                    }
                                ?>
                            </div>
                            <div class='satuan'>Produk</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10 mb20">
                <div class="block counter-block merah">
                    <div class="row">
                        <div class="col-sm-12 text-left p20">
                            <div class="valueicon">
                                <img width="30px" src="<?php echo "$url/img/garnis/sn0002-icon-glue.svg";?>">
                            </div>
                            <p class="label f14">Lem</p>
                            <div class="value">
                                <?php 
                                    if (empty($lem['jumlah'])) {
                                        echo "0";
                                    }
                                    else{
                                        echo $lem['jumlah'];
                                    }
                                ?>
                            </div>
                            <div class='satuan'>Produk</div>
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-md-3 col-lg-3 p10 mb20">
                <div class="block counter-block orange">
                    <div class="row">
                        <div class="col-sm-12 text-left p20">
                            <div class="valueicon">
                                <img width="30px" src="<?php echo "$url/img/garnis/sn0001-icon-sparepart.svg";?>">
                            </div>
                            <p class="label f14">Consumable</p>
                            <div class="value">
                                <?php 
                                    if (empty($consumable['jumlah'])) {
                                        echo "0";
                                    }
                                    else{
                                        echo $consumable['jumlah'];
                                    }
                                ?>
                            </div>
                            <div class='satuan'>Produk</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 mb40"></div>

        </div>
    </div>
</div>
