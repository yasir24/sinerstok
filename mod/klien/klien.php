<?php
    if ($_GET['mod'] == 'add') {
        $kat = 1;
    }
    else{
        $kat = 2;
        $de = mysqli_query($koneksi, "SELECT * from klien where id_klien = $_GET[id]");
        $d  = mysqli_fetch_array($de);
    }
 ?>
<form method="post" action="mod/<?php echo"$folder/aksi.php?mod=$mod&kat=$kat&url=$_GET[url]&folder=$_GET[folder]"; ?>">
    <input type="hidden" name="id" value="<?php echo $d['id_klien'];?>">
    <div class="header fixed-header">
        <div class="container-fluid" style="padding: 0px ">
            <div class="row">
                <div class="col-lg-12 d-none d-lg-block">

                    <?php
                        $aktif = "master";
                        include "inc/menu.php";
                    ?>

                </div>            
            </div>
        </div>
    </div>
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="">
                <div class="d-none d-lg-block">                    
                    <div class="row">
                        <div class="bold col-4">
                            <h4 class="arialbold mt5 f21" style="text-transform: c;">Klien</h4>
                        </div>
                        <!-- <div class="col-8 text-right">
                            <a href="mod-master-2.htm" class='btn w100 btn-default mr10'>BATAL</a>
                            <input type="submit" name="save" onclick="sessionStorage.clear()" value="SIMPAN" class="w100 btn btn-primary">
                        </div> -->
                    </div>
                    <hr>
                </div>
                <div class="counter-block mb-12 col-sm-12 mb-20 pl0 pt0">
                    <div class="row">
                        <div class="col-md-2 col-sm-6">
                            <div class="judul">
                                Perusahaan
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" class="form-control" width="80%" placeholder="..." name="nama_klien" value="<?php echo (isset($d['nama_klien'])) ? $d['nama_klien'] : '' ; ?>">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-2 col-sm-6">
                            <div class="judul">
                                Alamat
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" class="form-control" width="80%" placeholder="..." name="alamat" value="<?php echo (isset($d['alamat'])) ? $d['alamat'] : '' ; ?>">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-2 col-sm-6">
                            <div class="judul">
                                No. Telepon
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" class="form-control" width="80%" placeholder="..." name="tlp"  value="<?php echo (isset($d['tlp'])) ? $d['tlp'] : '' ; ?>">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-2 col-sm-6">
                            <div class="judul">
                                Propinsi
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" id="propinsi" name="id_propinsi">
                                        <?php
                                            $kota=mysqli_query($koneksi, "SELECT * FROM propinsi ORDER BY propinsi ASC");
                                            echo "<option value='' selected>- Pilih Propinsi -</option>";
                                            while ($k=mysqli_fetch_array($kota)){
                                                if ($d['id_propinsi'] == $k['id_propinsi']) {
                                                    echo "<option value='$k[id_propinsi]' selected>$k[propinsi]</option>";
                                                }
                                                else{
                                                    echo "<option value=$k[id_propinsi]>$k[propinsi]</option>";                                                    
                                                }
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-2 col-sm-6">
                            <div class="judul">
                                Kota / Kabupaten
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" id="kabupaten" name="id_kabupaten">
                                        <option>- Pilih Kabupaten -</option>
                                        <?php
                                            $kota=mysqli_query($koneksi, "SELECT * FROM kabupaten ORDER BY kabupaten ASC");
                                            while ($k=mysqli_fetch_array($kota)){
                                                if ($d['id_kabupaten'] == $k['id_kabupaten']) {
                                                    echo "<option value='$k[id_kabupaten]' selected>$k[kabupaten]</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-2 col-sm-6">
                            <div class="judul">
                                Kecamatan
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <span class="select-yasira">
                                    <select class="btn btn-yasir" id="kecamatan" name="id_kecamatan">
                                        <option>- Pilih Kecamatan -</option>
                                        <?php
                                            $kota=mysqli_query($koneksi, "SELECT * FROM kecamatan ORDER BY kecamatan ASC");
                                            while ($k=mysqli_fetch_array($kota)){
                                                if ($d['id_kecamatan'] == $k['id_kecamatan']) {
                                                    echo "<option value='$k[id_kecamatan]' selected>$k[kecamatan]</option>";
                                                }
                                            }
                                        ?>
                                    </select>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-2 col-sm-6">
                            <div class="judul">
                                Kelurahan
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" class="form-control" width="80%" placeholder="..." name="kelurahan" value="<?php echo (isset($d['kelurahan'])) ? $d['kelurahan'] : '' ; ?>">
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 mb15"></div>
                        <div class="col-md-2 col-sm-6">
                            <div class="judul">
                                Kode Pos
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-6">
                            <div class="inp-text abu">
                                <input type="text" class="form-control" width="80%" placeholder="..." name="kode_pos" value="<?php echo (isset($d['kode_pos'])) ? $d['kode_pos'] : '' ; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
