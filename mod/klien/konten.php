<div class="header fixed-header">
    <div class="container-fluid" style="padding: 0px ">
        <div class="row">
            <div class="col-lg-12 d-none d-lg-block">

                <?php
                    $aktif = "master";
                    include "inc/menu.php";
                ?>

            </div>            
        </div>
    </div>
</div>
<div class="content sm-gutter">
    <div class="container-fluid padding-25 sm-padding-10">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 text-left">
                    <div class="h30 w100 pull-left"></div>
                    <a href="add-<?=$folder;?>-2.htm" class="ml10 btn iconplus pull-right"></a>
                    <a href="#" class="ml10 btn icondownload pull-right"></a>
                    <div class="cari pull-right">
                        <i class="iconcari"></i>
                        <input type="text" id="input-filter" name="cari" placeholder="Kata kunci ...">
                    </div>
                    <span class="select-yasir pull-right">
                        <select class="btn btn-yasir"  id="urutkan" style="margin-top:0">
                            <option value="0">- Urutkan -</option>
                            <option value="1">Nama</option>
                        </select>
                    </span>
                    <hr class='pull-left' style="width: 100%;">
                </div>
                    <div class="col-sm-12 table-responsive text-no-wrap">
                        <table class="table" id="dataTable1" data-table="data-table-polos-disfirst">
                            <thead class="text-middle">
                                <tr>
                                    <th class="no-sort">Klien</th>
                                    <th class="no-sort" width="10%"></th>
                                </tr>
                            </thead>
                            <tbody class="text-middle">
                                <?php
                                    $se = mysqli_query($koneksi, "SELECT * from klien order by nama_klien asc");
                                    while ($s = mysqli_fetch_array($se)) { 
                                ?>                                
                                <tr>
                                    <td style="height: 50px;"><?php echo $s['nama_klien']; ?></td>
                                    <td style="height: 50px;">
                                        <a href="<?php echo"mod/$folder/aksi.php?mod=$mod&id=$s[id_klien]&kat=3&url=$_GET[url]&folder=$_GET[folder]"; ?>" class="btn btn-default pull-right close"></a>
                                        <?php
                                            if($s['kategori'] == 'klien'){
                                                echo"";
                                            }
                                        ?>
                                        <a href="<?php echo"edit-$folder-$s[id_klien]-$_GET[url].htm"; ?>" class="btn btn-default pull-right iconchange mr10"></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
