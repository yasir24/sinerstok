<?php
function upload($nama_file,$folder,$lokasi_file,$lebar,$tinggi){
	$nama_folder = "../../assets/images/$folder/";
	list($lebar_asli, $tinggi_asli, $source_type) = getimagesize($lokasi_file);
	$gambar_asli = imagecreatefromjpeg($lokasi_file);
	$ukuran_asli = $lebar_asli / $tinggi_asli;
	$ukuran = $lebar / $tinggi;
	if ($ukuran_asli > $ukuran) {
    	$tinggi_sementara = $tinggi;
    	$lebar_sementara = ( int ) ($tinggi * $ukuran_asli);
	} else {
    	/*jika gambar sama atau lebih tinggi*/
    	$lebar_sementara = $lebar;
    	$tinggi_sementara = ( int ) ($lebar / $ukuran_asli);
	}
	/*rubah ukuran gambar ke ukuran sementara*/
	$gambar_sementara = imagecreatetruecolor($lebar_sementara, $tinggi_sementara);
	imagecopyresampled($gambar_sementara,$gambar_asli,0, 0,0, 0,$lebar_sementara, $tinggi_sementara,$lebar_asli, $tinggi_asli);
	/*Copy cropped region from temporary image into the desired GD image*/
	$x_absis = ($lebar_sementara - $lebar) / 2;
	$y_absis = ($tinggi_sementara - $tinggi) / 2;
	$gambar_akhir = imagecreatetruecolor($lebar, $tinggi);
	imagecopy($gambar_akhir,$gambar_sementara,0, 0,$x_absis, $y_absis,$lebar, $tinggi);
	imagejpeg($gambar_akhir,$nama_folder.$nama_file);
	imagedestroy($gambar_akhir);
}

function upload_mark($nama_file,$folder,$lokasi_file,$lebar,$tinggi){
	$lokasi_watermark = "../../../images/watermark.png";
	$nama_folder = "../../../$folder/";
	list($lebar_asli, $tinggi_asli, $source_type) = getimagesize($lokasi_file);
	
    $im = imagecreatetruecolor($lebar, $tinggi);
    $img_src = imagecreatefromjpeg($lokasi_file);
    imagecopyresampled($im, $img_src, 0, 0, 0, 0, $lebar, $tinggi, $lebar_asli, $tinggi_asli);
    $watermark = imagecreatefrompng($lokasi_watermark);
    list($lebar_watermark, $tinggi_watermark) = getimagesize($lokasi_watermark);        
    $pos_x = $lebar - $lebar_watermark + 150; 
    $pos_y = $tinggi - $tinggi_watermark;
    imagecopy($im, $watermark, $pos_x, $pos_y, 0, 0, $lebar_watermark, $tinggi_watermark);
    imagejpeg($im,$nama_folder.$nama_file);
    imagedestroy($im);
    unlink($lokasi_file);
    return true;
}
?>