<?php
function tulis_cekbox($field,$koneksi,$judul) {
        $query ="select * from ".$judul." order by nama_".$judul;
        $r = mysqli_query($koneksi,$query);
        $_arrNilai = explode('+', $field);
        $str = '';
        $no=1;
        while ($w = mysqli_fetch_array($r)) {
            $_ck = (array_search($w[1], $_arrNilai) === false)? '' : 'checked';
            $str .= "<input type='checkbox' name='".$judul."[]' value='$w[1]' id='$judul$no' $_ck>";
            $no++;
        }
        return $str;
    }

    function tulis_selectbox($field,$koneksi,$judul) {
        $query ="select *, id_$judul as id from ".$judul." order by nama_".$judul;
        $r = mysqli_query($koneksi,$query);
        $_arrNilai = explode('+', $field);
        $str = '';
        $no=1;
        while ($w = mysqli_fetch_array($r)) {
            if(array_search($w['id'], $_arrNilai) === false){
                // $str .= "<option value='$w[id]'>$w[1]</option>";
            }
            else{
                $str .= "<option value='$w[0]'>$w[1]</option>";
            }
            $no++;
        }
        return $str;
    }
?>