<!DOCTYPE html>
<html lang="en">
<?php include 'inc/head.php';?>
<body>

<!-- Preloader Starts Here -->

<!-- Preloader Ends Here -->

<section style="background: url(../../../images.pexels.com/photos/176851/pexels-photo-176851663a.jpg?w=940&amp;h=650&amp;auto=compress&amp;cs=tinysrgb);background-size: cover">
    <div class="height-100-vh bg-primary-trans">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 col-lg-4">
                    <?php
                        if(isset($_GET['info'])){
                            if($_GET['info']==1){
                                $teks="<strong>Gagal!</strong> email dan password tidak cocok dengan database kami.";
                                echo"<div class='alert alert-danger'>
                                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
                                    $teks
                                </div>";
                            }
                            else{
                                $teks="<strong>Sukses!</strong> anda telah berhasil keluar dari sistem.";
                                echo"<div class='alert alert-success'>
                                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button
                                    $teks
                                </div>";
                            }
                        }
                    ?>
                    <div class="login-div">
                        <p class="logo mb-1">Sinerstok</p>
                        <p class="mb-4" style="color: #a5b5c5">Sign into your pages account</p>
                        <form id="needs-validation" action="proses-login.htm" novalidate  method="post">
                            <div class="form-group">
                                <label>Login</label>
                                <input name="email" class="form-control input-lg" placeholder="Username" type="text" required>
                                <div class="invalid-feedback">This field is required.</div>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input name="password" class="form-control input-lg" placeholder="Credentials" type="password" required>
                                <div class="invalid-feedback">This field is required.</div>
                            </div>
                            <button class="btn btn-primary mt-2">Sign In</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Jquery-->
<script type="text/javascript" src="assets/js/jquery-3.2.1.min.js"></script>
<!--Bootstrap Js-->
<script type="text/javascript" src="assets/js/popper.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!--Modernizr Js-->
<script type="text/javascript" src="assets/js/modernizr.custom.js"></script>

<!--Morphin Search JS-->
<script type="text/javascript" src="assets/plugins/morphin-search/classie.js"></script>
<script type="text/javascript" src="assets/plugins/morphin-search/morphin-search.js"></script>
<!--Morphin Search JS-->
<script type="text/javascript" src="assets/plugins/preloader/pathLoader.js"></script>
<script type="text/javascript" src="assets/plugins/preloader/preloader-main.js"></script>

<!--Chart js-->
<script type="text/javascript" src="assets/plugins/charts/Chart.min.js"></script>

<!--Sparkline Chart Js-->
<script type="text/javascript" src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="assets/plugins/sparkline/jquery.charts-sparkline.js"></script>

<!--Custom Scroll-->
<script type="text/javascript" src="assets/plugins/customScroll/jquery.mCustomScrollbar.min.js"></script>
<!--Sortable Js-->
<script type="text/javascript" src="assets/plugins/sortable2/sortable.min.js"></script>
<!--DropZone Js-->
<script type="text/javascript" src="assets/plugins/dropzone/dropzone.js"></script>
<!--Date Range JS-->
<script type="text/javascript" src="assets/plugins/date-range/moment.min.js"></script>
<script type="text/javascript" src="assets/plugins/date-range/daterangepicker.js"></script>
<!--CK Editor JS-->
<script type="text/javascript" src="assets/plugins/ckEditor/ckeditor.js"></script>
<!--Data-Table JS-->
<script type="text/javascript" src="assets/plugins/data-tables/datatables.min.js"></script>
<!--Editable JS-->
<script type="text/javascript" src="assets/plugins/editable/editable.js"></script>
<!--Full Calendar JS-->
<script type="text/javascript" src="assets/plugins/full-calendar/fullcalendar.min.js"></script>

<!--  Main JS -->
<script src="assets/js/main.js"></script>

</body>
</html>